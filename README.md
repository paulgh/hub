Election Hub Web
========================

Welcome to the Election Hub Web based on the Symfony Standard Edition(3.4) - a fully-functional Symfony2
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An FinFlow you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * [**AsseticBundle**][12] - Adds support for Assetic, an asset processing
    library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **FinFlow** -  bundle


    "doctrine/doctrine-bundle": "^1.6",
    "doctrine/doctrine-migrations-bundle": "^1.2",
    "doctrine/migrations": "^1.1",
    "doctrine/orm": "^2.5",
    "evence/soft-deleteable-extension-bundle": "^1.3",
    "friendsofsymfony/ckeditor-bundle": "^2.1",
    "friendsofsymfony/jsrouting-bundle": "^1.6",
    "friendsofsymfony/rest-bundle": "^2.2",
    "friendsofsymfony/user-bundle": "^2.0",
    "gedmo/doctrine-extensions": "^2.4",
    "ibrows/sonata-translation-bundle": "^1.1",
    "incenteev/composer-parameter-handler": "^2.0",
    "jms/di-extra-bundle": "^1.9",
    "jms/serializer-bundle": "~1.1",
    "jms/translation-bundle": "dev-master",
    "knplabs/knp-paginator-bundle": "^2.8",
    "kriswallsmith/buzz": "^0.16",
    "lunetics/locale-bundle": "^2.6",
    "ma27/api-key-authentication-bundle": "^1.2",
    "nelmio/api-doc-bundle": "2.12",
    "nelmio/cors-bundle": "^1.5",
    "ob/highcharts-bundle": "dev-master",
    "ocramius/proxy-manager": "^1.0|^2.0",
    "oh/google-map-form-type-bundle": "dev-master",
    "sensio/buzz-bundle": "dev-master",
    "sensio/distribution-bundle": "^5.0.19",
    "sensio/framework-extra-bundle": "^5.2",
    "sonata-project/admin-bundle": "^3.20",
    "sonata-project/doctrine-orm-admin-bundle": "^3.1",
    "sonata-project/easy-extends-bundle": "^2.2",
    "sonata-project/user-bundle": "dev-master",
    "stfalcon/tinymce-bundle": "^2.3",
    "stof/doctrine-extensions-bundle": "^1.2",
    "symfony/assetic-bundle": "^2.8",
    "twig/twig": "^2.6",
    "vich/uploader-bundle": "^1.6",
    "voryx/restgeneratorbundle": "dev-master",


All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  http://symfony.com/doc/3.4/book/installation.html
[6]:  http://symfony.com/doc/2.6/bundles/SensioFrameworkExtraBundle/index.html
[7]:  http://symfony.com/doc/2.6/book/doctrine.html
[8]:  http://symfony.com/doc/2.6/book/templating.html
[9]:  http://symfony.com/doc/2.6/book/security.html
[10]: http://symfony.com/doc/2.6/cookbook/email.html
[11]: http://symfony.com/doc/2.6/cookbook/logging/monolog.html
[12]: http://symfony.com/doc/2.6/cookbook/assetic/asset_management.html
[13]: http://symfony.com/doc/2.6/bundles/SensioGeneratorBundle/index.html
