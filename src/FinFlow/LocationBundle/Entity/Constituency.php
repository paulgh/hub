<?php

namespace FinFlow\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;

/**
 * Town
 * 
 * @ORM\Table (name="constituency")
 * @ORM\Entity(repositoryClass="FinFlow\LocationBundle\Repository\Constituency")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity(fields="name", message="Constituency Exits")

 * 
 */
class Constituency {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,unique=true)
     */
    private $name;


    /**
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;


    /**
     *
     * @ORM\OneToMany(targetEntity="FinFlow\ElectionBundle\Entity\PollingResult",mappedBy="constituency")
     */
    private $pollingResult;


    /**
     * @ORM\Column(name="flash_area", type="boolean", nullable=true)
     * @Expose
     */
    private $flashArea;


    /**
     * @ORM\Column(name="latitude", type="string", nullable=true)
     * @Expose
     */
    private $latitude;

    /**
     *
     * @ORM\Column(name="longitude",type="string", nullable=true)
     * @Expose
     */
    private $longitude;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Town
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Town
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Town
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Town
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    /**
     * Set region
     *
     * @param \FinFlow\LocationBundle\Entity\Region $region
     * @return Town
     */
    public function setRegion(\FinFlow\LocationBundle\Entity\Region $region = null) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \FinFlow\LocationBundle\Entity\Region 
     */
    public function getRegion() {
        return $this->region;
    }

    public function __toString() {
        return (String)$this->name;
    }


    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Actor
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Actor
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @return mixed
     */
    public function getFlashArea()
    {
        return $this->flashArea;
    }

    /**
     * @param mixed $flashArea
     */
    public function setFlashArea($flashArea)
    {
        $this->flashArea = $flashArea;
    }

    /**
     * @return mixed
     */
    public function getPollingResult()
    {
        return $this->pollingResult;
    }

    /**
     * @param mixed $pollingResult
     */
    public function setPollingResult($pollingResult)
    {
        $this->pollingResult = $pollingResult;
    }


}
