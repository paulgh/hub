<?php

namespace FinFlow\UserBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManager;

class ApiKeyUserProvider implements UserProviderInterface {

    private $em;

    public function __construct(EntityManager $entityManager) {
            $this->em = $entityManager;
    }

    public function getUsernameForApiKey($apiKey) {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        $device = $this->em->getRepository('UserBundle:DeviceIdentity')->findBy(array('token'=>$apiKey));
        if(!$device)
        {
            return null;
        }
        $username = $device[0]->getUser()->getUsername();

        return $username;
    }

    public function loadUserByUsername($username) {
        $user = $this->em->getRepository('UserBundle:User')->loadUserByUsername($username);
        return $user;

    }

    public function refreshUser(UserInterface $user) {

        return $user;

        //throw new UnsupportedUserException();
    }

    public function supportsClass($class) {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }

}
