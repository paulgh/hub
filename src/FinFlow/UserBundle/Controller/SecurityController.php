<?php

namespace FinFlow\UserBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use FOS\UserBundle\Model\UserInterface;
use FinFlow\UserBundle\Entity\User;
use FinFlow\UserBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller {

    /**
     *  @ApiDoc(
     *  resource=true,
     *  description="Log In User entity",
     *  parameters={
     * {"name"="_username", "dataType"="string", "required"=false, "description"="username"},
     * {"name"="_password", "dataType"="string", "required"=true, "description"="password"},
     * {"name"="_deviceId", "dataType"="string", "required"=true, "description"="device Id"},
     *  }
     * )
     * @Route("/login/api", name="paulem")
     * @return type
     */
    public function loginAction(Request $request) {
//        $request = $this->$request;
//         $request;exit;
        $session = $request->getSession();

        $username = $request->get("_username");
        $password = $request->get("_password");
        $deviceid = $request->get("_deviceId");

        if (empty($username) AND empty($password)) {
            $username = $request->get("username");
            $password = $request->get("password");
            $deviceid = $request->get("deviceId");
        }

        if (!empty($username) AND ! empty($password)) {
            $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);
            if ($user != null AND is_object($user)) {
                $token = new UsernamePasswordToken($user, $password, "main", $user->getRoles());
                $passwordValid = $this->get("security.encoder_factory")->getEncoder($user)->isPasswordValid($user->getPassword(), $token->getCredentials(), $user->getSalt());

                if ($passwordValid) {

                    if (is_object($user)) {

                            $apikey = bin2hex(openssl_random_pseudo_bytes(100));
                            $em = $this->getDoctrine()->getManager();
                            $device = $em->getRepository('UserBundle:DeviceIdentity')->findBy(array('device' => $deviceid));

                            if (!$device) {
                                $device = new DeviceIdentity();
                                $device->setToken($apikey);
                                $device->setUser($user);
                                $device->setDevice($deviceid);
                                $em->persist($device);
                                $em->flush();
                            } else {
                                $device = $device[0];
                                $device->setToken($apikey);
                                $em->persist($device);
                                $em->flush();
                            }
//                            dump($user);exit;
                            return new JsonResponse(array('status' => true, 'data' => array(
                                    'apiKey' => $apikey,
                                    'id' => $user->getId(),
                                    'pixUrl' => $user->getPixUrl(),
                                    'name' => $user->getName(),
                                    'userType' => $user->getUserType(),
                                    'username' => $user->getUsername(),
                                    'password' => $password,
                                    'shop'=> $user->getShopId(),
                                    'shopname'=> $user->getShopName(),
                                )
                                    )
                            );

                    }
                } else {
                    return $this->errorResponse("Bad credentials.", 401);
                }
            } else {
                return $this->errorResponse("No user found.");
            }
        }
    }

    /**
     * Log Out User entity
     *  @ApiDoc(
     *  resource=true,
     * )
     * @Route("/logout/api", name="api_logout")
     * @return type
     */
    public function logoutAction() {
        $user = $this->getUser();
        $devices = $user->getDeviceIdentity();
        foreach ($devices as $device) {
            $user->removeDeviceIdentity($device);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return new JsonResponse(array('status' => true, 'data' => array(
                'message' => "You have been logged out.",
            )
                )
        );
    }

}
