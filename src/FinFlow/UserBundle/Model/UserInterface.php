<?php
namespace FinFlow\UserBundle\Model;

interface UserInterface
{
	/**
   * Get FirstName
   *
   * @return string 
   */
  public function getUsername();
  

  /**
   * Get FirstName
   *
   * @return string 
   */
  public function setUsername( $firstName);
}