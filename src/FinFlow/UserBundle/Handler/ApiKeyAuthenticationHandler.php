<?php

namespace FinFlow\UserBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Ma27\ApiKeyAuthenticationBundle\Event\OnAuthenticationEvent;
use Ma27\ApiKeyAuthenticationBundle\Event\OnInvalidCredentialsEvent;
use Ma27\ApiKeyAuthenticationBundle\Ma27ApiKeyAuthenticationEvents;
use Ma27\ApiKeyAuthenticationBundle\Model\Key\KeyFactoryInterface;
use Ma27\ApiKeyAuthenticationBundle\Model\Login\AuthenticationHandlerInterface;
use Ma27\ApiKeyAuthenticationBundle\Model\Password\PasswordHasherInterface;
use Ma27\ApiKeyAuthenticationBundle\Model\User\ClassMetadata;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Concrete handler for api key authorization.
 */
class ApiKeyAuthenticationHandler implements AuthenticationHandlerInterface {

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var PasswordHasherInterface
     */
    private $passwordHasher;

    /**
     * @var KeyFactoryInterface
     */
    private $keyFactory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var string
     */
    private $modelName;

    /**
     * @var ClassMetadata
     */
    private $classMetadata;

    /**
     * @var Container
     */
    private $container;

    /**
     * Constructor.
     *
     * @param ObjectManager            $om
     * @param PasswordHasherInterface  $passwordHasher
     * @param KeyFactoryInterface      $keyFactory
     * @param EventDispatcherInterface $dispatcher
     * @param string                   $modelName
     * @param ClassMetadata            $metadata
     * @param Container                $container
     */
    public function __construct(
    ObjectManager $om, PasswordHasherInterface $passwordHasher, KeyFactoryInterface $keyFactory, EventDispatcherInterface $dispatcher, $modelName, ClassMetadata $metadata, Container $container
    ) {

        $this->om = $om;
        $this->passwordHasher = $passwordHasher;
        $this->keyFactory = $keyFactory;
        $this->eventDispatcher = $dispatcher;
        $this->modelName = (string) $modelName;
        $this->classMetadata = $metadata;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(array $credentials) {
        $loginProperty = $this->classMetadata->getPropertyName(ClassMetadata::LOGIN_PROPERTY);
        $passwordProperty = $this->classMetadata->getPropertyName(ClassMetadata::PASSWORD_PROPERTY);
       
        if (!isset($credentials[$passwordProperty])) {
            throw new \InvalidArgumentException(
            sprintf('Unable to find password property "%s" in credential set!', $passwordProperty)
            );
        }

        if (!isset($credentials[$loginProperty])) {
            throw new \InvalidArgumentException(
            sprintf('Unable to find login property "%s" in credential set!', $loginProperty)
            );
        }

        $objectRepository = $this->om->getRepository($this->modelName);
        $object = $objectRepository->findOneBy(array($loginProperty => $credentials[$loginProperty]));

        $user_manager = $this->container->get('fos_user.user_manager');
        $factory = $this->container->get('security.encoder_factory');
        $user = $user_manager->loadUserByUsername($object->getUsername());
        $encoder = $factory->getEncoder($user);
        

        $bool = ($encoder->isPasswordValid($user->getPassword(), $credentials[$passwordProperty], $user->getSalt())) ? true : false;

        if (null === $object || !$bool) {
            $this->eventDispatcher->dispatch(Ma27ApiKeyAuthenticationEvents::CREDENTIAL_FAILURE, new OnInvalidCredentialsEvent($object));
            throw new \Exception('invalid username or password');
        }
        
        $this->eventDispatcher->dispatch(Ma27ApiKeyAuthenticationEvents::AUTHENTICATION, new OnAuthenticationEvent($object));

        $this->classMetadata->modifyProperty($object, $this->keyFactory->getKey(), ClassMetadata::API_KEY_PROPERTY);
        $this->om->persist($object);

        $this->om->flush();

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function removeSession($user, $purgeJob = false) {
        $this->classMetadata->modifyProperty($user, null, ClassMetadata::API_KEY_PROPERTY);

        $event = new OnLogoutEvent($user);
        if ($purgeJob) {
            $event->markAsPurgeJob();
        }

        $this->eventDispatcher->dispatch(Ma27ApiKeyAuthenticationEvents::LOGOUT, $event);

        $this->om->persist($user);

        // on purge jobs one big flush will be commited to the db after the whole action
        if (!$purgeJob) {
            $this->om->flush();
            return $user->getUsername();
        }
    }

    /**
     * Getter for the object manager.
     *
     * @return ObjectManager
     */
    protected function getOm() {
        return $this->om;
    }

    /**
     * Getter for the password hasher.
     *
     * @return PasswordHasherInterface
     */
    protected function getPasswordHasher() {
        return $this->passwordHasher;
    }

    /**
     * Getter for the key factory.
     *
     * @return KeyFactoryInterface
     */
    protected function getKeyFactory() {
        return $this->keyFactory;
    }

    /**
     * Getter for the dispatcher.
     *
     * @return EventDispatcherInterface
     */
    protected function getEventDispatcher() {
        return $this->eventDispatcher;
    }

    /**
     * Getter for the model name.
     *
     * @return string
     */
    protected function getModelName() {
        return $this->modelName;
    }

    /**
     * @return ClassMetadata
     */
    public function getClassMetadata() {
        return $this->classMetadata;
    }

}
