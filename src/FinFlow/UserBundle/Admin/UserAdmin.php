<?php

namespace FinFlow\UserBundle\Admin;

use Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends Admin
{

    /**
     * Create and Edit
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '500M');
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        $adminId= $this->getSubject()->getId();
        $userId= $user->getId();
        $adminParentId= $this->getSubject()->getParent()? $this->getSubject()->getParent()->getId():null;

        //protect what which accounts staff can edit
        if ($user->hasRole('ROLE_STAFF')) {
            //is user the parent of the client account?
            if($this->getSubject()->getUserType()=='Client' and $adminParentId!==$userId) {
            exit;
            }
            //prevent staff from editing other staff
            if($this->getSubject()->getUserType()=='Staff' and $adminId!==$userId) {
                exit;
            }

        }


        //check client account edit access (Only edit their account)
        if ($user->hasRole('ROLE_CLIENT')) {
            //client owns accounts do nothing
            if($adminId!==$userId){

                exit;
            }

        }

        if ($user->hasRole('')) {
            //client owns accounts do nothing
            if($adminId!==$userId){
                exit;
            }

        }


        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getPix();
            $logo = $this->getSubject()->getLogoName();

            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            $logoFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/user_image/' . $image;

                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = '<img  width="150px" src="' . $fullPath . '" class="admin-preview" />';
            }
            if ($logo) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/logo/' . $logo;

                // add a 'help' option containing the preview's img tag
                $logoFieldOptions['help'] = '<img src="' . $fullPath . '" class="admin-preview" />';
            }
        }

        $formMapper
            ->with('Account Info', array('class' => 'col-md-6'))->end()
            ->with('Address Info', array('class' => 'col-md-6'))->end();
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
//        $countryCode = $user->getCountry() ? $user->getCountry()->getcountryCode() : "";
        $children = $this->getUserChildren();
        $parent = $user->getParent();
        $formMapper
            ->with('Account Info')
            ->add('imageFile', 'file', array(
                'label' => 'Profile Picture',
                'by_reference' => false,
                'data_class' => null,
                'required' => false), $fileFieldOptions)
            ->add('name')
            ->add('gender', 'choice', array('choices' => array('Male' => 'M', 'Female' => 'F')))
            ->add('username')
            ->add('email')
            ->add('plainpassword', null, array('label' => 'Plain Password'));

        if (in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            $formMapper
                ->add('userType', 'choice', array('choices' => array(
                    'Client' => 'Client', 'Staff' => 'Staff'
                )))
                ->add('parent', null, array(
                    'placeholder' => 'Select Parent'
                ))
                ->add('groups', null, array(
                        'label' => 'User  Abiltity',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => true,
                        'class' => 'ApplicationSonataUserBundle:Group',
                        'error_bubbling' => true,
                    )
                )
                ->add('roles', 'choice', array('choices' => $this->getRoles(), 'multiple' => true))
                ->add('enabled');


        }


        $formMapper;
//            ->end()
//                    ->add('mobileNo', null, array('help' => "Mobile Number should be with country code eg:<b>233xxxxxxxxx </b>"))
//            ->add('address')
//            ->add('nationality')
//            ->end();
    }

    public function getNewInstance()
    {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
//        $roles = array('ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_ALLOWED_TO_SWITCH');
        $roles = array('ROLE_ADMIN');
        $instance = parent::getNewInstance();
        $instance->setParent($user);
        $instance->setRoles($roles);


        return $instance;
    }

    /**
     * List
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
//                ->add('actor.pix', null, array(
//                    'template' => 'FinFlowActorBundle:Admin:list_pix_preview.html.twig',
//                    'attr' => array('style' => 'width: 44.171%; ')))
            ->add('username')
            ->add('name')
//            ->add('parent')
//            ->add('gender', 'choice', array('choices' => array('M' => 'Male', 'F' => 'Female')))
//            ->add('email')
//            ->add('plainpassword')
            ->add('userType')
//            ->add('mobileNo')
//            ->add('country')
//            ->add('region')
//            ->add('district')
//            ->add('town')
//            ->add('village')
            ->add('groups')
            ->add('parent')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }


    /**
     * Filters
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('parent')
            ->add('name')
            ->add('groups')
            ->add('email')
            ->add('mobileNo')
            ->add('gender')
            ->add('roles');;
    }

    /**
     * Show
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('actor.pix', null, array(
                'template' => 'FinFlowActorBundle:Admin:list_pix_preview.html.twig',
                'attr' => array('style' => 'width: 44.171%; ')))
            ->add('username')
            ->add('firstName')
            ->add('lastName');
    }

//    public function getTemplate($name)
//    {
//        switch ($name) {
////            case 'edit':
////                return 'UserBundle:Admin:user_edit.html.twig';
////                break;
////            default:
////                return parent::getTemplate($name);
////                break;
//        }
//    }

    private function getRoles()
    {
        return self::flattenRoles($this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles'));
    }

    /** Turns the role's array keys into string <ROLES_NAME> keys.
     * @todo Move to convenience or make it recursive ? ;-)
     */
    protected static function flattenRoles($rolesHierarchy)
    {
        $flatRoles = array();
        foreach ($rolesHierarchy as $roles) {

            if (empty($roles)) {
                continue;
            }
            foreach ($roles as $role) {
                if (!isset($flatRoles[$role])) {
                    $flatRoles[$role] = $role;
                }
            }
        }

        return $flatRoles;
    }

    public function getDefaultOptions()
    {
        return array(
            'roles' => null
        );
    }

    public function getUser()
    {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        return $securityContext->getToken()->getUser();
    }

    public function preUpdate($user)
    {

    }
    public function prePersist($object)
    {

    }

    public function getUserChildren()
    {
//        return $this->getConfigurationPool()->getContainer()
//            ->get('doctrine')->getRepository("UserBundle:User")
//            ->getChildren($this->getUser()->getId());
    }


    public function createQuery($context = 'list')
    {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $user = $securityContext->getToken()->getUser();
        $query = parent::createQuery($context);
//        dump($user->getRoles());

        if ($user->hasRole('ROLE_STAFF') && $user->getUserType() == 'Staff') {
            $query = $this->getModelManager()->createQuery($this->getClass(), 'u');
            $query->where('u.parent = :user_id')
                ->setParameter('user_id', $user->getId());
        }


        return $query;
    }

}
