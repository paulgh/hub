<?php 
namespace FinFlow\UserBundle\Services;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Sms Handler 
 * @DI\Service("security.manager")
 * @DI\Tag("kernel.event_listener", attributes = {"event" = "kernel.request", "method"="onKernelRequest"})
 */
class FinFlowRequestListener
{

	private $om;
	private $auth;
	private $security;

	/**
	* @DI\InjectParams({
	*     "om" = @DI\Inject("doctrine.orm.entity_manager"),
    *     "auth" = @DI\Inject("security.authorization_checker", required = false),
    *     "security" = @DI\Inject("security.token_storage", required = false),
	*     "container" = @DI\Inject("service_container", required = false),
	* })
	*/
	public function __construct(ObjectManager $om, $auth, $security, $container)
	{
		$this->om = $om;
        $this->auth = $auth;
        $this->security = $security;
		$this->container = $container;
	}
    public function onKernelRequest(GetResponseEvent $event)
    {


    }
}