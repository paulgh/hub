<?php

namespace FinFlow\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\SerializerBundle\Annotation\Exclude;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

//use Ma27\ApiKeyAuthenticationBundle\Annotation as Auth;
//use Cunningsoft\ChatBundle\Entity\AuthorInterface;


/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="FinFlow\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 * @UniqueEntity("mobileNo")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;


    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Serializer\Exclude()
     */
    protected $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Serializer\Exclude()
     */
    protected $groups;


    /**
     *
     * @Assert\NotBlank(message = "Password can not be blank!!", groups="required")
     * @ORM\Column(name="plain_password", type="string", nullable=true)
     *  Add a comment to this line
     * @Serializer\Exclude()
     */
    private $plainpassword;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;


    /**
     *  @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @Serializer\Exclude()
     */
    private $name;

    /**
     *  @Assert\Type(type="numeric", message="actor.error.mobile_no_onlynumbers", groups={"default_actor"})
     * @Assert\Length(min= 8, max= 15, maxMessage="actor.error.mobile_no_maxlength", minMessage="actor.error.mobile_no_minlength", groups={"default_actor"})
     * @ORM\Column(name="mobile_no", type="string", length=20 , nullable=true)
     * @Serializer\Exclude()
     */
    private $mobileNo;


    /**
     * @ORM\Column(name="pix", type="string", length=255, nullable=true)
     * @Serializer\Exclude()
     */
    private $pix;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="pix")
     *
     * @var File
     * @Serializer\Exclude()
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, name="logo_name",nullable=true)
     *
     * @var string $imageName
     * @Serializer\Exclude()
     */
    protected $logoName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="logo", fileNameProperty="logoName")
     *
     * @var File $imageFile
     * @Serializer\Exclude()
     */
    protected $logoFile;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Commodity
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setLogoFile(File $logo = null)
    {
        $this->logoFile = $logo;

        if ($logo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @param string $logoName
     */
    public function setLogoName($logoName)
    {
        $this->logoName = $logoName;
    }

    /**
     * @return string
     */
    public function getLogoName()
    {
        return $this->logoName ? $this->logoName : 'null';
    }


    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=255,nullable=true)
     * @Serializer\Exclude()
     */
    private $userType;


    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setPlainpassword($password)
    {
        $this->plainpassword = $password;

        return $this;
    }

    public function getPlainpassword()
    {
        return $this->plainpassword;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt = NULL)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set parent
     *
     * @param \FinFlow\UserBundle\Entity\User $parent
     * @return User
     */
    public function setParent(\FinFlow\UserBundle\Entity\User $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \FinFlow\UserBundle\Entity\User
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mobileNo
     *
     * @param string $mobileNo
     * @return User
     */
    public function setMobileNo($mobileNo)
    {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo
     *
     * @return string
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set pix
     *
     * @param string $pix
     * @return User
     */
    public function setPix($pix)
    {
        $this->pix = $pix;

        return $this;
    }

    /**
     * Get pix
     *
     * @return string
     */
    public function getPix()
    {
        return $this->pix;
    }


    /**
     *
     * @Accessor(getter="getcreated")
     */
    private $createdat;

    /**
     *
     * @Accessor(getter="getupdated")
     * @Expose
     */
    private $updatedat;

    /**
     *
     * @Accessor(getter="getdeleted")
     *
     */
    private $deletedat;

    public function getdeleted()
    {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }


    /**
     */
    protected $username;

    /**
     */
    protected $password;


    /**
     * @Expose
     * @Accessor(getter="getPixUrl",setter="setPixUrl")
     */
    private $pixUrl;

    /**
     * Get userId
     *
     */
    public function getPixUrl()
    {
//        return $this->getUploadDir();
        return $this->pix ? $this->getUploadDir() . 'user/' . $this->pix : $this->getUploadDir() . "default/default_avatar.png";
    }

    protected function getUploadDir()
    {

        return 'https://' . $_SERVER['SERVER_NAME'] . '/uploads/';
    }

    /**
     * @Expose
     * @Accessor(getter="getParentIds")
     * @SerializedName("user")
     */
    private $userId;

    /**
     * Get actorGroup
     *
     */
    public function getParentIds()
    {
        return $this->parent ? $this->parent->getId() : null;
    }


    /**
     * Set userType
     *
     * @param string $userType
     * @return User
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }


    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /** Get userType
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }


    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set gender
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }




}
