<?php


    namespace FinFlow\NewsBundle\Repository;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;


    class Article extends EntityRepository
    {

        public function getNewsArticles($limit=null)
        {
            $qb = $this->createQueryBuilder('a');
            $qb->join('a.user','user');
            $qb->addSelect('user');
            $qb->Where('a.publish = :pub');

            $qb->setParameter('pub', true)
                ->orderBy('a.createdAt', 'DESC');
            if($limit!=null){
                $qb->setMaxResults($limit);

            }
            $all=$qb->getQuery()->getArrayResult();

            $qb->andWhere('a.top = :top');
            $qb->setParameter('pub', true);
            $qb->setParameter('top', true)

                ->orderBy('a.updatedAt', 'DESC');

            $top=$qb->getQuery()->getArrayResult();
            return compact('all','top');
        }

    }