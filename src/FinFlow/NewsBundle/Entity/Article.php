<?php

    namespace FinFlow\NewsBundle\Entity;


    use DateTime;
    use Doctrine\ORM\Mapping as ORM;
    use Exception;
    use FinFlow\UserBundle\Entity\User;
    use JMS\Serializer\Annotation as Serializer;
    use JMS\Serializer\Annotation\ExclusionPolicy;
    use JMS\Serializer\Annotation\Expose;
    use JMS\SerializerBundle\Annotation\Exclude;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\File\UploadedFile;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;
    use Symfony\Component\Validator\Constraints as Assert;

    use Gedmo\Mapping\Annotation as Gedmo;

    use JMS\Serializer\Annotation\Type;


    /**
     * Article
     * @Vich\Uploadable
     * @ORM\Table (name="article")
     * @ORM\Entity(repositoryClass="FinFlow\NewsBundle\Repository\Article")
     * @ExclusionPolicy("all")
     * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
     * @ExclusionPolicy("all")
     *
     */
    class Article
    {

        /**
         *
         * @ORM\Column(name="id", type="integer", nullable=false)
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         * @Expose
         */
        private $id;


        /**
         * @var string
         * @Assert\NotBlank(message = "Title can not be Blank")
         * @ORM\Column(name="title", type="string", length=255)
         */
        private $title;

        /**
         * @ORM\Column(type="string", unique=true)
         * @Gedmo\Slug(fields={"title"})
         */
        private $slug;

        /**
         * @var string
         *
         * @ORM\Column(name="publish", type="boolean", nullable=true)
         */
        private $publish;


        /**
         * @var string
         *
         * @ORM\Column(name="top", type="boolean", nullable=true)
         */
        private $top;


        /**
         * @var string
         * @Assert\NotBlank(message = "Body Cannot be Empty")
         * @ORM\Column(name="body", type="text",nullable=true)
         */
        private $body;


        /**
         * @ORM\Column(name="pix", type="string", length=255, nullable=true)
         * @Serializer\Exclude()
         * @Expose
         */
        private $pix;

        /**
         * NOTE: This is not a mapped field of entity metadata, just a simple property.
         *
         * @Vich\UploadableField(mapping="teaser_image", fileNameProperty="pix")
         *
         * @var File
         * @Serializer\Exclude()
         */
        private $imageFile;

        /**
         * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
         * of 'UploadedFile' is injected into this setter to trigger the  update. If this
         * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
         * must be able to accept an instance of 'File' as the bundle will inject one here
         * during Doctrine hydration.
         *
         * @param File|UploadedFile $image
         * @return Candidate
         * @throws Exception
         */
        public function setImageFile(File $image = null)
        {
            $this->imageFile = $image;
            if ($image) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
                $this->updatedAt = new DateTime('now');
            }
            return $this;
        }

        /**
         * @return File
         */
        public function getImageFile()
        {
            return $this->imageFile;
        }


        /**
         *
         * @ORM\ManyToOne(targetEntity="\FinFlow\UserBundle\Entity\User")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
         * })
         */
        private $user;


        /**
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="created_at", type="datetime",nullable=true)
         * @Type("DateTime<'Y-m-d H:i:s'>")
         */
        private $createdAt;

        /**
         * @Gedmo\Timestampable(on="update")
         * @ORM\Column(name="updated_at", type="datetime",nullable=true)
         * @Type("DateTime<'Y-m-d H:i:s'>")
         */
        private $updatedAt;

        /**
         * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
         */
        private $deletedAt;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set name
         * @param string $name
         * @return Town
         */
        public function setName($name)
        {
            $this->name = $name;
            return $this;
        }

        /**
         * Get name
         *
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set createdAt
         *
         * @param DateTime $createdAt
         * @return Town
         */
        public function setCreatedAt($createdAt)
        {
            $this->createdAt = $createdAt;

            return $this;
        }

        /**
         * Get createdAt
         *
         * @return DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Set updatedAt
         *
         * @param DateTime $updatedAt
         * @return Article
         */
        public function setUpdatedAt($updatedAt)
        {
            $this->updatedAt = $updatedAt;

            return $this;
        }

        /**
         * Get updatedAt
         *
         * @return DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * Set deletedAt
         *
         * @param DateTime $deletedAt
         * @return Town
         */
        public function setDeletedAt($deletedAt)
        {
            $this->deletedAt = $deletedAt;

            return $this;
        }

        /**
         * Get deletedAt
         *
         * @return DateTime
         */
        public function getDeletedAt()
        {
            return $this->deletedAt;
        }


        public function __toString()
        {
            return (String)$this->title;
        }

        /**
         * @return mixed
         */
        public function getPix()
        {
            return $this->pix;
        }

        /**
         * @param mixed $pix
         */
        public function setPix($pix)
        {
            $this->pix = $pix;
        }

        /**
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * @param string $title
         */
        public function setTitle($title)
        {
            $this->title = $title;
        }

        /**
         * @return string
         */
        public function getPublish()
        {
            return $this->publish;
        }

        /**
         * @param string $publish
         */
        public function setPublish($publish)
        {
            $this->publish = $publish;
        }

        /**
         * @return string
         */
        public function getTop()
        {
            return $this->top;
        }

        /**
         * @param string $top
         */
        public function setTop($top)
        {
            $this->top = $top;
        }

        /**
         * @return string
         */
        public function getBody()
        {
            return $this->body;
        }

        /**
         * @param string $body
         */
        public function setBody($body)
        {
            $this->body = $body;
        }

        /**
         * @return mixed
         */
        public function getSlug()
        {
            return $this->slug;
        }

        /**
         * @param mixed $slug
         */
        public function setSlug($slug)
        {
            $this->slug = $slug;
        }

        /**
         * Set user.
         *
         * @param User|null $user
         *
         * @return Article
         */
        public function setUser(User $user = null)
        {
            $this->user = $user;

            return $this;
        }

        /**
         * Get user.
         *
         * @return User|null
         */
        public function getUser()
        {
            return $this->user;
        }


    }
