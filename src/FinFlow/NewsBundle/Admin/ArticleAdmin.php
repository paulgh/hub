<?php

    declare(strict_types = 1);

    namespace FinFlow\NewsBundle\Admin;

    use Sonata\AdminBundle\Admin\AbstractAdmin;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;
    use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

    final class ArticleAdmin extends AbstractAdmin
    {
        protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
        {
            $datagridMapper
                ->add('id')
                ->add('title')
                ->add('publish')
                ->add('top')
                ->add('body');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->add('Teaser', null, array(
                    'template' => 'FinFlowNewsBundle:Default:teaserImage.html.twig',
                    'attr' => array('style' => 'width: 54.171%; ')))
                ->add('title')
                ->add('publish')
                ->add('top')
                ->add('user',null,array('label'=>'Author'))
//            ->add('body')
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ]);
        }

        protected function configureFormFields(FormMapper $formMapper): void
        {


            $fileFieldOptions = array();
            if (null != $this->getSubject()) {
                $image = $this->getSubject()->getPix();
                // use $fileFieldOptions so we can add other options to the field
                $fileFieldOptions = array('required' => false);
                $logoFieldOptions = array('required' => false);
                if ($image) {
                    // get the container so the full path to the image can be set
                    $container = $this->getConfigurationPool()->getContainer();
                    $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/teaser_image/' . $image;
                    // add a 'help' option containing the preview's img tag
                    $fileFieldOptions['help'] = '<a class="fancybox" href="' . $fullPath . '"> <img  width="150px" src="' . $fullPath . '"
                            class="admin-preview" /></a>';
                }
            }
            $formMapper
                ->add('imageFile', 'file', array(
                    'label' => 'Image',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => false), $fileFieldOptions);
            $formMapper
                ->add('title')
                ->add('body', 'Trsteel\CkeditorBundle\Form\Type\CkeditorType', array(
//                                    'config' => array('toolbar' => 'full'),
                ))
                ->add('publish')
                ->add('top');
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            $showMapper
//            ->add('id')
//            ->add('title')
//            ->add('publish')
//            ->add('top')
//            ->add('body')
//            ->add('pix')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt')
            ;
        }

        public function prePersist($object)
        {
            $user = $this->getUser();
            $object->setUser($user);

        }



        public function preUpdate($object)
        {
            $user = $this->getUser();
            $object->setUser($user);
        }

        public function getUser()
        {
            $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
            return $securityContext->getToken()->getUser();
        }
    }
