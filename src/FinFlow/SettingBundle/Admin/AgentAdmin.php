<?php

declare(strict_types=1);

namespace FinFlow\SettingBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class AgentAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('tin', null, array('label' => 'Tax Identification Number'))
            ->add('businessPhone')
            ->add('issuanceCountry', null, array('label' => 'Agent is Organized under the Laws of (state/country)'))
            ->add('placeOfBusCountry', null, array('label' => 'Agent’s Principal Place of Business is in (state/country)'))
            ->add('country')
            ->add('region')
            ->add('town')
            ->add('address', null, array('label' => 'Address ,Postal Code and others'))
            ->add('regBrokerDelear', null, array('label' => 'Ghana SEC registered broker-dealer?'))
            ->add('regInvestAdvisor', null, array('label' => 'Ghana SEC registered investment adviser?'))
            ->add('quaProfFundMan', null, array(
                    'label' => 'corporate, retirement plan or  “plan assets”')
            );
    }


    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('tin', null, array('label' => 'Tax Identification Number'))
            ->add('businessPhone')
            ->add('issuanceCountry', null, array('label' => ' Organized under the Laws of'))
            ->add('placeOfBusCountry', null, array('label' => ' Place of Business is in '))
            ->add('country')
            ->add('region')
            ->add('town')
            ->add('address', null, array('label' => 'Address,Postal Code and others'))
            ->add('regBrokerDelear', null, array('label' => 'SEC registered broker-dealer?'))
            ->add('regInvestAdvisor', null, array('label' => 'SEC registered investment adviser?'))
            ->add('quaProfFundMan', null, array(
                    'label' => 'corporate, retirement plan or “plan assets”')
            )
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Agent’s Information, Tax ID, Jurisdiction of Organization ', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('name')
            ->add('tin', null, array('label' => 'Tax Identification Number'))
            ->add('businessPhone')
            ->add('issuanceCountry', null, array('label' => 'Agent is Organized under the Laws of (state/country)'))
            ->add('placeOfBusCountry', null, array('label' => 'Agent’s Principal Place of Business is in (state/country)'))
            ->add('regBrokerDelear', null, array('label' => 'Agent a Ghana SEC registered broker-dealer'))
            ->add('regInvestAdvisor', null, array('label' => 'Agent a Ghana SEC registered investment adviser?'))
            ->add('quaProfFundMan', null, array(
                'label' => 'Entity is a corporate or other retirement plan or its assets are “plan assets”:
                Is the Agent a “Qualified Professional Fund Manager” in respect of NPRA rules and guidelines?'))
            ->end()
            ->with('Address', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('country')
            ->add('region')
            ->add('town')
            ->add('address', null, array('label' => 'Address ,Postal Code and others'));
    }


    public function prePersist($object)
    {
        $user = $this->getUser();
        $object->setUser($user);

    }

    public function preUpdate($object)
    {
        $user = $this->getUser();
        $object->setUser($user);
    }


    public function getUser() {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        return $securityContext->getToken()->getUser();
    }

}
