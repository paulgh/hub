<?php

declare(strict_types=1);

namespace FinFlow\SettingBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class MediaUploadAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('imageName')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('description', null, array(
            ))
            ->add('imageName')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('description', null, array())
            ->add('imageFile', 'file', array(
                'label' => 'Upload files(.pdf,.docx,.xls) 12M max',
                'required' => false,
                'data_class' => null,
                'help' => '(.pdf,.docx,) 12M'
            ))
            ->add('imageName', null, array('disabled'=>1,'label'=>'file name'))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('imageName')
            ->add('updatedAt')
        ;
    }
}
