<?php

declare(strict_types=1);

namespace FinFlow\SettingBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class PrimeBrokerAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('accountName')
            ->add('accountNumber')
            ->add('clearingFirm')
            ->add('businessPhone')
            ->add('address')

        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name')
            ->add('accountName')
            ->add('accountNumber')
            ->add('clearingFirm')
            ->add('businessPhone')
            ->add('address')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name',null,array(
        'error_bubbling' => true,
    ))
            ->add('accountName',null,array(
            ))
            ->add('accountNumber',null,array(
            ))
            ->add('clearingFirm',null,array(
            ))
            ->add('businessPhone',null,array(
            ))
            ->add('address')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
//        $showMapper
//            ->add('id')
//            ->add('name')
//            ->add('accountName')
//            ->add('accountNumber')
//            ->add('clearingFirm')
//            ->add('businessPhone')
//            ->add('address')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt')
        ;
    }
}
