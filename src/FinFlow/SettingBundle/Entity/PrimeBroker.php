<?php

namespace FinFlow\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\ClientBundle\Entity\PersonalInfo;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;

/**
 * AccountType
 *
 * @ORM\Table (name="prime_broker")
 * @ORM\Entity(repositoryClass="FinFlow\SettingBundle\Repository\AccountTypeRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("accountNumber")
 * @UniqueEntity("businessPhone")
 * @UniqueEntity("accountName")
 */
class PrimeBroker
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;


    /**
     * @var PersonalInfo
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo",inversedBy="personalPrimeBroker",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $personalInfo;

    /**
     * @return PersonalInfo
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * @param PersonalInfo $personalInfo
     */
    public function setPersonalInfo($personalInfo)
    {
        $this->personalInfo = $personalInfo;
    }

    /**
     * @Assert\NotBlank(message="Enter the Full name for Prime Broker")
     * @Assert\Length(max= 100, maxMessage="country.error.name_maxlength", groups={"default_country"})
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     */
    private $name;


    /**
     * @Assert\NotBlank(message="Enter the Account name for Prime Broker")
     * @ORM\Column(name="account_name", type="string")
     * @Expose
     */
    private $accountName;

    /**
     * @Assert\NotBlank(message="Enter the Account Number for Prime Broker")
     * @ORM\Column(name="account_number", nullable=true)
     * @Expose
     */
    private $accountNumber;


    /**
     * @Assert\NotBlank(message="Enter the clearing firm for Prime Broker")
     * @ORM\Column(name="clearing_firm", type="string",nullable=true)
     */
    private $clearingFirm;

    /**
     * @Assert\NotBlank(message="Enter the business number for prime broker")
     *  @MisdAssert\PhoneNumber()
     * @ORM\Column(name="business_phone", type="string",nullable=true)
     * @Expose
     */
    private $businessPhone;

    /**
     * @ORM\Column(name="address", type="text",  nullable=true)
     * @Expose
     */
    private $address;


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
        return (String)$this->name.' ('.$this->accountNumber.')';
    }
    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PrimeBroker
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set accountName.
     *
     * @param string $accountName
     *
     * @return PrimeBroker
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;

        return $this;
    }

    /**
     * Get accountName.
     *
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Set accountNumber.
     *
     * @param string $accountNumber
     *
     * @return PrimeBroker
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber.
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set clearingFirm.
     *
     * @param string $clearingFirm
     *
     * @return PrimeBroker
     */
    public function setClearingFirm($clearingFirm)
    {
        $this->clearingFirm = $clearingFirm;

        return $this;
    }

    /**
     * Get clearingFirm.
     *
     * @return string
     */
    public function getClearingFirm()
    {
        return $this->clearingFirm;
    }

    /**
     * Set businessPhone.
     *
     * @param string $businessPhone
     *
     * @return PrimeBroker
     */
    public function setBusinessPhone($businessPhone)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    /**
     * Get businessPhone.
     *
     * @return string
     */
    public function getBusinessPhone()
    {
        return $this->businessPhone;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return PrimeBroker
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return PrimeBroker
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return PrimeBroker
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return PrimeBroker
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
