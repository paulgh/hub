<?php

namespace FinFlow\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * @ORM\Table(name="media_upload")
 * @ORM\Entity()
 */
class MediaUpload {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     * @Assert\NotBlank(message="Add a Description to this file")
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     */
    private $createdAt;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="media_files", fileNameProperty="imageName")
     * @var File $imageFile
     * @Assert\File(
     *     maxSize = "12M",
     *     mimeTypesMessage = "File size too large"
     * )
     * @Expose
     */

    protected $imageFile;

    /**
     * @var \PersonalInfo
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\ClientBundle\Entity\PersonalInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $uploadBearerFiles;

    /**
     * @var \PersonalInfo
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\ClientBundle\Entity\PersonalInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sig_personal_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $personalInfoSignFiles;

    /**
     * @ORM\Column(type="string", length=255, name="image_name",nullable=true)
     *
     * @var string $imageName
     */
    protected $imageName;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     * )
     *
     * @var \DateTime $updatedAt
     */
    protected $updatedAt;

    /**
     * @return \PersonalInfo
     */
    public function getPersonalInfoSignFiles()
    {
        return $this->personalInfoSignFiles;
    }

    /**
     * @param \PersonalInfo $personalInfoSignFiles
     */
    public function setPersonalInfoSignFiles($personalInfoSignFiles)
    {
        $this->personalInfoSignFiles = $personalInfoSignFiles;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @throws \Exception
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return \PersonalInfo
     */
    public function getUploadBearerFiles()
    {
        return $this->uploadBearerFiles;
    }

    /**
     * @param \PersonalInfo $uploadBearerFiles
     */
    public function setUploadBearerFiles($uploadBearerFiles)
    {
        $this->uploadBearerFiles = $uploadBearerFiles;
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName) {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName() {
        return $this->imageName;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Media
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }



}
