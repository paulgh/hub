<?php

namespace FinFlow\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;

/**
 * AccountType
 *
 * @ORM\Table (name="agent")
 * @ORM\Entity(repositoryClass="FinFlow\SettingBundle\Repository\AccountTypeRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("tin")
 */
class Agent
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;


    /**
     * @var \entityPriBene
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo",inversedBy="principal"))
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_pri_bene_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $personalInfo;


    /**
     * @Assert\NotBlank(message="Name field cannot be blank")
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @Expose
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="tin", type="string", length=255, nullable=true)
     * @Expose
     */
    private $tin;

    /**
     *@Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="issuance_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $issuanceCountry;

    /**
     *@Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="place_bus_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $placeOfBusCountry;

    /**
     *@Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $country;

    /**
     *@Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $region;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="\FinFlow\LocationBundle\Entity\Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $town;

    /**
     * @Assert\NotBlank(message="Enter the business number")
     *  @MisdAssert\PhoneNumber()
     * @ORM\Column(name="business_phone", type="string", nullable=true)
     * @Expose
     */
    private $businessPhone;

    /**
     * @ORM\Column(name="address", type="text",  nullable=true)
     * @Expose
     */
    private $address;

    /**
     * @ORM\Column(name="reg_broker_delear", type="boolean")
     * @Expose
     */
    private $regBrokerDelear;

    /**
     * @ORM\Column(name="reg_invest_advisor", type="boolean")
     * @Expose
     */
    private $regInvestAdvisor;

    /**
     * @ORM\Column(name="qua_prof_fund_man", type="boolean")
     * @Expose
     */
    private $quaProfFundMan;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
        return (String)$this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {

    }




    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Agent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tin.
     *
     * @param string|null $tin
     *
     * @return Agent
     */
    public function setTin($tin = null)
    {
        $this->tin = $tin;

        return $this;
    }

    /**
     * Get tin.
     *
     * @return string|null
     */
    public function getTin()
    {
        return $this->tin;
    }

    /**
     * Set businessPhone.
     *
     * @param string $businessPhone
     *
     * @return Agent
     */
    public function setBusinessPhone($businessPhone)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    /**
     * Get businessPhone.
     *
     * @return string
     */
    public function getBusinessPhone()
    {
        return $this->businessPhone;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return Agent
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set regBrokerDelear.
     *
     * @param bool $regBrokerDelear
     *
     * @return Agent
     */
    public function setRegBrokerDelear($regBrokerDelear)
    {
        $this->regBrokerDelear = $regBrokerDelear;

        return $this;
    }

    /**
     * Get regBrokerDelear.
     *
     * @return bool
     */
    public function getRegBrokerDelear()
    {
        return $this->regBrokerDelear;
    }

    /**
     * Set regInvestAdvisor.
     *
     * @param bool $regInvestAdvisor
     *
     * @return Agent
     */
    public function setRegInvestAdvisor($regInvestAdvisor)
    {
        $this->regInvestAdvisor = $regInvestAdvisor;

        return $this;
    }

    /**
     * Get regInvestAdvisor.
     *
     * @return bool
     */
    public function getRegInvestAdvisor()
    {
        return $this->regInvestAdvisor;
    }

    /**
     * Set quaProfFundMan.
     *
     * @param bool $quaProfFundMan
     *
     * @return Agent
     */
    public function setQuaProfFundMan($quaProfFundMan)
    {
        $this->quaProfFundMan = $quaProfFundMan;

        return $this;
    }

    /**
     * Get quaProfFundMan.
     *
     * @return bool
     */
    public function getQuaProfFundMan()
    {
        return $this->quaProfFundMan;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return Agent
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Agent
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Agent
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set issuanceCountry.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $issuanceCountry
     *
     * @return Agent
     */
    public function setIssuanceCountry(\FinFlow\LocationBundle\Entity\Country $issuanceCountry = null)
    {
        $this->issuanceCountry = $issuanceCountry;

        return $this;
    }

    /**
     * Get issuanceCountry.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getIssuanceCountry()
    {
        return $this->issuanceCountry;
    }

    /**
     * Set placeOfBusCountry.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $placeOfBusCountry
     *
     * @return Agent
     */
    public function setPlaceOfBusCountry(\FinFlow\LocationBundle\Entity\Country $placeOfBusCountry = null)
    {
        $this->placeOfBusCountry = $placeOfBusCountry;

        return $this;
    }

    /**
     * Get placeOfBusCountry.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getPlaceOfBusCountry()
    {
        return $this->placeOfBusCountry;
    }

    /**
     * Set country.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $country
     *
     * @return Agent
     */
    public function setCountry(\FinFlow\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region.
     *
     * @param \FinFlow\LocationBundle\Entity\Region|null $region
     *
     * @return Agent
     */
    public function setRegion(\FinFlow\LocationBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \FinFlow\LocationBundle\Entity\Region|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set town.
     *
     * @param \FinFlow\LocationBundle\Entity\Town|null $town
     *
     * @return Agent
     */
    public function setTown(\FinFlow\LocationBundle\Entity\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town.
     *
     * @return \FinFlow\LocationBundle\Entity\Town|null
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set personalInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\PersonalInfo|null $personalInfo
     *
     * @return Agent
     */
    public function setPersonalInfo(\FinFlow\ClientBundle\Entity\PersonalInfo $personalInfo = null)
    {
        $this->personalInfo = $personalInfo;

        return $this;
    }

    /**
     * Get personalInfo.
     *
     * @return \FinFlow\ClientBundle\Entity\PersonalInfo|null
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }
}
