<?php

namespace FinFlow\SettingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function getAccTyByIdAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if($request->get('id')){
            return $this->getDoctrine()->getRepository("SettingBundle:AccountType")->find($request->get('id'));
        }
    }

}
