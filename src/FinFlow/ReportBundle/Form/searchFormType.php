<?php

namespace FinFlow\ReportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class searchFormType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
//     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('year', EntityType::class, [
            'class' => 'FinFlowElectionBundle:Year',
            'attr' => ['class' => 'yearResults'],
            'label' => ' ',
        ]);

        $builder->add('region', EntityType::class, [
            'required'=>false,
            'class' => 'LocationBundle:Region',
                'attr' => ['class' => 'regionResults'],
                'label' => 'By Region ',
             'placeholder' => 'All Regions',

            ]
            )
            ->add('constituency', EntityType::class,[
            'class' => 'LocationBundle:Constituency',
                'attr' => ['class' => 'constituencyResults'],
            ]
            )

//            ->add('Constituencies', ModelAutocompleteType::class, [
//                'class' => 'LocationBundle:',
//                'property' => 'title'
//            ])


        ;





    }

    /**
     * @return string
     */
    public function getName() {
        return 'year';
    }

    public function setDefaultOptions(\Symfony\Component\Form\OptionsResolverInterface $resolver)
    {
        // TODO: Implement setDefaultOptions() method.
    }

}
