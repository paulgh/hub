<?php

namespace FinFlow\ReportBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManager;

/**
 * Campaign Handler
 * @DI\Service("reportbundle.Service.handler")
 */
class ReportServiceHandler {

    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;
    private $connection;
    private $container;

//    private $surveyManager;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     */
    public function __construct(ObjectManager $em, FormFactoryInterface $formFactory, $connection, $container, $securityContext, $coreAdminManager) {
        $this->em = $em;
        $this->context = $securityContext;
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    public function getCountries() {
        $result = array();
        $repo = $this->em->getRepository('BeneficialOwner.php');
        $qb = $repo->createQueryBuilder('c')
            ->orWhere('c.priority = :priority')
            ->setParameter('priority', true);
        $countrys = $qb->getQuery()->getResult();
        foreach ($countrys as $country) {
            $result[] = $country->getId();
        }
        return $result;
    }

    public function dashboardMenu() {
        $repo = $this->em->getRepository('ReportBundle:Dashboard');
        $qb = $repo->createQueryBuilder('c');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getSql($sql, $filters) {
//        $filters=["user"=>$this->getUser()->getId()]
        $sqlArs = explode(PHP_EOL, $sql);

        foreach ($sqlArs as $key => $value) {
            $sqlArs[$key] = $this->fixWhere($value, $filters);
        }
        $sql = implode(' ', $sqlArs);

        $statement = $this->connection->prepare($sql);
//        dump($filters);
//        dump($statement);
//        EXIT;
        $statement->execute();
        $results = $statement->fetchAll();
        return $results;
    }

    function fixWhere($sqlAr, $filters) {

        $parsed = $this->get_string_between($sqlAr, '{', '}');
        $where = $wheres = $sqlWhere = NULL;
        if ('' != $parsed) {
            $wheres = explode(',', $parsed);
            $sqlString = preg_replace('/\{[^\]]+\}/', '[SQL]', $sqlAr);
            foreach ($wheres as $where) {
                preg_match_all('/(?!\b)(:\w+\b)/', $where, $bindValues);

                if (isset($bindValues[0][0])) {
                    $bindValue = str_replace(':', '', $bindValues[0][0]);

                    if (isset($filters[$bindValue]) && '' != $filters[$bindValue]) {
                        if (null == $sqlWhere) {
                            $sqlWhere = ' where ';
                                $where = str_replace(array('--and', '--or'), '', $where);
                        }
                        $sqlWhere .= ',' . $where;
                    }
                }else
                {
                    if (null == $sqlWhere) {
                        $sqlWhere = ' where ';
                        $where = str_replace(array('--and', '--or'), '', $where);
                    }
                    $sqlWhere .= ',' . $where;
                }
            }


            $sarr = explode(',', $sqlWhere);
            $illigalAfterWhere = array('--OR', '--AND', '--or', '--and', ',');
            if (isset($sarr[1])) {
                $sarr[1] = str_replace($illigalAfterWhere, '', $sarr[1]);
                $sqlString = implode(' ', $sarr);
            }
            $sqlString = preg_replace('/\[[^\]]+\]/', $sqlWhere, $sqlString);

            if ($filters) {
                foreach ($filters as $key => $filter) {
//                    dd($filter);

                    if (is_array($filter)){
                        $filter =implode(', ', $filter);
                    }
                    $sqlString = str_replace(':' . $key, $filter, $sqlString);
                }
            }
            $sqlAr = $sqlString;
        }
        $sqlAr = str_replace('--', '', $sqlAr);
        $sqlAr = str_replace('|', ',', $sqlAr);
        return $sqlAr;
    }

    function checkIfFirstWhereNot($sqlWhere) { // if the first where statement is not part of the filter
        $illigalAfterWhere = array('OR', 'AND', 'or', 'and', ','); // words that should not be available just after where statement
        $regex = '/(?<=\bwhere \s)(?:[\w-]+)/is';
        preg_match($regex, $sqlWhere, $matches);
        $result = array_intersect($matches, $illigalAfterWhere);

        if (!empty($result)) {
            $sqlWhere = str_replace($illigalAfterWhere, '', $sqlWhere);
        }

        return $sqlWhere;
    }

    function get_string_between($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function getColumns($dashId)
    {
        return $this->em->getRepository('ReportBundle:Columns')
            ->findBy(['dashboard' => $dashId]);
    }

    public function getQueryReports($dashboardId = null)
    {
        return $this->em->getRepository('ReportBundle:Dashboard')->getReports($dashboardId);
    }

    public function getMainReports($projectId=null)
    {
        return $this->em->getRepository('ReportBundle:Dashboard')->getReports('main', $projectId);
    }

//    public function getQueryReports($dashboardId = null, $projectId = null)
//    {
//        return $this->em->getRepository('ReportBundle:Dashboard')->getReports('query', $projectId, $dashboardId);
//    }

}