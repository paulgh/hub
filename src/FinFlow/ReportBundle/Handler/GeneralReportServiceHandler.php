<?php

namespace FinFlow\ReportBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use FinFlow\ActorBundle\Entity\Farm;
use FinFlow\SettingBundle\Entity\Product;
use FinFlow\SettingBundle\Entity\Service;
use FinFlow\TransactionBundle\Entity\ActorAggregateSaleItem;
use FinFlow\TransactionBundle\Entity\ActorForecastSaleItem;
use FinFlow\TransactionBundle\Entity\ActorForecastServiceItem;
use FinFlow\TransactionBundle\Entity\ActorTransactionServiceItem;
use FinFlow\TransactionBundle\Entity\TargetActivity;
use FinFlow\TransactionBundle\Entity\TargetCategory;
use FinFlow\TransactionBundle\Entity\TargetedValue;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManager;

/**
 * Campaign Handler
 * @DI\Service("general.Report.Service.handler")
 */
class GeneralReportServiceHandler {

    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;
    private $connection;
    private $container;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     */
    public function __construct(ObjectManager $em, FormFactoryInterface $formFactory, $connection, $container, $securityContext, $coreAdminManager) {
        $this->em = $em;
        $this->context = $securityContext;
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    public function getServices()
    {
        $services = $this->em->getRepository(Service::class)->findBy(['id' => [2, 20, 21]]);

        return $services;
    }

    public function getProducts()
    {
        $products = $this->em->getRepository(Product::class)->findAll();

        return $products;
    }

    public function getSHFsTargetedPerService($gender = null, $product = null, $service = null)
    {
        $data = $this->em->getRepository(ActorForecastServiceItem::class)->getFarmersTargeted($gender, $product, $service);

        return $data;
    }

    public function getSHFsReachedPerService($gender = null, $product = null, $service = null, $filtersData = [])
    {
        $data = $this->em->getRepository(ActorTransactionServiceItem::class)->getFarmersReached($gender, $product, $service, $filtersData);

        return $data;
    }

    public function getSHFsTargetedPerSale($gender = null, $product = null)
    {
        $data = $this->em->getRepository(ActorForecastSaleItem::class)->getFarmersTargeted($gender, $product);

        return $data;
    }

    public function getSHFsReachedPerSale($gender = null, $product = null)
    {
        $data = $this->em->getRepository(ActorAggregateSaleItem::class)->getFarmersReached($gender, $product);

        return $data;
    }

    public function getAcresTargeted($gender, $product, $service)
    {
        $data = $this->em->getRepository(ActorForecastServiceItem::class)->getAcresTargeted($gender, $product, $service);

        return $data;
    }

    public function getAcresServed($gender, $product, $service, $filtersData = [])
    {
        $data = $this->em->getRepository(ActorTransactionServiceItem::class)->getAcresServed($gender, $product, $service, $filtersData);

        return $data;
    }

    public function getUnitCostOfService($product = null, $service = null, $filtersData = [])
    {
        $data = $this->em->getRepository(ActorTransactionServiceItem::class)->getUnitCostOfService($product, $service, $filtersData);

        return $data;
    }

    public function getProjectedInvestment($product = null, $service = null)
    {
        $data = $this->em->getRepository(ActorForecastServiceItem::class)->getProjectedInvestment($product, $service);

        return $data;
    }

    public function getActualInvestment($product = null, $service = null, $filtersData = [])
    {
        $data = $this->em->getRepository(ActorTransactionServiceItem::class)->getActualInvestment($product, $service, $filtersData);

        return $data;
    }

    public function getAcresAllocatedForCultivation($gender = null, $product = null, $filtersData = [])
    {
        $data = $this->em->getRepository(Farm::class)->getAcresAllocatedForCultivation($gender, $product, $filtersData);

        return $data;
    }

    public function getClusters($product = null)
    {
        $data = $this->em->getRepository(Farm::class)->getClusters($product);

        return $data;
    }

    public function getGroups($product = null)
    {
        $data = $this->em->getRepository(Farm::class)->getGroups($product);

        return $data;
    }

    public function getIndividuals($product = null)
    {
        $data = $this->em->getRepository(Farm::class)->getIndividuals($product);

        return $data;
    }

    public function getQuantityOfInputDelivered($gender = null, $product = null, $service = null, $filtersData = [])
    {
        $data = $this->em->getRepository(ActorTransactionServiceItem::class)->getQuantityOfInputDelivered($gender, $product, $service);

        return $data;
    }


    /**
     * @param TargetActivity $activity
     * @param TargetedValue $targetValue
     * @param TargetCategory $category
     * @param string $subCategory
     * @param $filtersData
     * @return string
     */
    public function getData($activity, $targetValue, $category, $subCategory, $filtersData)
    {

        $methods = [
            "# of SHFs targeted" => 'getSHFsTargetedData',
            "# of SHFs reached" => 'getSHFsReachedData',
            "# of Acres targeted" => 'getAcresTargetedData',
            "# of Acres served" => 'getAcresServedData',
            "Quantity (kg) of input targeted" => 'getQuantityOfInputTargetedData',
            "Quantity (kg) of input delivered" => 'getQuantityOfInputDeliveredData',
            "Unit cost (kg) of input" => 'getUnitCostOfServiceData',
            "Quantity (litres) of input targeted" => 'getQuantityOfInputTargetedData',
            "Quantity (litres) of input delivered" => 'getQuantityOfInputDeliveredData',
            "Unit cost (litres) of input" => 'getUnitCostOfServiceData',
            "Quantity (25 kg bag) of input targeted" => 'getQuantityOfInputTargetedData',
            "Quantity (25 kg bag) of input delivered" => 'getQuantityOfInputDeliveredData',
            "Unit cost (25 kg bag) of input" => 'getUnitCostOfServiceData',
            "Quantity (50 kg bag) of input targeted" => 'getQuantityOfInputTargetedData',
            "Quantity (50 kg bag) of input delivered" => 'getQuantityOfInputDeliveredData',
            "Unit cost (50 kg bag) of input" => 'getUnitCostOfServiceData',
            "Quantity (gram) of input targeted" => 'getQuantityOfInputTargetedData',
            "Quantity (50 gram) of input delivered" => 'getQuantityOfInputDeliveredData',
            "Unit cost (gram) of input" => 'getUnitCostOfServiceData',
            "Unit cost of service" => 'getUnitCostOfServiceData',
            "Projected investment" => 'getTargetedInvestmentData',
            "Actual investment" => 'getActualInvestmentData',
        ];

        if($activity->getService()){

            if (isset($methods[$category->getName()])){

                $method = $methods[$category->getName()];

                return $this->{$method}($activity, $targetValue, $subCategory, $filtersData);
            }
        }

        return '-';
    }

    /**
     * @param $subCategory
     * @param TargetedValue $targetValue
     * @param TargetActivity $activity
     * @return mixed
     */
    public function getSHFsTargetedData($activity, $targetValue, $subCategory, $filtersData)
    {
        switch ($subCategory) {
            case 'Male':
                return $targetValue->getTargetedMaleFarmers();
            case 'Female':
                return $targetValue->getTargetedFemaleFarmers();
            case 'Total':
                return $targetValue->getTargetedMaleFarmers() + $targetValue->getTargetedFemaleFarmers();
        }
    }

    /**
     * @param TargetActivity $activity
     * @param TargetedValue $targetValue
     * @param $subCategory
     * @param $filtersData
     * @return mixed
     */
    public function getSHFsReachedData($activity, $targetValue, $subCategory, $filtersData)
    {

        $subCategory = ($subCategory == 'Total') ? null : strtolower($subCategory);

        return $this->getSHFsReachedPerService(strtolower($subCategory), $targetValue->getProduct()->getId(), $activity->getService(), $filtersData);

    }

    /**
     * @param $subCategory
     * @param TargetedValue $targetValue
     * @param TargetActivity $activity
     * @return mixed
     */
    public function getAcresTargetedData($activity, $targetValue, $subCategory, $filtersData)
    {

        switch ($subCategory) {
            case 'Male':
                return $targetValue->getTargetedMaleAcres();
            case 'Female':
                return $targetValue->getTargetedFemaleAcres();
            case 'Total':
                return $targetValue->getTargetedMaleAcres() + $targetValue->getTargetedFemaleAcres();
        }
    }

    /**
     * @param TargetActivity $activity
     * @param TargetedValue $targetValue
     * @param $subCategory
     * @param $filtersData
     * @return mixed
     */
    public function getAcresServedData($activity, $targetValue, $subCategory, $filtersData)
    {

        $subCategory = ($subCategory == 'Total') ? null : strtolower($subCategory);

        return $this->getAcresServed(strtolower($subCategory), $targetValue->getProduct()->getId(), $activity->getService(), $filtersData);
    }

    /**
     * @param TargetActivity $activity
     * @param TargetedValue $targetValue
     * @param $subCategory
     * @param $filtersData
     * @return mixed
     */
    public function getUnitCostOfServiceData($activity, $targetValue, $subCategory, $filtersData)
    {

        return $this->getUnitCostOfService($targetValue->getProduct(), $activity->getService(), $filtersData);
    }

    /**
     * @param $subCategory
     * @param TargetedValue $targetValue
     * @param TargetActivity $activity
     * @return mixed
     */
    public function getTargetedInvestmentData($activity, $targetValue, $subCategory, $filtersData)
    {

        return $targetValue->getTargetedInvestment();
    }

    /**
     * @param $subCategory
     * @param TargetedValue $targetValue
     * @param TargetActivity $activity
     * @return mixed
     */
    public function getActualInvestmentData($activity, $targetValue, $subCategory, $filtersData)
    {
        return $this->getActualInvestment($targetValue->getProduct(), $activity->getService(), $filtersData);
    }

    /**
     * @param $subCategory
     * @param TargetedValue $targetValue
     * @param TargetActivity $activity
     * @return mixed
     */
    public function getQuantityOfInputTargetedData($activity, $targetValue, $subCategory, $filtersData)
    {

        switch ($subCategory) {
            case 'Male':
                return $targetValue->getTargetedMaleAcres();
            case 'Female':
                return $targetValue->getTargetedFemaleAcres();
            case 'Total':
                return $targetValue->getTargetedMaleAcres() + $targetValue->getTargetedFemaleAcres();
        }
    }

    /**
     * @param TargetActivity $activity
     * @param TargetedValue $targetValue
     * @param $subCategory
     * @param $filtersData
     * @return mixed
     */
    public function getQuantityOfInputDeliveredData($activity, $targetValue, $subCategory, $filtersData)
    {

        $subCategory = ($subCategory == 'Total') ? null : strtolower($subCategory);

        return $this->getQuantityOfInputDelivered(strtolower($subCategory), $targetValue->getProduct()->getId(), $activity->getService(), $filtersData);
    }

}