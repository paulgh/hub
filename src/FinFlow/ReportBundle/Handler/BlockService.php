<?php

namespace FinFlow\ReportBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use FinFlow\ApiBundle\Classes\Util;
use Symfony\Component\Form\FormFactoryInterface;
use Lg\ActorBundle\Form\SyncDetailType;
use Lg\ActorBundle\Form\SyncDataType;
use Doctrine\ORM\EntityManager;

/**
 * Campaign Handler 
 * @DI\Service("block.Service")
 */
class BlockService {

    private $entityClass;
    private $repository;
    private $formFactory;
    private $context;
    private $connection;
    private $container;

//    private $surveyManager;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     * @param ObjectManager $em
     * @param FormFactoryInterface $formFactory
     * @param $connection
     * @param $container
     * @param $securityContext
     * @param $coreAdminManager
     */
    public function __construct(ObjectManager $em, FormFactoryInterface $formFactory, $connection, $container, $securityContext, $coreAdminManager) {
        $this->em = $em;
        $this->context = $securityContext;
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    public function getFields($entity) {
//        dump($entity);
//        exit;
        $fieldId = [];
        $indicator = $this->em->getRepository('ReportBundle:Indicator')->findBy(['name'=>$entity]);
//        dump($indicator);
//        exit;
        if(!is_null($indicator))
        {
            $indicator = $indicator[0];
            $indicatorFieds = $indicator->getIndicatorField();
            foreach ($indicatorFieds as $indicatorFied) {
                $fieldId[] = ['field'=>$indicatorFied->getField()->getId(),'indicator'=>$indicator->getId()];
            }
        }
        
        return $fieldId;
    }

    public function getPlantedSpecies()
    {
        return $this->em->getRepository('SolidaridadBundle:TreeInformation')->countSpecies();
    }

    public function getAllSpecies()
    {
        return $this->em->getRepository('SolidaridadBundle:Species')->getSpeciesDashboard();
    }

}
