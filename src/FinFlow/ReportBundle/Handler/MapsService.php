<?php

namespace FinFlow\ReportBundle\Handler;

use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Campaign Handler
 * @DI\Service("maps.Service")
 */
class MapsService {

    private $context;
    private $connection;
    private $container;
    private $em;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     * @param ObjectManager $em
     * @param FormFactoryInterface $formFactory
     * @param $connection
     * @param $container
     * @param $securityContext
     * @param $coreAdminManager
     */
    public function __construct(ObjectManager $em, FormFactoryInterface $formFactory, $connection, ContainerInterface $container, $securityContext, $coreAdminManager) {
        $this->em = $em;
        $this->context = $securityContext;
        $this->syncDatas = array();
        $this->container = $container;
        $this->connection = $connection;
        $this->coreAdminManager = $coreAdminManager;
    }

    public function getDetails($class, $fields, $filters) {
        //Get entity fields
        $entityFields = $this->em->getClassMetadata($class)->getFieldNames();

        $locations = null;

        //Check if entity has longitude and latitude as fields
        if (array_intersect(array('longitude', 'latitude'), $entityFields)) {

            $data = $this->getFilteredData($class, $filters);

            if ($data) {
                foreach ($data as $datum) {
                    $locations[] = array(
                        'longitude' => $datum['longitude'],
                        'latitude' => $datum['latitude'],
                        'info' => $this->getInfo($class, $fields, $datum),
                    );
                }

                $locations = json_encode($locations, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            }
        }

        return $locations;
    }

    /**
     * Generate an info for each marker on the map
     *
     * @param $class
     * @param array $fields
     * @param $data
     * @return string
     * @internal param object $entityObject
     */
    private function getInfo($class, $fields, $data) {
        $entityObject = $this->em->getRepository($class)->find($data['id']);

        // Remove unwanted fields
        $fields = array_diff($fields, array('id', 'batch', '_action'));

        $info = '';
        foreach ($fields as $field) {
            $field = explode("_", $field);
            $fieldName = ucfirst($field[0]);
            $method_name = 'get' . ucfirst($field[0]);
            for ($i = 1; $i < sizeof($field); $i++) {
                $method_name .= ucfirst($field[$i]);
                $fieldName .= ' ' . ucfirst($field[$i]);
            }
//            dump($method_name);
//            exit;

            $method_value = $entityObject->$method_name();

            if (is_a($method_value, 'DateTime')) {

                $value = $method_value->format('Y-m-d');
            } elseif ($method_value instanceof \Doctrine\ORM\PersistentCollection) {

                $value = '';
                foreach ($method_value as $item) {

                    $value .= $item . ', ';
                }
                $value = rtrim(trim($value), ',');
            } else {

                $value = $method_value;
            }

            $info .= '<b>' . $fieldName . '</b>: ' . $value . '<br>';
        }


        return $info;
    }

    /**
     * Filter the data passed to the map based
     *
     * @param $class
     * @param $filters
     * @return mixed
     */
    private function getFilteredData($class, $filters) {
        $qb = $this->em->getRepository($class)->createQueryBuilder('table');

        //Remove item where values are null
        $filters = $this->removeNullValueInArray($filters);

        foreach ($filters as $field => $value) {

            //Remove item where values are null
            if (is_array($value)) {
                $value = $this->removeNullValueInArray($value);
            }

            if ($value) {
                if (is_array($value) && array_key_exists("start", $value) && array_key_exists("end", $value)) {
                    $startDate = $value['start'];
                    $endDate = $value['end'];

                    $qb->andWhere(
                                    $qb->expr()->between("table.$field", ':start', ':end')
                            )
                            ->setParameter('start', $startDate)
                            ->setParameter('end', $endDate);
                } else {

                    $qb->andwhere("table.$field=:value");
                    $qb->setParameter('value', $value);
                }
            }
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param $value
     * @return array
     */
    private function removeNullValueInArray($value): array {
        return array_filter($value, function ($var) {
            return !is_null($var);
        });
    }

    public function drawFarmsLocations($farms) {
//        if(is_array($farms)){
//            $farmIds=[];
//            foreach ($farms as $farm) {
//                
//            }
//            
//    }
//       return $farms[0];
//       exit;
//        if ($farmerID) {
//
//            //Get single entity field
//            $farmers = $this->em->getRepository('FinFlowActorBundle:Actor')->findBy(['id' => $farmerID]);
//        } else {
//
//            //Get entity fields
//            $farmers = $this->em->getRepository('FinFlowActorBundle:Actor')->findAll();
//        }
//
//        $locations = [];
//
//        if ($farmers) {
//
//            $i = 0;
//
//            foreach ($farmers as $farmer) {
//                $farms = $farmer->getFarms();
        $i = 0;
        $locations = [];
        if (count($farms) > 0) {

            foreach ($farms as $farm) {


//                        if ($farmID AND $farmID != $farm->getId()) {
//                            continue;
//                        }

                $farmInfo = $farm->getActor() ? $this->getFarmInfo($farm) : '';

                $farmsInformation = $farm->getFarmInformation();
                $locations[$i]['info'] = $farmInfo;

                $locations[$i]['farmID'] = $farm->getId();
                foreach ($farmsInformation as $farmInformation) {

                    $longitude = $farmInformation->getLongitude();
                    $latitude = $farmInformation->getLatitude();

                    if (!$longitude OR ! $latitude) {
                        continue;
                    }



                    $locations[$i]['locations'][] = array(
                        'lat' => (float) $latitude,
                        'lng' => (float) $longitude
                    );
                }

                //Duplication the first location as last item
                if (!empty($locations[$i]['locations'])) {
                    $locations[$i]['locations'][] = $locations[$i]['locations'][0];
                }


                $i++;
            }
        }
//            }
//        }

        $locations = json_encode($locations, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

//        dump($locations);
//        exit;
        return $locations;
    }

    private function getFarmInfo($farm) {
        return "
                            <b>Farmer</b>: " . $farm->getActor() . ", <br>
                            <b>Community</b>: " . $farm->getActor()->getCommunity() . ", <br>
                            <b>Farm Size</b>: " . $farm->getFarmSize() . " acres, <br>
                            <b>Commodity</b>: " . $farm->getProduct() . ", <br>
                           <b>Year</b>: " . $farm->getYear() . ", <br>";
    }

}
