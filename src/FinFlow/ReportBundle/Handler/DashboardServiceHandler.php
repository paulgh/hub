<?php

namespace FinFlow\ReportBundle\Handler;

use Doctrine\DBAL\Connection;
use JMS\DiExtraBundle\Annotation as Di;
use Doctrine\Common\Persistence\ObjectManager;
use FinFlow\ReportBundle\Classes\DashboardRawQuery;
use FinFlow\UserBundle\Entity\User;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Campaign Handler
 * @DI\Service("dashboard.Service.handler")
 */
class DashboardServiceHandler
{

    private $context;
    private $connection;
    private $container;
    private $em;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * "formFactory" = @DI\Inject("form.factory"),
     *     "connection" = @DI\Inject("database_connection"),
     * "container" = @DI\Inject("service_container", required = false),
     *     "securityContext" = @DI\Inject("security.context", required = false),
     * "coreAdminManager" = @DI\Inject("core.admin.manager", required = false)
     * })
     * @param ObjectManager $em
     * @param FormFactoryInterface $formFactory
     * @param Connection $connection
     * @param $container
     * @param $securityContext
     * @param $coreAdminManager
     */
    public function __construct(ObjectManager $em, FormFactoryInterface $formFactory, Connection $connection, $container, $securityContext, $coreAdminManager)
    {
        $this->em = $em;
        $this->context = $securityContext;
        $this->container = $container;
        $this->connection = $connection;
    }


    public function getMonthlySales()
    {
        $sales = $this->em->getRepository('TransactionBundle:ActorAggregate')->getMonthlySales($this->getOptions());

        $sales = $this->formatGraphResponse($sales);

//        dd($sales);

        return json_encode($sales, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }


    public function getMonthlyServices()
    {
        $services = $this->em->getRepository('TransactionBundle:ActorTransaction')->getMonthlyServices($this->getOptions());

        $services = $this->formatGraphResponse($services);

        return json_encode($services, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function getForecastService($options)
    {
        $sql = $this->getSql($options, 'getForecastService');

        return $this->getSQLResult($sql);
    }

    public function getForecastProduct($options)
    {
        $sql = $this->getSql($options, 'getForecastProduct');

        return $this->getSQLResult($sql);
    }

    public function getTotalServiceDebt($options)
    {
        $sql = $this->getSql($options, 'getTotalServiceDebt');

        $data = $this->getSQLResult($sql);

        return $data[0]['forecast_total'] - $data[0]['transaction_total'];
    }

    public function getTotalProductDebt($options)
    {

        $sql = $this->getSql($options, 'getTotalProductDebt');

        $data = $this->getSQLResult($sql);

        return $data[0]['forecast_total'] - $data[0]['aggregate_total'];
    }

    public function getShopSales($options)
    {
//        dump($options); exit;

        $sql = $this->getSqls($options, 'getShopSales');

        return $this->getSQLResult($sql);

    }

    public function getProductSales($options)
    {
        $sql = $this->getSqls($options, 'getProductSales');

        return $this->getSQLResult($sql);

    }

    public function getUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param $options
     * @param $method
     * @return string
     */
    private function getSql($options, $method): string
    {
        $sql = DashboardRawQuery::{$method}();

//        dump($sql); exit;
        /**
         * @var $user User
         */
        $user = $options['user'];

        $where = '';

        if (!$user->hasRole('ROLE_MADE')) {

            $userIds = [$user->getId()];

            if ($options['children']){

                $userIds = array_merge($userIds, $options['children']);
            }

            $queryParameters = implode(", ", $userIds);

            $where = ' WHERE user_id IN ('. $queryParameters .') ';
        }

        $sql = str_replace("WHERE_CLAUSE", $where, $sql);

        return $sql;
    }

    private function getSqls($options, $method): string
    {
        $sql = DashboardRawQuery::{$method}();

//        dump($sql); exit;
        /**
         * @var $user User
         */
        $user = $options['user'];

        $where = '';

        if (!$user->hasRole('ROLE_MADE')) {

            $userIds = [$user->getId()];

            if ($options['children']){

                $userIds = array_merge($userIds, $options['children']);
            }

            $queryParameters = implode(", ", $userIds);

            $where = ' WHERE fos_user.id IN ('. $queryParameters .') ';
        }

        $sql = str_replace("WHERE_CLAUSE", $where, $sql);

        return $sql;
    }

    /**
     * @param string $sql
     * @return mixed
     */
    private function getSQLResult(string $sql)
    {
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param array $items
     * @return array
     */
    private function formatGraphResponse(array $items): array
    {
        $labels = [];
        $data = [
            'payments' => [],
            'costs' => []
        ];

        foreach ($items as $sale) {
            $labels[] = $sale['period'];
            $data['payments'][] = $sale['payments'];
            $data['costs'][] = $sale['costs'];
        }

        $items = [
            'labels' => $labels,
            'data' => $data
        ];

        return $items;
    }

    public function getOptions()
    {

        $options = [
            'user' => $this->getUser(),
            'children' => null
        ];

        if (!$this->getUser()->getParent()){

            $children =  $this->em->getRepository('UserBundle:User')->findBy(['parent' => $this->getUser()]);

            $childrenIds = [];

            foreach ($children as $child){
                $childrenIds[] = $child->getId();
            }

            $options['children'] = $childrenIds;
        }

        return $options;
    }

}
