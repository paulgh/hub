function getCountryCodeFormCountry(uniqId,idObject,reqLocale,fields)
{
    
    /**********set Text Box***********/
        var textbox="<input type='text' id='ccode' name='ccode' style='width:50px' readonly>";
        
        if(!Array.isArray(fields))
        {
            $("#"+uniqId+"_"+fields).before(textbox); 
        }
        else
            {
                for(i=0;i<fields.length;i++)
                    {
                        $("#"+uniqId+"_"+fields[i]).before(textbox); 
                    }
            }
        
        
        
        
   /********get Country Code***********/     
        var CountryEle = $("#"+uniqId+"_country");
        $(document).delegate("#"+uniqId+"_country",'change',countryChanged()); 
        CountryEle.change(); 
         
    var countryName = null;
    var count=0;     
        function countryChanged(){
                return function () {
                    var countryId = $("#"+uniqId+"_country option:selected").val();
                    
                    
                    if( typeof countryId == "undefined" )
                        return;
                    
                    if( countryId == null || countryId == '' )
                        return;
                    if( $('#'+uniqId+'_latlng_input'))
                    {
                        var lat = $("#"+uniqId+"_latlng_lat").val();
                        var lng = $("#"+uniqId+"_latlng_lng").val();
                        if((lng ==''&& lat=='') || count>=2 ){

                            countryName=$("#"+uniqId+"_country option:selected").html();
                            $('#'+uniqId+'_latlng_input').val(countryName);

                             $('#'+uniqId+'_latlng_search_button').trigger( "click" );
                            
                        }
                         count++;
                    }
                    var objectId = ""+idObject+""
                    
                    var locale = reqLocale;
                    
                    var Bss = $("#"+uniqId+"_BSSs");
                    
                    var url = Routing.generate('core_get_country_code_from_country', 
                        { '_locale': locale, 'countryId': countryId,
                         _sonata_admin: 'reporting.location.admin.country',
                          id: objectId },true);
                    $.post(url, { countryId: countryId }, function(data){
                                   
                                   $('input[name=ccode]').val(data);
                                   
                    	
                    },"text");
                    
                    
                };
            }
            
            
}
        
        
        