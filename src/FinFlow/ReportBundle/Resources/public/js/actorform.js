
// Every link with an attribute data-actor-method
    $(document).delegate("a[data-actor-action]",'click', function (event) {
        event.preventDefault();
        var target = $(event.currentTarget);
        var actorAction = target.attr('data-actor-action');
        var action = target.attr('href');
        var farmerIds = new Array();
        var associationIds = new Array();
        $('input[name="actors"]').each(function () {
          if(this.checked && $(this).attr('data-user-type') == 'farmer'){
            farmerIds.push($(this).val());
          }

          if(this.checked && $(this).attr('data-user-type') == 'association'){
            associationIds.push($(this).val());
          }
        });

        if( farmerIds.length <= 0 && associationIds.length <= 0){
            alert('Please select at least one actor');
            return false;
        }
        // Create a form on click
        var form = $('<form/>', {
            style:  "display:none;",
            method: 'POST',
            action: action,
        }).append($('<input>', {
            'name': 'type',
            'value': actorAction,
            'type': 'hidden'
        }));
        form.append($('<input>', {
            'name': 'farmers',
            'value': farmerIds,
            'type': 'hidden'
        }));
        form.append($('<input>', {
            'name': 'associations',
            'value': associationIds,
            'type': 'hidden'
        }));

        form.appendTo(target);
        // Submit the form
        form.submit();
    });