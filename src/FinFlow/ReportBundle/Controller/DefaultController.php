<?php

    namespace FinFlow\ReportBundle\Controller;

    use FinFlow\Report\Entity\Project;
    use FinFlow\SettingBundle\Entity\Service;
    use FinFlow\SolidaridadBundle\Entity\OilPalmSwappFarmers;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use FinFlow\ReportBundle\Form\searchFormType;


    class DefaultController extends Controller

    {

        public function homepageAction(Request $request)
        {
            $form = $this->createForm(searchFormType::class, null);
            $year = null;
            $regionName = 'All';
            $flashMap=[];

            if ($request->getMethod() == Request::METHOD_POST) {
                $formPostValues = $request->request->get($form->getName());
                $year = $formPostValues['year'];
                $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')->findOneBy(['id' => $year]);
            } else {
                //when page load at default or with a change in the dropdown form field for an election year...
                $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')->findOneBy(['setAsHome' => true]);
                $year = $homeDefault->getId();
            }

            //get the HomeDefault result level type...(either at the Constituency level or Polling Station Level)
            $resLevel = $homeDefault->getResultLevel();


            //set the option ........
            $options = [
                'year' => $year,
                'region' => null,
                'constituency' => null,
            ];


            if ($resLevel === 'Constituency') {
                //constituency..

                //get the totals Population for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')->getVotesTotalByElection($options);
                //get the totals for candidate
                $constCandidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PartyBallot')->getYearConstituencyCandidateVotes($options);

                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getTotalSeats($options, null);

                $flashMap = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getYearConstituencyCandidateFlash('presi', $options, 'b.votes');

            } else {
                //get the totals Population for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')->getVotesTotalByElection($options);

                //get the totals for candidate
                $constCandidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getYearConstituencyCandidateVotes($options);

                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')
                    ->getTotalSeats($options, null);

                //for this list only the constituencies which is known as flash area...
                $flashLoc = $this->getDoctrine()->
                getRepository('LocationBundle:Constituency')->findBy(array('flashArea'=>true));

                $options['flashArea'] = $flashLoc;
                $flashMap = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getYearConstituencyCandidateFlash($options);

            }

            $news = $this->getDoctrine()->
            getRepository('FinFlowNewsBundle:Article')->getNewsArticles(6);

            return $this->render('@Report/Default/home.html.twig', array(
                'form' => $form->createView(),
                'homeDefault' => $homeDefault,
                'votePopulation' => $votePopulation,
                'candidates' => $constCandidateVote,
                'year' => $year,
                'seats' => $seats,
                'flash' => $flashMap,
                'regionName' => $regionName,
                'news' => $news,
                'resLevel' => $resLevel,


            ));
        }

        public function presidentialAction(Request $request)
        {

            $form = $this->createForm(searchFormType::class, null);
            $year = null;
            $region = null;
            $regionName = 'All';
            $candidateVote = array();
            $query = $this->getDoctrine()->getRepository('LocationBundle:Constituency');

            if ($request->getMethod() == Request::METHOD_POST) {
                $formPostValues = $request->request->get($form->getName());
                $year = $formPostValues['year'];
                $region = $formPostValues['region'];
                $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')
                    ->findOneBy(['id' => $year]);
                $regionName = $region ? $this->getDoctrine()->getRepository('LocationBundle:Region')
                    ->findOneBy(['id' => $region])->getName() : 'All';
                //get the totals number constitutencies;
                $constituencies = $region ? $query->findBy(['region' => $region]) : $query->findAll();
                $constituenciesCount = (count($constituencies));

            } else {
                //when page load at default or with a change in the dropdown form field for an election year...
                $homeDefault = $this->getDoctrine()
                    ->getRepository('FinFlowElectionBundle:Year')
                    ->findOneBy(['setAsHome' => true]);
                $year = $homeDefault->getId();
                $constituenciesCount = count($query->findAll());

            }


            $resLevel = $homeDefault->getResultLevel();
            //set the option ........
            $options = [
                'year' => $year,
                'region' => $region,
                'constituency' => null,
            ];

            if ($resLevel === 'Constituency') {
                //constituency..
                //get the totals Population for an election..
                //get the totals for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')->getVotesTotalByElection($options);
                //get the totals for candidate
                $candidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PartyBallot')->getYearConstituencyCandidateVotes($options);

                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getTotalSeats($options, null);

                //get the totals by election (year) and constituencies
                $constituenciesResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getYearConstituencyCandidate('presi', $options, 'b.votes');

            } else {
                //pollingStation..
                //get the totals Population for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')->getVotesTotalByElection($options);

                //get the totals for candidate
                $candidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getYearConstituencyCandidateVotes($options);
                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')
                    ->getTotalSeats($options, null);
                //TODO elections will have results in either polling station level or Constituency level
                $conResults = $this->getDoctrine()->
                getRepository('LocationBundle:Constituency')->getConstituencyResult($options);

                $options['conResults'] = $conResults;
                $constituenciesResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getCandidateTotalByConstituency($options);


            }


            //get the last update time from results... (cannot use above as result of sum and ordering.. use below)
            $latest = $this->getDoctrine()->
            getRepository('FinFlowElectionBundle:Result')
                ->findOneBy(array('year' => $year), array('updatedAt' => 'Desc'));
            $latest = $latest ? $latest->getUpdatedAt() : null;
            return $this->render('@Report/Default/presidential.html.twig', array(
                'form' => $form->createView(),
                'homeDefault' => $homeDefault,
                'votePopulation' => $votePopulation,
                'candidates' => $candidateVote,
                'year' => $year,
                'region' => $region,
                'constituenciesCount' => $constituenciesCount,
                'seats' => $seats,
                'resLevel' => $resLevel,
                'regionName' => $regionName,
                'constituenciesResult' => $constituenciesResult,
                'latest' => $latest

            ));


        }


        public function parliamentaryAction(Request $request)
        {
            $form = $this->createForm(searchFormType::class, null);
            $year = null;
            $region = null;
            $regionName = 'All';
            $candidateVote = array();
            $query = $this->getDoctrine()->getRepository('LocationBundle:Constituency');

            if ($request->getMethod() == Request::METHOD_POST) {
                $formPostValues = $request->request->get($form->getName());
                $year = $formPostValues['year'];
                $region = $formPostValues['region'];
                $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')
                    ->findOneBy(['id' => $year]);
                $regionName = $region ? $this->getDoctrine()->getRepository('LocationBundle:Region')
                    ->findOneBy(['id' => $region])->getName() : 'All';
                //get the totals number constitutencies;
                $constituencies = $region ? $query->findBy(['region' => $region]) : $query->findAll();
                $constituenciesCount = (count($constituencies));

            } else {
                //when page load at default or with a change in the dropdown form field for an election year...
                $homeDefault = $this->getDoctrine()
                    ->getRepository('FinFlowElectionBundle:Year')
                    ->findOneBy(['setAsHome' => true]);
                $year = $homeDefault->getId();
                $constituenciesCount = count($query->findAll());

            }


            $resLevel = $homeDefault->getResultLevel();
            //set the option ........
            $options = [
                'year' => $year,
                'region' => $region,
                'constituency' => null,

            ];


            if ($resLevel === 'Constituency') {
                //constituency..

                //get the totals Population for an election..
                //get the totals for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')->getVotesTotalByElection($options);

                //get the totals for candidate
                $candidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PartyBallot')->getYearConstituencyCandidateVotes($options);

                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getTotalSeats($options, 'parliament');

                //get the totals by election (year) and constituencies
                $constituenciesResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getYearConstituencyCandidate('presi', $options, 'b.votes');

                $latest = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->findOneBy(array('year' => $year), array('updatedAt' => 'Desc'));


//                dump('constituencies..');

//                dump($votePopulation);
//                dump($seats);
                dump($constituenciesResult);
//                dump($candidateVote);

//                exit;


                $latest = $latest ? $latest->getUpdatedAt() : null;



            } else {
                //pollingStation..

                //get the totals Population for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')->getVotesTotalByElection($options);

                //get the totals for candidate
                $candidateVote = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getYearConstituencyCandidateVotes($options);

                //get the totals seats won (year) and constituencies
                $seats = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')
                    ->getTotalSeats($options, null);

                //TODO elections will have results in either polling station level or Constituency level
                $conResults = $this->getDoctrine()->
                getRepository('LocationBundle:Constituency')->getConstituencyResult($options);

                dump($conResults);
                $options['conResults'] = $conResults;

                $constituenciesResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollParliamentaryBallot')->getCandidateTotalByConstituency($options);

                dump($votePopulation);
//                dump($candidateVote);
//                dump($seats);
                dump($constituenciesResult);

//                exit;
                $latest = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')
                    ->findOneBy(array('year' => $year), array('updatedAt' => 'Desc'));
                $latest = $latest ? $latest->getUpdatedAt() : null;


//                exit;
            }


//            exit;
            //get the last update time from results... (cannot use above as result of sum and ordering.. use below)


            return $this->render('@Report/Default/parliamentary.html.twig', array(
                'form' => $form->createView(),
                'homeDefault' => $homeDefault,
                'votePopulation' => $votePopulation,
                'candidates' => $candidateVote,
                'year' => $year,
                'region' => $region,
                'constituenciesCount' => $constituenciesCount,
                'seats' => $seats,
                'resLevel' => $resLevel,
                'regionName' => $regionName,
                'constituenciesResult' => $constituenciesResult,
                'latest' => $latest

            ));
        }


        public function resultDetailAction(Request $request)
        {
            $form = $this->createForm(searchFormType::class, null);
            //get the values from the urls...
            $year = $request->attributes->get('year');
            $constituency = $request->attributes->get('id');
            $constituencyName = $this->getDoctrine()->getRepository('LocationBundle:Constituency')->findOneBy(['id' => $constituency])->getName();
            $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')->findOneBy(['id' => $year]);

            //if user changes form values...
            if ($request->getMethod() == Request::METHOD_POST) {
                $formPostValues = $request->request->get($form->getName());
                $year = $formPostValues['year'];
                $constituency = $formPostValues['constituency'];
                $homeDefault = $this->getDoctrine()->getRepository('FinFlowElectionBundle:Year')->findOneBy(['id' => $year]);
                $constituencyName = $this->getDoctrine()->getRepository('LocationBundle:Constituency')->findOneBy(['id' => $constituency])->getName();
            }
            //set the option ........s
            $options = [
                'year' => $year,
                'constituency' => $constituency,
                'region' => null
            ];

            $resLevel = $homeDefault->getResultLevel();

            if ($resLevel === 'Constituency') {

                $viewToload='@Report/Default/constituencyDetail.html.twig';

                //get the totals for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')->getVotesTotalByElection($options);

                // issue of ordering reason for duplicates clean code up.... TODO
                $result = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getYearConstituencyCandidate(null, $options, 'b.votes');

                // issue of ordering reason for duplicates clean code up.... TODO
                $parliamentsResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:Result')
                    ->getYearConstituencyCandidate('parliament', $options, 'pb.votes');
                dump($result);
                //    exit;

            } else {

                $viewToload='@Report/Default/pollDetail.html.twig';

                //get the totals Population for an election..
                $votePopulation = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')->getVotesTotalByElection($options);

                //get the totals for candidate
                $presidentialResult = $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollPartyBallot')->getYearConstituencyCandidateVotes($options);

                //  issue of ordering reason for duplicates clean code up.... TODO
                $parliamentsResult =  $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollParliamentaryBallot')->getYearConstituencyCandidateVotes($options);

                //list all the poling results..
                $pollingResults =  $this->getDoctrine()->
                getRepository('FinFlowElectionBundle:PollingResult')->getPollingStationResult($options);

                $result=compact(
                    'presidentialResult','parliamentsResult','pollingResults');
                dump($votePopulation);
                dump($result);
//
//                    exit;
            }

            return $this->render($viewToload, array(
                'form' => $form->createView(),
                'homeDefault' => $homeDefault,
                'candidates' => $result,
                'parliamentsResult' => $result,
                'constituencyName' => $constituencyName,
                'year' => $year,
                'constituency' => $constituency,
                'votePopulation' => $votePopulation,
            ));
        }



        public function articleDetailAction(Request $request){
            $slug = $request->attributes->get('slug');
            $em = $this->getDoctrine()->getManager();
            $article = $em->getRepository('FinFlowNewsBundle:Article')
                ->findOneBy(array('slug' => $slug));
            $all = $em->getRepository('FinFlowNewsBundle:Article')
                ->getNewsArticles();

            return $this->render('@Report/Default/article.html.twig', array(
                'article' => $article,
                'all' => $all,
            ));
        }

        public function articleListAction(Request $request){
            $slug = $request->attributes->get('slug');
            $em = $this->getDoctrine()->getManager();
            $article = $em->getRepository('FinFlowNewsBundle:Article')
                ->findOneBy(array('slug' => $slug));
            $all = $em->getRepository('FinFlowNewsBundle:Article')
                ->getNewsArticles();
//            dump($all);
//            exit;
            return $this->render('@Report/Default/articleList.html.twig', array(
                'article' => $article,
                'all' => $all,
            ));
        }



        public function mapDetails($datas)
        {
            $i = 0;
            $mapData = array();
            foreach ($datas as $data) {
                $mapData[$i]['type'] = "Feature";
                $mapData[$i]['properties'] = array(
                    "dealerType" => 'test',
                    "popupContent" =>
                        ''
                        . '<strong> <h3 style="color:#e41266"></strong> ' . $data['name'] . ' : ' . 'ssssss' . '</h3>'
                        . '</div>'
                );
                $mapData[$i]['geometry'] = array("type" => "Point", "coordinates" => array(
                    floatval($data['longitude']), floatval($data['latitude'])
                ));
                $i++;
            }

            return $mapData;

        }
    }
