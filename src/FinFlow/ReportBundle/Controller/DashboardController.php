<?php

namespace FinFlow\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController;
use FinFlow\FarmerBundle\Form\FarmerSearchType;
use APY\DataGridBundle\Grid\Source\Vector;
use APY\DataGridBundle\Grid\Export\ExcelExport;
use Sonata\AdminBundle\Controller\CRUDController as cController;
use Ob\HighchartsBundle\Highcharts\Highchart;


class DashboardController extends cController {

    public function listAction(Request $request = null) {


        $context = $this->container->get('security.token_storage');

        $tableData = array(
            array(
                "name" => "bootstrap-table",
                "stargazers_count" => "526",
                "forks_count" => "122",
                "description" => "An extended Bootstrap table with radio, checkbox, sort"
            ),
            array("name" => "multiple-select",
                "stargazers_count" => "288",
                "forks_count" => "150",
                "description" => "A jQuery plugin to select multiple elements with checkboxes :)"
            ),
            array("name" => "blog",
                "stargazers_count" => "13",
                "forks_count" => "4",
                "description" => "my blog"
            ),
        );
        $graphData = array(
            array(
                'name' => 'Tokyo',
                'data' => array(49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4)
            )
            ,
            array(
                'name' => 'New York',
                'data' => array(83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3))
        );

        $mapMoreData = array(
            array("type" => "Feature",
                "properties" => array(
                    "popupContent" => "This is where the Paul play!"
                ),
                "geometry" => array(
                    "type" => "Point",
                    "coordinates" => [6.14055, -1.75781]
                )), array("type" => "Feature",
                "properties" => array(
                    "popupContent" => "This is where the Dela play!"
                ),
                "geometry" => array(
                    "type" => "Point",
                    "coordinates" => [8.14055, -3.75781]
        )));


        $searchForm = $this->createForm(new FarmerSearchType($context));
        $postData = $request->get('region');

        return $this->render('ReportBundle:Dashboard:firstReport.html.twig', array(
                    'mapMoreData' => json_encode($mapMoreData),
                    'searchForm' => $searchForm->createView(),
                    'counter' => 0,
                    'tableData' => json_encode($tableData),
                    'chart' => $this->getChart(' '),
                    'postData' => $postData,
                    'graphData' => json_encode($graphData),
                    'base_template' => $this->getBaseTemplate(),
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks')
        ));
    }

    public function getChart($data) {
        $series = array(
            array("name" => "Data Serie Name", "data" => array(1, 2, 4, 5, 6, 3, 8))
        );

        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
        $ob->chart->type('pie');
        $ob->exporting->enabled(true);
        $ob->title->text('Chart Title');
        $ob->xAxis->title(array('text' => "Horizontal axis title"));
        $ob->yAxis->title(array('text' => "Vertical axis title"));
        $ob->series($series);
        return $ob;
    }

}
