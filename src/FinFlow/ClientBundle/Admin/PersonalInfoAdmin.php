<?php

//declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Anacona16\Bundle\DependentFormsBundle\Form\Type\DependentFormsType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


final class PersonalInfoAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('fullName', null, array('label' => "Entity Name "))
            ->add('accountNumber', null, array('disabled' => 1, 'help' => "This will be auto-generated*"))
            ->add('naturalPerson', null, array('label' => "Entity a natural person(s)", 'help' => "i.e., an individual, or joint individuals?"))
            ->add('accountType', null, array("label" => "Account Entity Type"))
            ->add('business', null, array("label" => "Nature of Business", "required" => false))
            ->add('orgUnderLawCountry', null, array("label" => "Organised under the Laws of "))
            ->add('tin', null, array("label" => "Tax Identification Number", "help" => "(or, for any Non-Ghanaian Entity, any Government-Issued Identification Number):"))
            ->add('businessPhone')
            ->add('faxNumber')
            ->add('legalAddress', NULL, array('help' => "(used for tax reporting purposes,GhPost etc.):"))
            ->add('country')
            ->add('region', null, array())
            ->add('town', null, array("label" => "City/Town"))
            ->add('ghBroDealReg', null, array('label' => 'Gh. reg. broker-dealer'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('fullName', null, array('label' => "Entity Name "))
            ->add('accountNumber', null, array('disabled' => 1, 'help' => "This will be auto-generated*"))
            ->add('accountType')
            ->add('naturalPerson', null, array('editable' => true, 'label' => "Natural person(s) ", 'help' => "i.e., an individual, or joint individuals?"))
//            ->add('business', null, array("label" => "Nature of Business"))
//            ->add('orgUnderLawCountry', null, array("label" => "Organised under the Laws of "))
            ->add('ghBroDealReg', null, array('label' => 'Gh. reg. broker-dealer'))
            ->add('tin')
            ->add('businessPhone', null, array('editable' => true))
            ->add('faxNumber')
            ->add('country')
            ->add('Uploaded Docs.', null, array('template' => 'ClientBundle:Default:personalInfoDocList.html.twig'))
            ->add('totalNetWorth', null, array('template' => 'ClientBundle:Default:networthList.html.twig'))
            ->add('politicalAff', null, array('editable' => true, 'label' => 'Political Affiliation'))
            ->add('user',null,array('label'=>'created By'))
            ->add('_action', null, [
                'actions' => [
//                    'show' => [],
                    'edit' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();

        $imp = " <strong class='title-purple'>IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT<hr></strong>
                In accordance with government regulations, financial institutions are required to obtain, verify, and record information that identifies each person or entity that opens an account.
                What this means for you: When you open an account, we will ask for your name, address, government issued identification number and other information that will allow us to identify you. We may also require
                copies of documentation be provided to us.<br>
                Additional Required Documents:</p>
                1. Organizational / Authority Documents<hr>
                <div class='order-indent'>
                <ol type='a'>
                <li>Documentation showing/establishing the existence of entity (e.g., articles of incorporation, government-issued business license, partnership agreement, trust agreement, offering memorandum, prospectus, statutes, etc.); 
                in the case of Pension Plans and other Retirement Arrangements, plan documents and trust agreements and any of the items in 1(f) below; in addition, in the case of Governmental retirement programs, any statute, rule, regulation
                or constitutional provision authorizing or restricting investments or parties with whom the plan can transact;
                </li>
                <li>Documentation establishing authority to engage in securities and/or derivative transactions (e.g., internal investment policy, prospectus, trust agreement, Board certified minutes/resolutions, etc.);
               </li>
                <li>Signature authority of individual signing the enclosed documents;</li>
                <li>Government issued identification number (e.g. Driver’s License, Passport, Social Security Number, Ghana Card etc.);</li>
                <li>Current financial information (e.g., financial statements, assets under management, etc.); and</li>
                <li>Investment management agreement, with investment guidelines, if applicable.</li>
                </ol>
               </div>
                2. Trading Authorization (if applicable)<hr>
                If the Entity is designating an agent to place orders on its behalf (such as a hedge fund designating an investment manager), please complete the Third-Party Agent Authorization located 
                in Section C.1 of the attached application. The agent must also sign (in Section D), signifying acceptance of this authority.</p>
                3. Tax Forms (W-8 or W-9 for U.S. domiciled Individuals)<hr>
                Please complete the appropriate tax form and return to us along with the Application.";
        $AgentAuth = $this->getRequest()->getUriForPath('/Documents/Agent Auth Form.pdf');
        $signRequired = $this->getRequest()->getUriForPath('/Documents/SignaturePage.pdf');
        $resolution = $this->getRequest()->getUriForPath('/Documents/Resolution.pdf');
        $partnershipAuthorization = $this->getRequest()->getUriForPath('/Documents/PartnershipAuthorization.pdf');

        $formMapper
            // Name and Type of Entity Opening the Account:
//            ->tab('Notices')
//            ->with('Guidelines', array(
//                'class' => 'col-md-12 ',
//                'box_class' => 'box box-solid box-success border-purple',
//
//            ))
//            ->add('notices', null, array(
//                    'disabled' => 1,
//                    'label' => "This represents a No response",
//                    'help' => '
//                <hr>
//                    <div class="checkbox"><label class="">
//                    <div class="icheckbox_square-blue checked">
//                    <input type="checkbox" disabled="disabled" class="" value="1" style="position: absolute; opacity: 0;">
//                    <ins class="iCheck-helper"  disabled="disabled" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
//                    </div><span class="control-label__text">
//                    This indicates a  <b>Yes</b> response</span>
//                    </label>
//                    </div>
//                     <hr>
//                    <p>( * ) On forms represents a required Field</p><br>
//
//                   <strong class="title-purple">CUSTOMER WARNING<hr></strong>
//                    <p style="text-align: justify">Sarpong capital markets limited does not offer services or carry out any business with customers categorised as retail clients. You can only fill in, sign and deliver this new
//                    account application and agreement, if you are classified as a professional client or an eligible counterparty. Where you are classified as a professional client or an eligible
//                    counterparty you shall not benefit from certain investor
//                    protections which are available to retail clients under the applicable laws and regulations. We are entitled to assume that as a professional client you have the necessary experience and knowledge to understand the relevant risks
//                    involved in the products or services offered or demanded under this agreement or to be entered into
//                    between you and Sarpong Capital Markets Limited and, therefore, our obligation to assess the appropriateness thereof shall be deemed fulfilled.
//                    We are under no obligation to
//                    assess the appropriateness of services and financial instruments offered to or demanded by eligible counterparties. We do not offer investment advice.</p>
//
//                                  ' . $imp
//                )
//            )
//            ->end()
//            ->end()
            ->tab('General Info')
            ->with('Name and Type of Entity Opening the Account', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('naturalPerson', null, array('label' => "Entity a natural person(s)", 'help' => "i.e., an individual, or joint individuals"))
            ->add('civilStatus', "choice", array("required" => false, 'choices' => array(
                'Single' => 'Single',
                'Married/Cohabitant' => 'Married/Cohabitant',
                'Separated/Divorced' => 'Separated/Divorced',
                'Widower/Widow' => 'Widower/Widow'
            )))
            ->add('fullName', null, array('label' => "Name of Entity"))
            ->add('dob', null, array(
                'label' => 'Date of Birth',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('registeredPoolInv', "choice", array("required" => false, 'choices' => array(
                'N/A' => 'N/A',
                'No' => 'No',
                'Yes' => 'Yes'
            ), 'label' => "Unregistered Pooled investment vehicle*", 'help' => "Entity is an unregistered pooled investment vehicle (i.e., a mutual fund)"))
            ->add('accountType', "sonata_type_model", array("required" => 0, 'label' => 'Account Type *'))
            ->add('accountNumber', null, array('disabled' => 1, 'help' => "Only admins see this,This will be auto-generated*"))
            ->end()
            //contact
            ->with('Contact Information, Tax ID and Nature of Business/Occupation', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('business', null, array("label" => "Nature of Business*", 'required' => 0))
            ->add('occupation', null, array('label' => 'Occupation*'))
            ->add('residentCountry', "sonata_type_model", array("label" => "Country of Residence* ", 'required' => 0))
            ->add('national', "sonata_type_model", array("label" => "Nationality"))
            ->add('orgUnderLawCountry', "sonata_type_model", array("label" => "Entity is Organised under the Laws of *", 'required' => 0))
            ->add('tin', null, array("label" => "Tax Identification Number", "help" => "(or, for any Non-Ghanaian Entity, any Government-Issued Identification Number):"))
            ->add('businessPhone', null, array('required' => true,
                'help' => "Mobile Number should be with country code eg:<b> +233xxxxxxxxx </b>"))
            ->add('faxNumber')
            ->add('legalAddress', NULL, array('help' => "(used for tax reporting purposes):"))
            ->add('gps', null, array('label' => "Postal Code (Ghana Post GPS or Google Plus Code)"))
            ->add('country')
            ->add('region', DependentFormsType::class, array('required' => false, 'entity_alias' => 'region_by_country'
            , 'parent_field' => 'country', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
            ->add('town', DependentFormsType::class, array('required' => false, 'entity_alias' => 'town_by_region'
            , 'parent_field' => 'region', 'attr' => array('required' => false, 'class' => 'select2-offscreen', 'style' => 'width: 100%; ')), array('list' => 'list'))
            ->end()
            ->with('Mailing Address (if different):', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-danger',
            ))
            //Mailing Address
            ->add('sameAddr', null, array('label' => "Same As Provided Above"))
            ->add('mailAddress', null, array('label' => "Address"))
            ->add('mailAddCountry', null, array('label' => "Country"))
            ->add('mailAddRegion', "sonata_type_model", array("required" => 0, 'label' => "Region"))
            ->add('mailAddTown', "sonata_type_model", array("required" => 0, 'label' => "City/Town"))
            ->end()
            ->end()
            ->tab('Political, Broker-Dealer, CSD and Finance')
            ->with('Political Affiliation', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-info'
            ))
            ->add('politicalAff', null, array('label' => 'Political Affiliation'))
            ->add('politicalAffInfo', null, array('label' => 'Provide name(s) and political affiliation*', 'help' => ""))
            ->end()
            //Broker-Dealer Status:
            ->with('Broker-Dealer Status', array(
                'class' => 'col-md-12 broker-dealer',
                'box_class' => 'box box-solid box-info'
            ))
            ->add('ghBroDealReg', null, array('label' => 'Entity is a Ghanaian registered broker-dealer', 'help' => ""))
            ->end()
//             Central Securities Depository (CSD) Status:
            ->with('Central Securities Depository (CSD) Status:', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-info',
                "description" => ""
            ))
//            ->add('csdAccount', null, array('label' => 'I (entity) already holds a CSD Account'))
            ->add('requireCsdAcc', null, array('label' => ' I  (entity) requires a CSD Account'))
//            ->add('personalCsdAccountInfo', 'sonata_type_collection', array('label' => 'Central Securities Depository Information', 'required' => false, 'by_reference' => false)
//                , array('edit' => 'inline', 'inline' => 'table'))
            ->add('personalCsdAccountInfo', 'sonata_type_collection',
                array(
                    'error_bubbling' => false,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click to add CSD Info.',
                    'label' => false,
                    'type_options' => array(
                    )
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            //     Financial Situation of Entity:
            ->with('Financial Situation of Entity:', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-info',
            ))
            ->add('annualGrossIncome', null, array('required' => 0, 'label' => "Annual Gross Income (from all sources)*"))
            ->add('totalLiqAsset', null, array('required' => 0, 'label' => "Total Net Liquid Assets*"))
            ->add('totalNetWorth', null, array('required' => 0, 'label' => "Total Net Worth (total assets minus total liabilities)*"))
            ->end()
            ->end()
            ->tab('Pensions & Bearer Share Entity')
            ->with('Pensions Act and Employee Benefit Plan Information for Entity:', array(
                'class' => 'col-md-12 pensionAct',
                'box_class' => 'box box-solid box-info',
            ))
//            Pensions Act and Employee Benefit Plan Information for Entity:
            ->add('corpRetirePlan', null, array(
                    'label' => "Corporate Retirement Plan",
                    'help' => 'If Entity is a Corporate Retirement Plan (e.g., pension, profit sharing,or other plan), is the plan subject to the Pensions Act 2010 or
                 Income Tax Act 2015 as amended?')
            )
            ->add('otherRetirePlan', null, array('label' => 'Other Retirement Plan (describe type)'))
            ->add('otherRetirePlanActSubject', null, array(
                    'label' => " Other Retirement Plan is subject to the Pensions Act 2010 or Income Tax Act 2015 ",
                    'help' => "Is the other retirement plan subject to the Pensions Act 2010 or Income Tax Act 2015 as amended?")
            )
            ->add('otherEquity', null, array(
                    'help' => "Other Entity (e.g. corporation, partnership, trust, limited liability company), 
                the assets of which constitute “plan assets” subject to the Pensions Act or Income Tax Act 2015 as amended")
            )
            ->end()
            ->with('Bearer Share Entity:', array(
                'class' => 'col-md-12 bearerEntity',
                'box_class' => 'box box-solid box-info',
                "description" => "Bearer Share Entity: 
                This section must be completed by any new client that is a private investment vehicle (PIV), 
                personal holding company (PHC), Limited Liability Company (Ltd), limited partnership, charity or foundation. 
                A “Bearer Share Entity” is an entity, which, pursuant to the laws of the jurisdiction in which it was organized,
                is permitted to issue shares in bearer form, meaning that the ownership interest in the corporate entity is not 
                registered with the relevant regional authority, but rather resides with the person who physically possesses the 
                share certificates."
            ))
            ->add('bearerShareEntity', null, array('label' => "(a) Client is a Bearer Share Entity"))
            ->add('otherBearerShareOwned', null, array(
                    'label' => " (b) Is any entity that wholly or partially owns the client a Bearer Share Entity?",
                    'help' => '<br>If the answer is Yes, please indicate all entities in the client’s ownership structure that are Bearer Share Entities below.
                    <br><hr>
                    If you answered “Yes” <i>(checked) </i> to Question a or b, prior to opening an account, for each Bearer Share Entity in the ownership structure, you must provide the following:<br>
                    - organizational documents (e.g., articles of incorporation and/or memorandum of association);<br>
                    - the Share Register; and/or<br>
                    - Share Certificates<hr> <br>
                    If you answered “No” <i> (unchecked) </i>  to Questions (a) and (b)  and the client was organized in a Bearer Share Jurisdiction, prior to opening an account, you must provide the following:
                   <br> - client’s organizational documents;
                   <br> - the Share Register; and/or
                    - Share Certificates ')
            )
            ->add('uploadBearerFile', 'sonata_type_collection',
                array(
                    'label' => 'Upload Bearer Share Documents',
                    'error_bubbling' => false,
                    'required' => false,
                    'by_reference' => true,
                    'help' => '<br><hr>
                    * Please note that Sarpong Capital may require additional documentation for Bearer Share Entities, depending upon a review of the information provided.
                <br>
                <br>
               <p><strong>Investment Objective: Institutional and Professional Clients:</strong><hr>
                In lieu of providing an investment objective, the Entity represents that it (together with its agents, if applicable) has the capability to 
                independently evaluate investment risk and is exercising independent judgment in evaluating investment decisions in that its investment decisions will be based on its own 
                independent assessment of the opportunities and risks presented by a potential investment, market factors and other investment considerations. The Entity acknowledges that information provided
                by Sarpong Capital or any affiliate is not and will not form a primary basis for any investment decision.</p>
                 '
                )
                , array('edit' => 'inline', 'inline' => 'table'))
            ->end()
            ->end()
            ->tab('B. Principal & Beneficial Owner', array('description' => 'To comply with the Companies Code 1965, (Act 179) and its amendments, 
            securities laws and regulations and anti-money laundering laws, please complete the applicable sections on the next page on behalf of each 
            principal/authorized person/beneficial owner set forth below. In addition, please complete the applicable sections on the next page on behalf of each person named in an authorized signatory list, if one is provided to us. Sarpong Capital may, from time to time, ask for additional information or for information about additional Account Principals, Authorized Persons and Beneficial Owners of the Entity. If your Account is
             managed by an intermediary (e.g., an adviser), we may require information on the intermediary.
             * Non-Ghanaian banks must complete a “foreign bank certification”; if applicable, please contact your Sarpong Capital sales/marketing contact.'))
            ->with('Notification', array(
                'class' => 'col-md-12 hide prinBeneOwner',
                'box_class' => 'box box-solid box-success',
            ))
            ->end()
            ->with('Section I : Principals', array(
                'class' => 'col-md-12 hide secI',
                'box_class' => 'box box-solid box-info',
            ))
            ->add('principal', 'sonata_type_collection',
                array(
                    'error_bubbling' => false,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click to add principal',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                    )
                ),
                array(
                    'edit' => 'inline',
//                    'inline' => 'table',
                )
            )
            ->end()
            ->with('Section II : Beneficial Owners', array(
                'class' => 'col-md-12 hide secII',
                'box_class' => 'box box-solid box-info',
            ))
            ->add('beneficialOwner', 'sonata_type_collection',
                array(
                    'error_bubbling' => false,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click to add Beneficial Owners',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                    )
                ),
                array(
                    'edit' => 'inline',
//                    'inline' => 'table',
                ))
            ->end()
            ->end()
            ->tab('C. Special Products & Services')
            ->with('Third-Party Agent Appointment and Authorization:', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))

            ->add('thirdParty',null,array('label'=>'Third-Party Agent Appointment and Authorization',
                'help'=>'Check only if the Entity is authorizing a third party to act as agent. DO NOT COMPLETE FOR EMPLOYEES OF THE ENTITY '))
            ->add('trade', "choice", array("required" => false, 'choices' => array(
                    'Trade' => 'Trade',
                    'Trade and Broad Authority to Move Assets' => 'Trade and Broad Authority to Move Assets'
                ),
                    'label' => '',
                    'help' => 'The Entity hereby appoints and authorizes the third party (such as the investment manager of a hedge fund) listed below as its agent to do the following, as more fully
                            described in, and in accordance with, the Third-Party Agent Supplement and Authorization:<br>
                        <a>Trade: </a> To purchase, 
                    invest in, or otherwise acquire, exchange, transfer, borrow, lend, sell or otherwise dispose of and generally deal in and with, 
                    any and all forms of securities, security futures, swap agreements and/or security-based swap agreements, foreign currency, 
                    and all other products or transactions described in the Third-Party Agent Supplement and Authorization. 
                    (The Entity may not limit its authorization for trading to specific individuals who act on behalf of a third- party agent, 
                    but must instead authorize the third-party agent for trading.)<br><hr>
                    <a>Trade and Broad Authority to Move Assets:</a>  To authorize the withdrawal of funds and securities from the Entity’s Accounts,
                     in addition to the authority to trade as defined above. (The Entity may not limit its authorization for trading to specific 
                     individuals who act on behalf of a third-party agent, but must instead authorize the third-party agent for trading.)
                    '
                )
            )
            ->end()
            ->with('Agents Information', array(
                'class' => 'col-md-12 agentInfo hide',
                'box_class' => 'box box-solid box-success',
            ))

            ->add('agent', 'sonata_type_collection',
                array(
                    'help' => "<p class='help-warning'>Please note that the AGENT MUST SIGN Third-Party Agent Authorization form
                     <a href=\"$AgentAuth\" title='Agent Authorization Form' 
                      target='_blank'> Download File Here </a><br></p>",
                    'error_bubbling' => false,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click Agent Info',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                    )
                ),
                array(
                    'edit' => 'inline',
//                    'inline' => 'table',
                ))

            ->add('largeTraderAgent', null, array('label' => 'Agent is a Large Trader',
                    'help' => 'Agent is a Large Trader as that is exercising investment discretion in respect of 
            securities transaction in this account')
            )
            ->add('largeTraderAgentCSDID', null, array("required" => 0, 'label' => 'Provide large trader CSD ID(s) applicable to this account, including any suffixes *',
                )
            )
            ->end()
            ->with('Margin Account', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => "The Entity hereby applies for a Margin Account. In addition to other applicable agreements and supplements set forth herein, 
                the Entity acknowledges receipt of the Interest Charges and Margin Requirements Disclosure Statement, the Margin Risk Disclosure Statement, 
                and the Entity acknowledges that Sarpong Capital. may use, rehypothecate or transfer securities and other property held in the Entity’s Margin 
                Account in accordance with the attached New Account Agreement. The Entity agrees that neither Sarpong Capital. nor its affiliates has provided advice 
                relating to the tax consequences of the Margin Account. By transacting in the Margin Account, the Entity and any fiduciary acting on its behalf agree 
                that there is no applicable law, rule or regulation that would limit Sarpong Capital’s ability to exercise its rights in connection with the Margin Account. 
                Entity agrees that a Margin 
                Account will not be available should the Firm deny such application."
            ))
            ->add('marginAccount')
            ->end()
            ->with('Sarpong Capital to serve as Entity’s Prime Broker', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => "The Entity hereby applies for a Prime Brokerage Account. 
                In addition to other applicable agreements and supplements set forth herein, 
                the Entity agrees to abide by the Prime Brokerage Supplement. The list of executing 
                brokers below (or on an attached sheet) shall constitute the Entity’s schedule of authorized
                executing brokers where the Entity maintains accounts. The Entity agrees to provide Sarpong 
                Capital with written updates to this list."
            ))
            ->add('sarponAsPrimeBroker', null, array('label' => 'Sarpong Capital As Prime Broker'))
            ->add('personalPrimeBroker', 'sonata_type_collection',
                array(
                    'by_reference' => false,
                    'required' => true,
                    'error_bubbling' => false,
                    'btn_add' => ' Click to add executing brokers',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                        'error_bubbling' => false,
                    )
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with('Disclosures to Issuers and foreign tax authorities', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => '(Please check one or both boxes below if you do NOT
                 consent to the stated disclosure.)',
            ))
            ->add('disclosureIssuer', null, [
                'label' => 'The Entity OBJECTS to disclosure to issuers.',
                'help' => '<a>Disclosure to Issuers:</a> Sarpong Capital is 
                required to disclose to an issuer the name, address, and position of its customers 
                who are beneficial owners of that issuer’s securities unless the customer objects.
                 If the Entity objects, please check the box on the right'
            ])
            ->add('disclosureForeignTax', null, [
                'label' => 'The Entity OBJECTS to disclosure to foreign taxation authorities.',
                'help' => '<a>Disclosure to Foreign Taxation Authorities:</a> Unless the Entity objects, Sarpong Capital may disclose certain
                 information about the Entity to foreign taxation authorities from time to time in an effort to reduce the Entity’s withholding tax liability 
                 on certain foreign source income payments.<br>
                The information disclosed may consist of, among other things, the Entity’s name, address, tax identification number, tax domicile and the quantity 
                of the subject security / securities the Entity may hold. Additionally, unless the Entity objects, the Entity agrees to cooperate with any request
                for additional information or documentation about the Entity by a Foreign Taxation Authority seeking to verify the eligibility for the reduced 
                withholding rate. If the Entity objects, please check the box on the right.'
            ])
            ->end()
            ->with('Foreign Currency and Derivative Transactions', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => 'The Entity hereby applies for approval to enter into foreign exchange spot, forward, and 
                currency swap transactions, and options on such transactions. In addition to other applicable agreements and 
                supplements set forth herein, the Entity agrees to abide by the FX Trading Supplement. The Entity agrees to enter into a 
                Emerging Markets Traders Association (EMTA) Master Agreement, International Swaps and Derivatives Association (ISDA) Master 
                Agreement and ISDA Credit Support Annexes as required. If the Entity will engage in foreign exchange and FX derivative transactions 
                (other than spot transactions), Entity represents it is an “eligible market participant”, a “principal” or a “qualified investor” as 
                defined in the BOG Interbank Forex Market Guidelines 2019 and Section 37(2) of the SEC Guidelines for Private Funds SEC/GUI/002/04/2018. 
                By signing this New Account Agreement, the Entity agrees to being provided with advisory and structuring services in respect of foreign 
                currency and derivative transactions and for which a separate invoice for advisory and structuring services will be raised and submitted to Entity for payment',
            ))
            ->add('fxTransaction', null, ['label' => 'Foreign Currency and Derivative Transactions'])
            ->end()
            ->with('Money Market Transactions', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => 'The Entity hereby applies for approval to enter into money market, repurchase agreements (subject to execution of a Master Agreement), 
                Treasury/Interbank Deposits, Commercial Paper transactions. In addition to other applicable agreements and supplements set forth herein, the Entity agrees 
                to abide by the General Dealing Supplement. If the Entity will engage in money market transactions, Entity represents it is an “eligible market participant”,
                 a “principal” or a “qualified investor” as defined in the BOG Interbank Forex Market Guidelines 2019 and Section 37(2) of the SEC Guidelines for Private Funds SEC/GUI/002/04/2018.',
            ))
            ->add('moMarketTransaction', null, ['label' => 'Money Market Transactions'])
            ->end()
            ->with('Fixed Income Transactions', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description' => 'The Entity hereby applies for approval to enter into Fixed Income (subject to execution of a Master Agreement), transactions. 
                In addition to other applicable agreements and supplements set forth herein, the Entity agrees to abide by the General Dealing Supplement. 
                If the Entity will engage in fixed income transactions, Entity represents it is an “eligible market participant”, a “principal” or a “qualified investor” 
                as defined in the BOG Interbank Forex Market Guidelines 2019 and Section 37(2) of the SEC Guidelines for Private Funds SEC/GUI/002/04/2018.',
            ))
            ->add('fixedIncTransaction', null, ['label' => 'Fixed Income Transactions'])
            ->end()
            ->end()
            ->tab('Signature and Doc upload')
            ->with('Signatures Files ', array(
                'description' => "Kindly Download and sign the Appropriate file(s) and Upload in the Section Below ",
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-danger',
            ))
            ->add('doc', null, array(
                'label' => 'I have signed and Uploaded all required Files',
                'help' => "<p class='help-warning'>- Account signature form * (Required)<a href=\"$signRequired\" title='Account Signature Form'  target='_blank' > Download File </a></p>
                        <p> - Resolutions (complete the form of Resolutions if Entity is a limited liability company) <a href=\"$resolution\" title='Resolutions'  target='_blank' > Download File</a></p>
                       - Partnership Authorization (complete this form of Authorization if Entity is any form of Partnership) <a href=\"$partnershipAuthorization\" title='Partnership Authorization'  target='_blank' > Download File</a>
                        "
            ))
            ->end()
            ->with('upload Documents', array(
                'description' => " Upload all related and signed documents",
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-danger',
            ))
            ->add('uploadSignatureFiles', 'sonata_type_collection',
                array(
                    'label' => 'Upload Bearer Share Documents',
                    'required' => false,
                    'error_bubbling' => false,

                )
                , array('edit' => 'inline', 'inline' => 'table'))
            ->end()
            ->end();


        if ($user->hasRole('ROLE_STAFF') || $user->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->tab('Client Logins')
                ->with('Client User Account ', array(
                    'description' => "Select Client who need to have this Account. This Client should have a Login 
                Account to  track or make changes when needed to the Account ",
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-warning',
                ))
                ->add('client')
                ->end();
        }

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {

    }


    public function uploadMedia($param)
    {
        $files = array();
        $files2 = array();
//        for some unknown reason this supplies error when empty

//        if (is_array($param->getUploadSignatureFiles())) {
        foreach ($param->getUploadSignatureFiles() as $data1) {
            $data1->setPersonalInfoSignFiles($param);
            $files2[] = $data1->getImageFile() ? $data1->getImageFile()->getClientOriginalName() : $data1->getImageName();
//            }
        }

        $allFiles = array_merge($files2, $files);
        $allFiles = implode(",", $allFiles);
        $param->setMediaFilesNames($allFiles);
    }

    public function CsdAccountInfo($param)
    {
        foreach ($param->getPersonalCsdAccountInfo() as $data) {
            $data->setPersonalInfo($param);
        }
    }


    public function PrincipalsInfo($param)
    {
        foreach ($param->getPrincipal() as $data) {
            $data->setPersonalInfo($param);
        }
    }

    public function BeneficialOwnerInfo($param)
    {
        foreach ($param->getBeneficialOwner() as $data) {
            $data->setPersonalInfo($param);
        }
    }

    public function AgentInfo($param)
    {
        foreach ($param->getAgent() as $data) {
            $data->setPersonalInfo($param);
        }
    }

    public function personalPrimeBroker($param)
    {
        foreach ($param->getpersonalPrimeBroker() as $data) {
            $data->setPersonalInfo($param);
        }
    }

    public function prePersist($object)
    {
        $user = $this->getUser();
        $object->setUser($user);
        $this->uploadMedia($object);
        $this->CsdAccountInfo($object);
        $this->personalPrimeBroker($object);
        $this->AgentInfo($object);
        $this->BeneficialOwnerInfo($object);
        $this->PrincipalsInfo($object);
    }



    public function preUpdate($object)
    {
        $user = $this->getUser();
        $object->setUser($user);
        $this->personalPrimeBroker($object);
        $this->uploadMedia($object);
        $this->CsdAccountInfo($object);
        $this->AgentInfo($object);
        $this->BeneficialOwnerInfo($object);
        $this->PrincipalsInfo($object);

    }

    public function postPersist($object)
    {
//        $this->uploadMedia($object);
//        $this->CsdAccountInfo($object);

    }

    public function postUpdate($object)
    {
        $this->CsdAccountInfo($object);
//        $this->uploadMedia($object);

    }

    public function getUser()
    {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        return $securityContext->getToken()->getUser();
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'ClientBundle:Default:personalInfo.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function getUserChildren()
    {
        return $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getRepository("UserBundle:User")
            ->getChildren($this->getUser()->getId());
    }

    public function createQuery($context = 'list')
    {
        $parentResult = $this->getUserChildren();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);

        //get client data only
        if ($user->hasRole('ROLE_CLIENT')) {
            $query = $this->getModelManager()->createQuery($this->getClass(), 'f');
            $query->leftJoin('f.user', 'u');
            $query->where('u.id = :user_id')
                ->setParameter('user_id', $user->getId());
        }
        if ($user->hasRole('ROLE_STAFF')) {
            $query = $this->getModelManager()->createQuery($this->getClass(), 'f');
            $query->leftJoin('f.user', 'u');
            $query->where('u.id = :user_id')
                ->Orwhere('u.parent=:parentUserId')
                ->setParameter('user_id', $user->getId())
                ->setParameter('parentUserId', $user->getId());
        }

        //return empty for no roles.. hack access...
        if ($user->hasRole('')) {
            return null;
        }

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return $query;
        }

        return $query;
    }
}
