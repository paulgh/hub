<?php

declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class ProductServiceAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('trade')
            ->add('tradeBroadAuth')
            ->add('marginAccount')
            ->add('disclosureIssuer')
            ->add('disclosureForeignTax')
            ->add('fxTransaction')
            ->add('moMarketTransaction')
            ->add('fixedIncTransaction');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('personalInfo', null, ['label' => 'Entity'])
            ->add('trade')
            ->add('tradeBroadAuth', null, ['label' => 'Trade and Broad Authority to Move Assets'])
            ->add('marginAccount')
            ->add('disclosureIssuer')
            ->add('disclosureForeignTax')
            ->add('fxTransaction')
            ->add('moMarketTransaction')
            ->add('fixedIncTransaction')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
//                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper

            ->with('Notification', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-danger',
                'description'=>"Merge This with the Personal Information into news Tab  (New Account creation)"
            ))
            ->end()
            ->with('1a. Third-Party Agent Appointment and Authorization:', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('personalInfo', null, ['label' => 'Entity'])
            ->add('trade')
            ->add('tradeBroadAuth', null, ['label' => 'Trade and Broad Authority to Move Assets'])
            ->add('marginAccount')
            ->end()
            ->with('Agents Information', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description'=>"This should be merged with PrimeBroker into One entity"
            ))
            ->add('agent', 'sonata_type_model')
            ->end()
            ->with('3. Sarpong Capital to serve as Entity’s Prime Broker', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
                'description'=>"This should be merged with Agent into One entity"
            ))
            ->add('primeBroker',"sonata_type_model")
            ->end()
            ->with('', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('disclosureIssuer', null, ['label' => 'Disclosure to Issuers'])
            ->add('disclosureForeignTax', null, ['label' => 'Disclosure to Foreign Taxation Authorities'])
            ->add('fxTransaction', null, ['label' => 'Foreign Currency and Derivative Transactions'])
            ->add('moMarketTransaction', null, ['label' => 'Money Market Transactions'])
            ->add('fixedIncTransaction', null, ['label' => 'Fixed Income Transactions']);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
//            ->add('id')
//            ->add('fullname')
//            ->add('trade')
//            ->add('tradeBroadAuth')
//            ->add('marginAccount')
//            ->add('disclosureIssuer')
//            ->add('disclosureForeignTax')
//            ->add('fxTransaction')
//            ->add('moMarketTransaction')
//            ->add('fixedIncTransaction')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt')
        ;
    }
}
