<?php

declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class EntityPriBeneAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('personalInfo', null, array('label' => "Entity Name"))
            ->add('entityType');
//            ->add('charitable')
//            ->add('foundation')
//            ->add('governmentAgency')
//            ->add('hedgeFund')
//            ->add('llc')
//            ->add('mutualFund')
//            ->add('partnership')
//            ->add('pension')
//            ->add('otherPenson')
//            ->add('privateEquityFund')
//            ->add('trustInd')
//            ->add('trustCop')
//            ->add('privateCop')
//            ->add('piv')
//            ->add('publicComp')
//            ->add('spv')
//            ->add('uniHosHmo')
//            ->add('indJointOwn')
//            ->add('brokerDealer')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
//            ->add('id')
            ->add('personalInfo', null, array('label' => "Entity Name"))
            ->add('entityType', null, array('label' => "Entity Type(s)"))
//            ->add('bankCentral')
//            ->add('charitable')
//            ->add('foundation')
//            ->add('governmentAgency')
//            ->add('hedgeFund')
//            ->add('llc')
//            ->add('mutualFund')
//            ->add('partnership')
//            ->add('pension')
//            ->add('otherPenson')
//            ->add('privateEquityFund')
//            ->add('trustInd')
//            ->add('trustCop')
//            ->add('privateCop')
//            ->add('piv')
//            ->add('publicComp')
//            ->add('spv')
//            ->add('uniHosHmo')
//            ->add('indJointOwn')
//            ->add('brokerDealer')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Entity Information  ', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-info',
            ))
            ->add('personalInfo', null, array('label' => "Entity Name"))
            ->add('entityType', 'sonata_type_model', array())
            ->end()
            ->with('Section I : Principals', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-success',
            ))
            ->add('principal', 'sonata_type_collection',
                array(
                    'by_reference' => true,
                    'label' => false,
                    'btn_add' => ' Click to add principal(s)',
                    "required" => true
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('principal', 'sonata_type_collection',
                array(
                    'error_bubbling' => true,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click to add principal',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                    )
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                )
            )
            ->end()
            ->with('Section II : Beneficial Owners', array(
                'class' => 'col-md-12',
                'box_class' => 'box box-solid box-warning',
            ))
//            ->add('beneficialOwner', 'sonata_type_collection',
//                array(
//                    'label' => ' Click to add Beneficial Owners',
//                    'required' => false, 'by_reference' => false)
//                , array('edit' => 'inline', 'inline' => 'table')
//            )
            ->add('beneficialOwner', 'sonata_type_collection',
                array(
                    'error_bubbling' => true,
                    'by_reference' => false,
                    'required' => true,
                    'btn_add' => ' Click to add Beneficial Owners',
                    'label' => false,
                    'type_options' => array(
                        'required' => true,
                    )
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))

//            ->with('Entity Types ', array(
//                'class' => 'col-md-7',
//                'box_class' => 'box box-solid box-success',
//                'description' => "We can use this selection format",
//            ))
//            ->add('bankOther', null,
//                array('label' => "Bank (other than Central Banks) ",
//                    'help' => "Senior officers and owners 5% or more of entity, as applicable(Section I & II)")
//            )
//            ->add('bankCentral', null,
//                array('label' => "Bank (Central or Monetary Authority) ",
//                    'help' => "Head of Central Bank/Monetary Authority(Section I)")
//            )
//            ->add('brokerDealer', null,
//                array('label' => y"Broker-Dealers, Investment Advisors",
//                    'help' => "Senior officers, general partners, and owners of 5% or more of entity, as applicable(Section I & II)")
//            )
//            ->add('charitable', null,
//                array('label' => "Charitable, Religious or Non-Profit Organization (regardless of structure) ",
//                    'help' => "Chairman of the board, senior officers, and owners of 5% or more of entity, as applicable (Section I)")
//            )
//            ->add('foundation', null,
//                array('label' => "Foundation, Endowment (regardless of legal structure)",
//                    'help' => "Senior officers, trustees, and grantor (Section I)")
//            )
//            ->add('governmentAgency', null,
//                array('label' => "Government Agency, Sovereign Agency, Municipality, Public Authority ",
//                    'help' => "Head of Agency (Section I)")
//            )
//            ->add('hedgeFund', null,
//                array('label' => "Hedge Funds (regardless of legal structure) ",
//                    'help' => "Hedge fund manager, officers/directors and/or general partner/managing member, as applicable")
//            )
//            ->add('llc', null,
//                array('label' => "Limited Liability Company",
//                    'help' => "Chairman of the board, senior officers, and owners of 5% of more of entity, as applicable, Manager and managing members (Section I)")
//            )
//            ->add('mutualFund', null,
//                array(
//                    'help' => "Officers and trading advisor(Section I)")
//            )
//            ->add('partnership', null,
//                array(
//                    'help' => "General partner and managing general partner (Section I)")
//            )
//            ->add('pension', null,
//                array('label' => "Pension Plan (Pensions Act) ",
//                    'help' => "NA")
//            )
//            ->add('otherPenson', null,
//                array('label' => "Other Pension Plan",
//                    'help' => "Trustee and persons authorized to act in a fiduciary capacity(Section I)")
//            )
//            ->add('privateEquityFund', null,
//                array('label' => "Private Equity Fund (regardless of legal structure)",
//                    'help' => "Private equity fund manager, officers/directors and/or general partner/managing member, as applicable (Section I)")
//            )
//            ->add('trustInd', null,
//                array('label' => "Trust- with an Individual as a Trustee",
//                    'help' => "Grantor/settlor, the trustees and beneficial owners of the trust assets (Section I & II)")
//            )
//            ->add('trustCop', null,
//                array('label' => "Trust- with a Corporate Trustee",
//                    'help' => "Grantor/settlor, the trustees and beneficial owners of the trust assets (OR, if the Trustee is a
//                 recognized financial institution located in Ghana and regulated in Ghana, an AML certification from the corporate trustee)
//                (Section I & II)")
//            )
//            ->add('privateCop', null,
//                array('label' => "Private Corporation(other than private investment vehicle/ personal holding company) ",
//                    'help' => "Chairman of the board, senior officers, and owners of 5% of more of entity, as applicable (Section I & II)")
//            )
//            ->add('piv', null,
//                array('label' => "Private Investment Vehicles/Personal Holding Companies(regardless of legal structure) ",
//                    'help' => "All beneficial owners(Section II)")
//            )
//            ->add('publicComp', null,
//                array('label' => "Public (Listed) Company Ticker/Symbol:",
//                    'help' => "Chairman of the board and senior officers (Section I)")
//            )
//            ->add('spv', null,
//                array('label' => "Special Purpose Vehicle (regardless of legal structure)",
//                    'help' => "Special purpose vehicle manager, officers/directors and/or general partner/managing member, as applicable (Section I)")
//            )
//            ->add('uniHosHmo', null,
//                array('label' => "University, Hospital, HMOs (regardless of legal structure)",
//                    'help' => "Chairman of the board, senior officers, and owners of 5% of more of entity, as applicable(Section I & II)")
//            )
//            ->add('indJointOwn', null,
//                array('label' => "Individuals and Joint Owners",
//                    'help' => "Individuals or joint owners (spouses etc.)(Section I & II)")
//            )
//            ->end()


        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
//        $showMapper
//            ->add('id')
//            ->add('bankOther')
//            ->add('bankCentral')
//            ->add('charitable')
//            ->add('foundation')
//            ->add('governmentAgency')
//            ->add('hedgeFund')
//            ->add('llc')
//            ->add('mutualFund')
//            ->add('partnership')
//            ->add('pension')
//            ->add('otherPenson')
//            ->add('privateEquityFund')
//            ->add('trustInd')
//            ->add('trustCop')
//            ->add('privateCop')
//            ->add('piv')
//            ->add('publicComp')
//            ->add('spv')
//            ->add('uniHosHmo')
//            ->add('indJointOwn')
//            ->add('brokerDealer')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt');
    }


    public function addPrincipalBeneOwner($param)
    {
        $files = array();
        foreach ($param->getBeneficialOwner() as $data) {
            $data->setEntityPriBene($param);
        }
        foreach ($param->getPrincipal() as $data) {
            $data->setEntityPriBene($param);
        }

    }


    public function prePersist($object)
    {
        $this->addPrincipalBeneOwner($object);
    }

    public function preUpdate($object)
    {
        $this->addPrincipalBeneOwner($object);
    }

    public function postPersist($object)
    {

    }

    public function postUpdate($object)
    {
        //perform after update

    }


    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'ClientBundle:Default:entityPriBene.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
