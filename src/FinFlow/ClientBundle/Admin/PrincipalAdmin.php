<?php

declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class PrincipalAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('personalInfo', null, array('label' => 'Entity Name'))
            ->add('fullname')
            ->add('jobRole')
            ->add('dob')
            ->add('idNo')
            ->add('taxId')
            ->add('primaryMobNumber')
            ->add('otherContact')
            ->add('address')
            ->add('businessLine');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id')
            ->add('personalInfo', null, array('label' => 'Entity Name'))
            ->add('fullname')
            ->add('jobRole')
//            ->add('dob')
//            ->add('licenseNo')
//            ->add('taxId')
            ->add('primaryMobNumber', null, ['required' => 1, 'label' => 'Mobile NUmber'])
            ->add('country', null, ['label' => 'Home Country'])
            ->add('otherContact')
            ->add('address')
            ->add('createdAt', null, ['label' => 'date created'])
//            ->add('_action', null, [
//                'actions' => [
//                    'show' => [],
//                    'edit' => [],
//                    'delete' => [],
//                ],
//            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('General', array(
                'class' => 'col-md-6',
                'box_class' => 'box box-solid box-success',
            ))
//            ->add('personalInfo',null,array('label'=>'Entity Name'))
            ->add('fullname', null,
                [
                    'error_bubbling' => true,
                    'required' => true,
                    'help' => "First Name Last Name "
                ]
            )
            ->add('jobRole')
            ->add('dob', null, array(
                'label' => 'Date of Birth',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
//            ->add('region',null,['label'=>'Home Region'])
//            ->add('town',null,['label'=>'Home Town'])
            ->add('taxType')
            ->add('taxId', null, [
                'error_bubbling' => true,
                'label' => 'Tax Number'
            ])
            ->add('idType')
            ->add('issuanceCountry', 'sonata_type_model', ['label' => 'Country of Issuance'])
            ->add('idNo')
            ->end()
            ->with('Contact Information', array(
                'class' => 'col-md-6',
                'box_class' => 'box box-solid box-warning',
            ))
            ->add('country', null, ['label' => ' Country of Domicile'])
            ->add('primaryMobNumber', null, [ 'error_bubbling' => true,'required' => true, 'label' => 'Mobile Number'])
            ->add('otherContact', null, ['help' => 'other mobile numbers, email address etc'])
            ->add('address', null, ['help' => 'Postal Code, House number, Street address etc']);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('fullname')
            ->add('jobRole')
            ->add('dob')
            ->add('licenseNo')
            ->add('taxId')
            ->add('primaryMobNumber')
            ->add('otherContact')
            ->add('address')
            ->add('businessLine')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt');
    }


    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'ClientBundle:Default:hideActions.html.twig';
            case 'list':
                return 'ClientBundle:Default:hide.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
