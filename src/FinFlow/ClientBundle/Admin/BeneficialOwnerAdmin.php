<?php

declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class BeneficialOwnerAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('personalInfo',null,array('label'=>'Entity Name'))
            ->add('fullname')
            ->add('dob', null, array('label' => 'date of birth'))
            ->add('idNo')
            ->add('primaryMobNumber')
            ->add('otherContact')
            ->add('address')
            ->add('businessLine')
            ->add('wealthSource')
            ->add('fundSource')
            ->add('createdAt', null, array('label' => 'date created'));

    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id')
            ->add('personalInfo',null,array('label'=>'Entity Name'))
            ->add('fullname')
            ->add('dob', null, array('label' => 'date of birth'))
            ->add('idType')
            ->add('primaryMobNumber')
            ->add('otherContact')
            ->add('address')
            ->add('businessLine')
            ->add('wealthSource')
            ->add('fundSource')
            ->add('createdAt', null, array('label' => 'date created'))

//            ->add('_action', null, [
//                'actions' => [
////                    'show' => [],
////                    'edit' => [``],
////                    'delete' => [],
//                ],
//            ])
;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
//            ->tab('General Bio')
            ->with('Personal Bio-Data', array(
                'class' => 'col-md-6',
                'box_class' => 'box box-solid box-success',
            ))
//            ->add('personalInfo',null,array('label'=>'Entity Name'))
            ->add('fullname', null, [
                'required' => true,
                    'label' => 'Full Name',
                    'help'=>"First Name Last Name ",

                ]
            )
            ->add('dob', null, array(
                'label' => 'Date of Birth',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('taxType')
            ->add('taxId', null, [ 'error_bubbling' => true,
                'label' => 'Tax ID. Number'])
            ->add('idType')
            ->add('issuanceCountry', null, ['required' => true,'label' => 'Country of Issuance*'])
            ->add('idNo',null,['required' => true,'label'=>''])
            ->add('businessLine', null, ['error_bubbling' => true,
                'label' => 'Line of Business'])
            ->add('wealthSource', null, ['label' => 'Source of Wealth (for private investment vehicles and Trusts only)'])
            ->add('fundSource', null, ['label' => 'Source of Fund'])
            ->end()
            ->with('Contact Information', array(
                'class' => 'col-md-6',
                'box_class' => 'box box-solid box-warning',
            ))
            ->add('primaryMobNumber', null, array('error_bubbling' => true,
                'required' => true,'label'=>'Mob No',
                'help' => "Mobile Number should be with country code eg:<b> +233xxxxxxxxx </b>"))
            ->add('country', null, ['label' => 'Home Country'])
            ->add('region', null, ['label' => 'Home Region'])
            ->add('town', null, ['label' => 'Home City'])
            ->add('otherContact', null, ['help' => 'other mobile numbers, email address etc'])
            ->add('address', null, ['help' => 'Postal Code, House number, Street address etc'])
            ->end()
            ->end()
//            ->tab('Other Details')
//            ->with(' Info.', array(
//                'class' => 'col-md-8',
//                'box_class' => 'box box-solid box-warning',
//            ));

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
//        $showMapper
//            ->add('id')
//            ->add('fullname')
//            ->add('title')
//            ->add('dob')
//            ->add('licenseNo')
//            ->add('primaryMobNumber')
//            ->add('otherContact')
//            ->add('address')
//            ->add('businessLine')
//            ->add('wealthSource')
//            ->add('fundSource')
//            ->add('createdAt')
//            ->add('updatedAt')
//            ->add('deletedAt');
    }

//    public function getTemplate($name)
//    {
//        switch ($name) {
//            case 'edit':
//                return 'ClientBundle:Default:hideActions.html.twig';
//            case 'list':
//                return 'ClientBundle:Default:hide.html.twig';
//                break;
//            default:
//                return parent::getTemplate($name);
//                break;
//        }
//    }
}
