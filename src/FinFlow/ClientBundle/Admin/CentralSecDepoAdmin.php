<?php

declare(strict_types=1);

namespace FinFlow\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class CentralSecDepoAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('accountName')
            ->add('accountNo')
            ->add('iniAccOpenBroker')
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
//            ->add('id')
            ->add('accountName',null,array("required"=>1,'label'=>'Account Name'))
            ->add('accountNo',null,array("required"=>1,'label'=>'Account Number'))
            ->add('iniAccOpenBroker',null,array("required"=>1,'label'=>'Initial Account Opening Broker'))
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('accountName',null,array("required"=>1,'label'=>'Account Name'))
            ->add('accountNo',null,array("required"=>1,'label'=>'Account Number'))
            ->add('iniAccOpenBroker',null,array("required"=>1,'label'=>'Initial Account Opening Broker'))

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('accountName')
            ->add('accountNo')
            ->add('iniAccOpenBroker')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
        ;
    }
}
