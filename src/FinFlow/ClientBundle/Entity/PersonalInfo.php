<?php

namespace FinFlow\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * PersonalInfo
 * @Vich\Uploadable
 * @ORM\Table (name="personal_info")
 * @ORM\Entity(repositoryClass="\FinFlow\ClientBundle\Repository\BeneficialOwnerRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("tin")
 * @UniqueEntity("businessPhone")
 * @UniqueEntity("faxNumber")
 * @UniqueEntity("accountNumber")
 */
class PersonalInfo
{
    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;



    /**
     *
     * @Assert\NotBlank
     * @ORM\Column(name="full_name", type="string", nullable=false)
     * @Expose
     */
    private $fullName;


    /**
     * this entity is not mapped just a holder
     * @ORM\Column(name="doc", type="boolean", nullable=false)
     */
    protected $doc;


    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $notices;


    /**
     *
     * @ORM\Column(name="account_number", type="string", nullable=true)
     * @Expose
     */
    private $accountNumber;

    /**
     * @ORM\Column(name="registered_pool_inv", type="string", nullable=true)
     * @Expose
     */
    private $registeredPoolInv;

    /**
     * @ORM\Column(name="natural_person", type="boolean", nullable=true)
     * @Expose
     */
    private $naturalPerson;

    /**
     * @ORM\Column(name="occupation", type="string", nullable=true)
     * @Expose
     */
    private $occupation;
    /**
     * @ORM\Column(name="civil_status", type="string", nullable=true)
     * @Expose
     */
    private $civilStatus;


    /**
     * @var Date
     *
     * @ORM\Column(name="date_of_birth", type="date",nullable=true)
     */
    private $dob;


    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\AccountType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_type_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $accountType;

    /**
     * @ORM\Column(name="business", type="string",nullable=true)
     */
    private $business;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="res_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $residentCountry;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="national_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $national;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="org_law_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $orgUnderLawCountry;

    /**
     * @ORM\Column(name="tin", type="string", nullable=true)
     * @Expose
     */
    private $tin;

    /**
     * @Assert\NotBlank
     * @ORM\Column(name="legal_address", type="text", nullable=true)
     * @Expose
     */
    private $legalAddress;

    /**
     * @Assert\NotBlank(message="Enter the business number")
     * @MisdAssert\PhoneNumber()
     * @ORM\Column(name="business_phone", type="string", length=20)
     * @Expose
     */
    private $businessPhone;

    /**
     * @ORM\Column(name="fax_number", type="phone_number", nullable=true)
     * @Expose
     */
    private $faxNumber;

    /**
     *
     * @ORM\Column(name="gps",type="text", nullable=true)
     * @Expose
     */
    private $gps;

    /**
     *
     * @ORM\Column(name="same_addr",type="boolean", nullable=true)
     * @Expose
     */
    private $sameAddr;



    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\LocationBundle\Entity\Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $town;


    /**
     * @ORM\Column(name="mail_address", type="text", nullable=true)
     * @Expose
     */
    private $mailAddress;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mail_addr_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $mailAddCountry;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mail_addr_region_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $mailAddRegion;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\LocationBundle\Entity\Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mail_addr_town_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $mailAddTown;

    /**
     * @ORM\Column(name="political_aff", type="boolean", nullable=true)
     * @Expose
     */
    private $politicalAff;

    /**
     * @ORM\Column(name="political_aff_info", type="text", nullable=true)
     * @Expose
     */
    private $politicalAffInfo;

    /**
     * @ORM\Column(name="gh_bro_deal_reg", type="boolean", nullable=true)
     * @Expose
     */
    private $ghBroDealReg;

    /**
     * @ORM\Column(name="csd_account", type="boolean", nullable=true)
     * @Expose
     */
    private $csdAccount;

    /**
     * @ORM\Column(name="require_csd_acc", type="boolean", nullable=true)
     * @Expose
     */
    private $requireCsdAcc;

    /**
     *  @Assert\Valid()
     * @ORM\OneToMany(targetEntity="\FinFlow\ClientBundle\Entity\CentralSecDepo", mappedBy="personalInfo", orphanRemoval=true, cascade={"persist"})
     */
    private $personalCsdAccountInfo;


    /**
     * @ORM\Column(name="sarpon_as_prime_broker", type="boolean", nullable=true)
     * @Expose
     */
    private $sarponAsPrimeBroker;

    /**
     *  @Assert\Valid()
     * @ORM\OneToMany(targetEntity="FinFlow\SettingBundle\Entity\PrimeBroker", mappedBy="personalInfo", orphanRemoval=true, cascade={"persist"})
     */
    private $personalPrimeBroker;


    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\MonetaryRange")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="annual_gross_income_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $annualGrossIncome;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\MonetaryRange")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="total_liq_asset_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $totalLiqAsset;

    /**
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\MonetaryRange")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="total_net_worth_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $totalNetWorth;

    /**
     * @ORM\Column(name="corp_retire_plan", type="boolean", nullable=true)
     * @Expose
     */
    private $corpRetirePlan;

    /**
     * @ORM\Column(name="other_retire_plan", type="text", nullable=true)
     * @Expose
     */
    private $otherRetirePlan;

    /**
     * @ORM\Column(name="other_retire_plan_act_subject", type="boolean", nullable=true)
     * @Expose
     */
    private $otherRetirePlanActSubject;

    /**
     * @ORM\Column(name="other_equity", type="boolean", nullable=true)
     * @Expose
     */
    private $otherEquity;



    /**
     * @ORM\Column(name="large_trader_agent", type="boolean", nullable=true)
     * @Expose
     */
    private $largeTraderAgent;

    /**
     * @ORM\Column(name="large_trader_agent_csdid", type="text", nullable=true)
     * @Expose
     */
    private $largeTraderAgentCSDID;


    /**
     * @ORM\Column(name="bearer_share_entity", type="boolean", nullable=true)
     * @Expose
     */
    private $bearerShareEntity;

    /**
     * @ORM\Column(name="other_bearer_share_owned", type="boolean", nullable=true)
     * @Expose
     */
    private $otherBearerShareOwned;



    /**
     *
     * @Assert\Valid()
     * @var \Picture
     *  @ORM\OneToMany(targetEntity="FinFlow\SettingBundle\Entity\MediaUpload", mappedBy="personalInfoSignFiles", orphanRemoval=true, cascade={"persist"})
     */
    private $uploadSignatureFiles;

    /**
     *
     *  @Assert\Valid()
     * @var \Picture
     *  @ORM\OneToMany(targetEntity="FinFlow\SettingBundle\Entity\MediaUpload", mappedBy="uploadBearerFiles", orphanRemoval=true, cascade={"persist"})
     */
    private $uploadBearerFile;



    /**
     * @ORM\Column(name="third_party", type="boolean", nullable=true)
     * @Expose
     */
    private $thirdParty;

    /**
     * @ORM\Column(name="trade", type="string", nullable=true)
     * @Expose
     */
    private $trade;




    /**
     * @ORM\Column(name="margin_account", type="boolean", nullable=true)
     * @Expose
     */
    private $marginAccount;



    /**
     * @ORM\Column(name="disclosure_issuer", type="boolean", nullable=true)
     * @Expose
     */
    private $disclosureIssuer;

    /**
     * @ORM\Column(name="disclosure_foreign_tax", type="boolean", nullable=true)
     * @Expose
     */
    private $disclosureForeignTax;

    /**
     * @ORM\Column(name="fx_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $fxTransaction;

    /**
     * @ORM\Column(name="mo_market_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $moMarketTransaction;

    /**
     * @ORM\Column(name="fixed_inc_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $fixedIncTransaction;



    /**
     *  @Assert\Valid()
     * @var
     *  @ORM\OneToMany(targetEntity="FinFlow\SettingBundle\Entity\Agent", mappedBy="personalInfo", orphanRemoval=true, cascade={"persist"})
     */
    private $agent;


    /**
     * @Assert\Valid()
     * @var
     *  @ORM\OneToMany(targetEntity="FinFlow\ClientBundle\Entity\Principal", mappedBy="personalInfo", orphanRemoval=true, cascade={"persist"})
     */
    private $principal;

    /**
     * @var
     *  @ORM\OneToMany(targetEntity="FinFlow\ClientBundle\Entity\BeneficialOwner", mappedBy="personalInfo", orphanRemoval=true, cascade={"persist"})
     */
    private $beneficialOwner;

    /**
     * @var string
     *
     * @ORM\Column(name="media_files_names", type="text", nullable=true)
     */
    private $mediaFilesNames;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\Finflow\UserBundle\Entity\User",inversedBy="actor", inversedBy="personinfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     *
     * @ORM\ManyToOne(targetEntity="\Finflow\UserBundle\Entity\User",inversedBy="actor", inversedBy="personinfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;



    public function __toString() {
        return  (String) $this->fullName;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personalCsdAccountInfo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadBearerFile =new \Doctrine\Common\Collections\ArrayCollection();
        $this->personalPrimeBroker =new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadSignatureFiles =new \Doctrine\Common\Collections\ArrayCollection();
        $this->principal =new \Doctrine\Common\Collections\ArrayCollection();
        $this->beneficialOwner =new \Doctrine\Common\Collections\ArrayCollection();
        $this->agent =new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname.
     *
     * @param string $fullname
     *
     * @return PersonalInfo
     */
    public function setFullName($fullname)
    {
        $this->fullName = $fullname;

        return $this;
    }

    /**
     * Get fullname.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set accountNumber.
     *
     * @param string|null $accountNumber
     *
     * @return PersonalInfo
     */
    public function setAccountNumber($accountNumber = null)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber.
     *
     * @return string|null
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set registeredPoolInv.
     *
     * @param string|null $registeredPoolInv
     *
     * @return PersonalInfo
     */
    public function setRegisteredPoolInv($registeredPoolInv = null)
    {
        $this->registeredPoolInv = $registeredPoolInv;

        return $this;
    }

    /**
     * Get registeredPoolInv.
     *
     * @return string|null
     */
    public function getRegisteredPoolInv()
    {
        return $this->registeredPoolInv;
    }

    /**
     * Set naturalPerson.
     *
     * @param bool|null $naturalPerson
     *
     * @return PersonalInfo
     */
    public function setNaturalPerson($naturalPerson = null)
    {
        $this->naturalPerson = $naturalPerson;

        return $this;
    }

    /**
     * Get naturalPerson.
     *
     * @return bool|null
     */
    public function getNaturalPerson()
    {
        return $this->naturalPerson;
    }

    /**
     * Set tin.
     *
     * @param string|null $tin
     *
     * @return PersonalInfo
     */
    public function setTin($tin = null)
    {
        $this->tin = $tin;

        return $this;
    }

    /**
     * Get tin.
     *
     * @return string|null
     */
    public function getTin()
    {
        return $this->tin;
    }

    /**
     * Set legalAddress.
     *
     * @param string|null $legalAddress
     *
     * @return PersonalInfo
     */
    public function setLegalAddress($legalAddress = null)
    {
        $this->legalAddress = $legalAddress;

        return $this;
    }

    /**
     * Get legalAddress.
     *
     * @return string|null
     */
    public function getLegalAddress()
    {
        return $this->legalAddress;
    }

    /**
     * Set businessPhone.
     *
     * @param string|null $businessPhone
     *
     * @return PersonalInfo
     */
    public function setBusinessPhone($businessPhone = null)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    /**
     * Get businessPhone.
     *
     * @return string|null
     */
    public function getBusinessPhone()
    {
        return $this->businessPhone;
    }

    /**
     * Set faxNumber.
     *
     * @param string|null $faxNumber
     *
     * @return PersonalInfo
     */
    public function setFaxNumber($faxNumber = null)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get faxNumber.
     *
     * @return string|null
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set mailAddress.
     *
     * @param string|null $mailAddress
     *
     * @return PersonalInfo
     */
    public function setMailAddress($mailAddress = null)
    {
        $this->mailAddress = $mailAddress;

        return $this;
    }

    /**
     * Get mailAddress.
     *
     * @return string|null
     */
    public function getMailAddress()
    {
        return $this->mailAddress;
    }

    /**
     * Set politicalAff.
     *
     * @param bool|null $politicalAff
     *
     * @return PersonalInfo
     */
    public function setPoliticalAff($politicalAff = null)
    {
        $this->politicalAff = $politicalAff;

        return $this;
    }

    /**
     * Get politicalAff.
     *
     * @return bool|null
     */
    public function getPoliticalAff()
    {
        return $this->politicalAff;
    }

    /**
     * Set politicalAffInfo.
     *
     * @param string|null $politicalAffInfo
     *
     * @return PersonalInfo
     */
    public function setPoliticalAffInfo($politicalAffInfo = null)
    {
        $this->politicalAffInfo = $politicalAffInfo;

        return $this;
    }

    /**
     * Get politicalAffInfo.
     *
     * @return string|null
     */
    public function getPoliticalAffInfo()
    {
        return $this->politicalAffInfo;
    }

    /**
     * Set ghBroDealReg.
     *
     * @param bool|null $ghBroDealReg
     *
     * @return PersonalInfo
     */
    public function setGhBroDealReg($ghBroDealReg = null)
    {
        $this->ghBroDealReg = $ghBroDealReg;

        return $this;
    }

    /**
     * Get ghBroDealReg.
     *
     * @return bool|null
     */
    public function getGhBroDealReg()
    {
        return $this->ghBroDealReg;
    }

    /**
     * Set csdAccount.
     *
     * @param bool|null $csdAccount
     *
     * @return PersonalInfo
     */
    public function setCsdAccount($csdAccount = null)
    {
        $this->csdAccount = $csdAccount;

        return $this;
    }

    /**
     * Get csdAccount.
     *
     * @return bool|null
     */
    public function getCsdAccount()
    {
        return $this->csdAccount;
    }

    /**
     * Set requireCsdAcc.
     *
     * @param bool|null $requireCsdAcc
     *
     * @return PersonalInfo
     */
    public function setRequireCsdAcc($requireCsdAcc = null)
    {
        $this->requireCsdAcc = $requireCsdAcc;

        return $this;
    }

    /**
     * Get requireCsdAcc.
     *
     * @return bool|null
     */
    public function getRequireCsdAcc()
    {
        return $this->requireCsdAcc;
    }

    /**
     * Set corpRetirePlan.
     *
     * @param bool|null $corpRetirePlan
     *
     * @return PersonalInfo
     */
    public function setCorpRetirePlan($corpRetirePlan = null)
    {
        $this->corpRetirePlan = $corpRetirePlan;

        return $this;
    }

    /**
     * Get corpRetirePlan.
     *
     * @return bool|null
     */
    public function getCorpRetirePlan()
    {
        return $this->corpRetirePlan;
    }

    /**
     * Set otherRetirePlan.
     *
     * @param string|null $otherRetirePlan
     *
     * @return PersonalInfo
     */
    public function setOtherRetirePlan($otherRetirePlan = null)
    {
        $this->otherRetirePlan = $otherRetirePlan;

        return $this;
    }

    /**
     * Get otherRetirePlan.
     *
     * @return string|null
     */
    public function getOtherRetirePlan()
    {
        return $this->otherRetirePlan;
    }

    /**
     * Set otherRetirePlanActSubject.
     *
     * @param bool|null $otherRetirePlanActSubject
     *
     * @return PersonalInfo
     */
    public function setOtherRetirePlanActSubject($otherRetirePlanActSubject = null)
    {
        $this->otherRetirePlanActSubject = $otherRetirePlanActSubject;

        return $this;
    }

    /**
     * Get otherRetirePlanActSubject.
     *
     * @return bool|null
     */
    public function getOtherRetirePlanActSubject()
    {
        return $this->otherRetirePlanActSubject;
    }

    /**
     * Set otherEquity.
     *
     * @param bool|null $otherEquity
     *
     * @return PersonalInfo
     */
    public function setOtherEquity($otherEquity = null)
    {
        $this->otherEquity = $otherEquity;

        return $this;
    }

    /**
     * Get otherEquity.
     *
     * @return bool|null
     */
    public function getOtherEquity()
    {
        return $this->otherEquity;
    }

    /**
     * Set bearerShareEntity.
     *
     * @param bool|null $bearerShareEntity
     *
     * @return PersonalInfo
     */
    public function setBearerShareEntity($bearerShareEntity = null)
    {
        $this->bearerShareEntity = $bearerShareEntity;

        return $this;
    }

    /**
     * Get bearerShareEntity.
     *
     * @return bool|null
     */
    public function getBearerShareEntity()
    {
        return $this->bearerShareEntity;
    }

    /**
     * Set otherBearerShareOwned.
     *
     * @param bool|null $otherBearerShareOwned
     *
     * @return PersonalInfo
     */
    public function setOtherBearerShareOwned($otherBearerShareOwned = null)
    {
        $this->otherBearerShareOwned = $otherBearerShareOwned;

        return $this;
    }

    /**
     * Get otherBearerShareOwned.
     *
     * @return bool|null
     */
    public function getOtherBearerShareOwned()
    {
        return $this->otherBearerShareOwned;
    }

    /**
     * Set accountType.
     *
     * @param \FinFlow\SettingBundle\Entity\AccountType|null $accountType
     *
     * @return PersonalInfo
     */
    public function setAccountType(\FinFlow\SettingBundle\Entity\AccountType $accountType = null)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType.
     *
     * @return \FinFlow\SettingBundle\Entity\AccountType|null
     */
    public function getAccountType()
    {
        return $this->accountType;
    }



    /**
     * Set orgUnderLawCountry.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $orgUnderLawCountry
     *
     * @return PersonalInfo
     */
    public function setOrgUnderLawCountry(\FinFlow\LocationBundle\Entity\Country $orgUnderLawCountry = null)
    {
        $this->orgUnderLawCountry = $orgUnderLawCountry;

        return $this;
    }

    /**
     * Get orgUnderLawCountry.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getOrgUnderLawCountry()
    {
        return $this->orgUnderLawCountry;
    }

    /**
     * Set country.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $country
     *
     * @return PersonalInfo
     */
    public function setCountry(\FinFlow\LocationBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region.
     *
     * @param \FinFlow\LocationBundle\Entity\Region|null $region
     *
     * @return PersonalInfo
     */
    public function setRegion(\FinFlow\LocationBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \FinFlow\LocationBundle\Entity\Region|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set town.
     *
     * @param \FinFlow\LocationBundle\Entity\Town|null $town
     *
     * @return PersonalInfo
     */
    public function setTown(\FinFlow\LocationBundle\Entity\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town.
     *
     * @return \FinFlow\LocationBundle\Entity\Town|null
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set mailAddCountry.
     *
     * @param \FinFlow\LocationBundle\Entity\Country|null $mailAddCountry
     *
     * @return PersonalInfo
     */
    public function setMailAddCountry(\FinFlow\LocationBundle\Entity\Country $mailAddCountry = null)
    {
        $this->mailAddCountry = $mailAddCountry;

        return $this;
    }

    /**
     * Get mailAddCountry.
     *
     * @return \FinFlow\LocationBundle\Entity\Country|null
     */
    public function getMailAddCountry()
    {
        return $this->mailAddCountry;
    }

    /**
     * Set mailAddRegion.
     *
     * @param \FinFlow\LocationBundle\Entity\Region|null $mailAddRegion
     *
     * @return PersonalInfo
     */
    public function setMailAddRegion(\FinFlow\LocationBundle\Entity\Region $mailAddRegion = null)
    {
        $this->mailAddRegion = $mailAddRegion;

        return $this;
    }

    /**
     * Get mailAddRegion.
     *
     * @return \FinFlow\LocationBundle\Entity\Region|null
     */
    public function getMailAddRegion()
    {
        return $this->mailAddRegion;
    }

    /**
     * Set mailAddTown.
     *
     * @param \FinFlow\LocationBundle\Entity\Town|null $mailAddTown
     *
     * @return PersonalInfo
     */
    public function setMailAddTown(\FinFlow\LocationBundle\Entity\Town $mailAddTown = null)
    {
        $this->mailAddTown = $mailAddTown;

        return $this;
    }

    /**
     * Get mailAddTown.
     *
     * @return \FinFlow\LocationBundle\Entity\Town|null
     */
    public function getMailAddTown()
    {
        return $this->mailAddTown;
    }

    /**
     * Add personalCsdAccountInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\CentralSecDepo $personalCsdAccountInfo
     *
     * @return PersonalInfo
     */
    public function addPersonalCsdAccountInfo(\FinFlow\ClientBundle\Entity\CentralSecDepo $personalCsdAccountInfo)
    {
        $this->personalCsdAccountInfo[] = $personalCsdAccountInfo;

        return $this;
    }

    /**
     * Remove personalCsdAccountInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\CentralSecDepo $personalCsdAccountInfo
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePersonalCsdAccountInfo(\FinFlow\ClientBundle\Entity\CentralSecDepo $personalCsdAccountInfo)
    {
        return $this->personalCsdAccountInfo->removeElement($personalCsdAccountInfo);
    }

    /**
     * Get personalCsdAccountInfo.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonalCsdAccountInfo()
    {
        return $this->personalCsdAccountInfo;
    }

    /**
     * Set annualGrossIncome.
     *
     * @param \FinFlow\SettingBundle\Entity\MonetaryRange|null $annualGrossIncome
     *
     * @return PersonalInfo
     */
    public function setAnnualGrossIncome(\FinFlow\SettingBundle\Entity\MonetaryRange $annualGrossIncome = null)
    {
        $this->annualGrossIncome = $annualGrossIncome;

        return $this;
    }

    /**
     * Get annualGrossIncome.
     *
     * @return \FinFlow\SettingBundle\Entity\MonetaryRange|null
     */
    public function getAnnualGrossIncome()
    {
        return $this->annualGrossIncome;
    }

    /**
     * Set totalLiqAsset.
     *
     * @param \FinFlow\SettingBundle\Entity\MonetaryRange|null $totalLiqAsset
     *
     * @return PersonalInfo
     */
    public function setTotalLiqAsset(\FinFlow\SettingBundle\Entity\MonetaryRange $totalLiqAsset = null)
    {
        $this->totalLiqAsset = $totalLiqAsset;

        return $this;
    }

    /**
     * Get totalLiqAsset.
     *
     * @return \FinFlow\SettingBundle\Entity\MonetaryRange|null
     */
    public function getTotalLiqAsset()
    {
        return $this->totalLiqAsset;
    }

    /**
     * Set totalNetWorth.
     *
     * @param \FinFlow\SettingBundle\Entity\MonetaryRange|null $totalNetWorth
     *
     * @return PersonalInfo
     */
    public function setTotalNetWorth(\FinFlow\SettingBundle\Entity\MonetaryRange $totalNetWorth = null)
    {
        $this->totalNetWorth = $totalNetWorth;

        return $this;
    }

    /**
     * Get totalNetWorth.
     *
     * @return \FinFlow\SettingBundle\Entity\MonetaryRange|null
     */
    public function getTotalNetWorth()
    {
        return $this->totalNetWorth;
    }

    /**
     * Add uploadBearerFile.
     *
     * @param \FinFlow\SettingBundle\Entity\MediaUpload $uploadBearerFile
     *
     * @return PersonalInfo
     */
    public function addUploadBearerFile(\FinFlow\SettingBundle\Entity\MediaUpload $uploadBearerFile)
    {
        $this->uploadBearerFile[] = $uploadBearerFile;

        return $this;
    }

    /**
     * Remove uploadBearerFile.
     *
     * @param \FinFlow\SettingBundle\Entity\MediaUpload $uploadBearerFile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUploadBearerFile(\FinFlow\SettingBundle\Entity\MediaUpload $uploadBearerFile)
    {
        return $this->uploadBearerFile->removeElement($uploadBearerFile);
    }

    /**
     * Get uploadBearerFile.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUploadBearerFile()
    {
        return $this->uploadBearerFile;
    }

    /**
     * Set user.
     *
     * @param \Finflow\UserBundle\Entity\User|null $user
     *
     * @return PersonalInfo
     */
    public function setUser(\FinFlow\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \FinFlow\UserBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @return string
     */
    public function getMediaFilesNames()
    {
        return $this->mediaFilesNames;
    }

    /**
     * @param string $mediaFilesNames
     */
    public function setMediaFilesNames($mediaFilesNames)
    {
        $this->mediaFilesNames = $mediaFilesNames;
    }




    /**
     * Add principal.
     *
     * @param \FinFlow\ClientBundle\Entity\Principal $principal
     *
     * @return PersonalInfo
     */
    public function addPrincipal(\FinFlow\ClientBundle\Entity\Principal $principal)
    {
        $this->principal[] = $principal;

        return $this;
    }

    /**
     * Remove principal.
     *
     * @param \FinFlow\ClientBundle\Entity\Principal $principal
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePrincipal(\FinFlow\ClientBundle\Entity\Principal $principal)
    {
        return $this->principal->removeElement($principal);
    }

    /**
     * Get principal.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Add beneficialOwner.
     *
     * @param \FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner
     *
     * @return PersonalInfo
     */
    public function addBeneficialOwner(\FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner)
    {
        $this->beneficialOwner[] = $beneficialOwner;

        return $this;
    }

    /**
     * Remove beneficialOwner.
     *
     * @param \FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBeneficialOwner(\FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner)
    {
        return $this->beneficialOwner->removeElement($beneficialOwner);
    }

    /**
     * Get beneficialOwner.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficialOwner()
    {
        return $this->beneficialOwner;
    }

    /**
     * Set trade.
     *
     * @param bool|null $trade
     *
     * @return PersonalInfo
     */
    public function setTrade($trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade.
     *
     * @return bool|null
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * Set tradeBroadAuth.
     *
     * @param bool|null $tradeBroadAuth
     *
     * @return PersonalInfo
     */
    public function setTradeBroadAuth($tradeBroadAuth = null)
    {
        $this->tradeBroadAuth = $tradeBroadAuth;

        return $this;
    }

    /**
     * Get tradeBroadAuth.
     *
     * @return bool|null
     */
    public function getTradeBroadAuth()
    {
        return $this->tradeBroadAuth;
    }

    /**
     * Set marginAccount.
     *
     * @param bool|null $marginAccount
     *
     * @return PersonalInfo
     */
    public function setMarginAccount($marginAccount = null)
    {
        $this->marginAccount = $marginAccount;

        return $this;
    }

    /**
     * Get marginAccount.
     *
     * @return bool|null
     */
    public function getMarginAccount()
    {
        return $this->marginAccount;
    }

    /**
     * Set disclosureIssuer.
     *
     * @param bool|null $disclosureIssuer
     *
     * @return PersonalInfo
     */
    public function setDisclosureIssuer($disclosureIssuer = null)
    {
        $this->disclosureIssuer = $disclosureIssuer;

        return $this;
    }

    /**
     * Get disclosureIssuer.
     *
     * @return bool|null
     */
    public function getDisclosureIssuer()
    {
        return $this->disclosureIssuer;
    }

    /**
     * Set disclosureForeignTax.
     *
     * @param bool|null $disclosureForeignTax
     *
     * @return PersonalInfo
     */
    public function setDisclosureForeignTax($disclosureForeignTax = null)
    {
        $this->disclosureForeignTax = $disclosureForeignTax;

        return $this;
    }

    /**
     * Get disclosureForeignTax.
     *
     * @return bool|null
     */
    public function getDisclosureForeignTax()
    {
        return $this->disclosureForeignTax;
    }

    /**
     * Set fxTransaction.
     *
     * @param bool|null $fxTransaction
     *
     * @return PersonalInfo
     */
    public function setFxTransaction($fxTransaction = null)
    {
        $this->fxTransaction = $fxTransaction;

        return $this;
    }

    /**
     * Get fxTransaction.
     *
     * @return bool|null
     */
    public function getFxTransaction()
    {
        return $this->fxTransaction;
    }

    /**
     * Set moMarketTransaction.
     *
     * @param bool|null $moMarketTransaction
     *
     * @return PersonalInfo
     */
    public function setMoMarketTransaction($moMarketTransaction = null)
    {
        $this->moMarketTransaction = $moMarketTransaction;

        return $this;
    }

    /**
     * Get moMarketTransaction.
     *
     * @return bool|null
     */
    public function getMoMarketTransaction()
    {
        return $this->moMarketTransaction;
    }

    /**
     * Set fixedIncTransaction.
     *
     * @param bool|null $fixedIncTransaction
     *
     * @return PersonalInfo
     */
    public function setFixedIncTransaction($fixedIncTransaction = null)
    {
        $this->fixedIncTransaction = $fixedIncTransaction;

        return $this;
    }

    /**
     * Get fixedIncTransaction.
     *
     * @return bool|null
     */
    public function getFixedIncTransaction()
    {
        return $this->fixedIncTransaction;
    }

    /**
     * Set agent.
     *
     * @param \FinFlow\SettingBundle\Entity\Agent|null $agent
     *
     * @return PersonalInfo
     */
    public function setAgent(\FinFlow\SettingBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent.
     *
     * @return \FinFlow\SettingBundle\Entity\Agent|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set primeBroker.
     *
     * @param \FinFlow\SettingBundle\Entity\PrimeBroker|null $primeBroker
     *
     * @return PersonalInfo
     */
    public function setPrimeBroker(\FinFlow\SettingBundle\Entity\PrimeBroker $primeBroker = null)
    {
        $this->primeBroker = $primeBroker;

        return $this;
    }

    /**
     * Get primeBroker.
     *
     * @return \FinFlow\SettingBundle\Entity\PrimeBroker|null
     */
    public function getPrimeBroker()
    {
        return $this->primeBroker;
    }

    /**
     * Add uploadSignatureFile.
     *
     * @param \FinFlow\SettingBundle\Entity\MediaUpload $uploadSignatureFile
     *
     * @return PersonalInfo
     */
    public function addUploadSignatureFile(\FinFlow\SettingBundle\Entity\MediaUpload $uploadSignatureFile)
    {
        $this->uploadSignatureFiles[] = $uploadSignatureFile;

        return $this;
    }

    /**
     * Remove uploadSignatureFile.
     *
     * @param \FinFlow\SettingBundle\Entity\MediaUpload $uploadSignatureFile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUploadSignatureFile(\FinFlow\SettingBundle\Entity\MediaUpload $uploadSignatureFile)
    {
        return $this->uploadSignatureFiles->removeElement($uploadSignatureFile);
    }

    /**
     * Get uploadSignatureFiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUploadSignatureFiles()
    {
        return $this->uploadSignatureFiles;
    }

    /**
     * @return mixed
     */
    public function getLargeTraderAgent()
    {
        return $this->largeTraderAgent;
    }

    /**
     * @param mixed $largeTraderAgent
     */
    public function setLargeTraderAgent($largeTraderAgent)
    {
        $this->largeTraderAgent = $largeTraderAgent;
    }

    /**
     * @return mixed
     */
    public function getLargeTraderAgentCSDID()
    {
        return $this->largeTraderAgentCSDID;
    }

    /**
     * @param mixed $largeTraderAgentCSDID
     */
    public function setLargeTraderAgentCSDID($largeTraderAgentCSDID)
    {
        $this->largeTraderAgentCSDID = $largeTraderAgentCSDID;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param mixed $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return mixed
     */
    public function getCivilStatus()
    {
        return $this->civilStatus;
    }

    /**
     * @param mixed $civilStatus
     */
    public function setCivilStatus($civilStatus)
    {
        $this->civilStatus = $civilStatus;
    }

    /**
     * @return Date
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param Date $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return mixed
     */
    public function getResidentCountry()
    {
        return $this->residentCountry;
    }

    /**
     * @param mixed $residentCountry
     */
    public function setResidentCountry($residentCountry)
    {
        $this->residentCountry = $residentCountry;
    }

    /**
     * @return mixed
     */
    public function getNational()
    {
        return $this->national;
    }

    /**
     * @param mixed $national
     */
    public function setNational($national)
    {
        $this->national = $national;
    }
    /**
     * @return mixed
     */
    public function getGPs()
    {
        return $this->gps;
    }

    /**
     * @param mixed $gps
     */
    public function setGps($gps)
    {
        $this->gps = $gps;
    }

    /**
     * @return mixed
     */
    public function getSameAddr()
    {
        return $this->sameAddr;
    }

    /**
     * @param mixed $sameAddr
     */
    public function setSameAddr($sameAddr)
    {
        $this->sameAddr = $sameAddr;
    }



    /**
     * @return mixed
     */
    public function getSarponAsPrimeBroker()
    {
        return $this->sarponAsPrimeBroker;
    }

    /**
     * @param mixed $sarponAsPrimeBroker
     */
    public function setSarponAsPrimeBroker($sarponAsPrimeBroker)
    {
        $this->sarponAsPrimeBroker = $sarponAsPrimeBroker;
    }



    /**
     * Add personalPrimeBroker.
     *
     * @param \FinFlow\SettingBundle\Entity\PrimeBroker $personalPrimeBroker
     *
     * @return PersonalInfo
     */
    public function addPersonalPrimeBroker(\FinFlow\SettingBundle\Entity\PrimeBroker $personalPrimeBroker)
    {
        $this->personalPrimeBroker[] = $personalPrimeBroker;

        return $this;
    }

    /**
     * Remove personalPrimeBroker.
     *
     * @param \FinFlow\SettingBundle\Entity\PrimeBroker $personalPrimeBroker
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePersonalPrimeBroker(\FinFlow\SettingBundle\Entity\PrimeBroker $personalPrimeBroker)
    {
        return $this->personalPrimeBroker->removeElement($personalPrimeBroker);
    }

    /**
     * Get personalPrimeBroker.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonalPrimeBroker()
    {
        return $this->personalPrimeBroker;
    }

    /**
     * @return mixed
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * @param mixed $doc
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;
    }

    /**
     * @return mixed
     */
    public function getNotices()
    {
        return $this->notices;
    }

    /**
     * @param mixed $notices
     */
    public function setNotices($notices)
    {
        $this->notices = $notices;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }




    /**
     * Add agent.
     *
     * @param \FinFlow\SettingBundle\Entity\Agent $agent
     *
     * @return PersonalInfo
     */
    public function addAgent(\FinFlow\SettingBundle\Entity\Agent $agent)
    {
        $this->agent[] = $agent;

        return $this;
    }

    /**
     * Remove agent.
     *
     * @param \FinFlow\SettingBundle\Entity\Agent $agent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAgent(\FinFlow\SettingBundle\Entity\Agent $agent)
    {
        return $this->agent->removeElement($agent);
    }

    /**
     * Set business.
     *
     * @param string|null $business
     *
     * @return PersonalInfo
     */
    public function setBusiness($business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business.
     *
     * @return string|null
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * Set thirdParty.
     *
     * @param bool|null $thirdParty
     *
     * @return PersonalInfo
     */
    public function setThirdParty($thirdParty = null)
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    /**
     * Get thirdParty.
     *
     * @return bool|null
     */
    public function getThirdParty()
    {
        return $this->thirdParty;
    }
}
