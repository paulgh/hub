<?php

namespace FinFlow\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use FinFlow\LocationBundle\Model\CountryInterface;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;

/**
 * Country
 * 
 * @ORM\Table (name="beneficial_owner")
 * @ORM\Entity(repositoryClass="\FinFlow\ClientBundle\Repository\BeneficialOwnerRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("primaryMobNumber")
 * @UniqueEntity("idNo")
 * @UniqueEntity("taxId")
 */
class BeneficialOwner {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     * @Expose
     */
    private $fullname;



    /**
     * @var \entityPriBene
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo",inversedBy="beneficialOwner"))
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $personalInfo;

    /**
     * @var Date
     *
     * @ORM\Column(name="date_of_birth", type="date",nullable=true)
     */
    private $dob;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\IdType")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="id_type_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $idType;


    /**
     * @ORM\Column(name="id_no", type="string", length=255, nullable=true)
     * @Expose
     * @Assert\NotBlank()

     */
    private $idNo;


    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\LicenseType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tax_type_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $taxType;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="tax_id", type="string", length=255, nullable=true)
     * @Expose
     */
    private $taxId;


    /**
     * @return mixed
     */
    public function getTaxType()
    {
        return $this->taxType;
    }

    /**
     * @param mixed $taxType
     */
    public function setTaxType($taxType)
    {
        $this->taxType = $taxType;
    }

    /**
     * @return mixed
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * @param mixed $taxId
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;
    }

    /**
     *
     * @Assert\NotBlank()
     * @MisdAssert\PhoneNumber()
     * @ORM\Column(name="mobile_no", type="string", nullable=true)
     * @Expose
     */
    private $primaryMobNumber;


    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $country;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="issuance_country_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $issuanceCountry;

    /**
     * @return mixed
     */
    public function getIssuanceCountry()
    {
        return $this->issuanceCountry;
    }

    /**
     * @param mixed $issuanceCountry
     */
    public function setIssuanceCountry($issuanceCountry)
    {
        $this->issuanceCountry = $issuanceCountry;
    }

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $region;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="\FinFlow\LocationBundle\Entity\Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $town;

    /**
     * @ORM\Column(name="other_contact", type="text",  nullable=true)
     * @Expose
     */
    private $otherContact;

    /**
     * @ORM\Column(name="address", type="text",  nullable=true)
     * @Expose
     */
    private $address;

    /**
     * @ORM\Column(name="business_line", type="string",  nullable=true)
     * @Expose
     */
    private $businessLine;


    /**
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\FundWealth")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wealth_source_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $wealthSource;


    /**
     *
     * @ORM\ManyToOne(targetEntity="\FinFlow\SettingBundle\Entity\FundWealth")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="fund_source_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $fundSource;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString() {
        return (String)$this->fullname;
    }

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Country
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }


    /**
     * @return Date
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param Date $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return mixed
     */
    public function getLicenseType()
    {
        return $this->licenseType;
    }

    /**
     * @param mixed $licenseType
     */
    public function setLicenseType($licenseType)
    {
        $this->licenseType = $licenseType;
    }



    /**
     * @return mixed
     */
    public function getPrimaryMobNumber()
    {
        return $this->primaryMobNumber;
    }

    /**
     * @param mixed $primaryMobNumber
     */
    public function setPrimaryMobNumber($primaryMobNumber)
    {
        $this->primaryMobNumber = $primaryMobNumber;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param mixed $town
     */
    public function setTown($town)
    {
        $this->town = $town;
    }

    /**
     * @return mixed
     */
    public function getOtherContact()
    {
        return $this->otherContact;
    }

    /**
     * @param mixed $otherContact
     */
    public function setOtherContact($otherContact)
    {
        $this->otherContact = $otherContact;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBusinessLine()
    {
        return $this->businessLine;
    }

    /**
     * @param mixed $businessLine
     */
    public function setBusinessLine($businessLine)
    {
        $this->businessLine = $businessLine;
    }

    /**
     * @return mixed
     */
    public function getWealthSource()
    {
        return $this->wealthSource;
    }

    /**
     * @param mixed $wealthSource
     */
    public function setWealthSource($wealthSource)
    {
        $this->wealthSource = $wealthSource;
    }

    /**
     * @return mixed
     */
    public function getFundSource()
    {
        return $this->fundSource;
    }

    /**
     * @param mixed $fundSource
     */
    public function setFundSource($fundSource)
    {
        $this->fundSource = $fundSource;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Country
     */
    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;

        return $this;
    }
    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt() {
        return $this->deletedAt;
    }

    public function getdeleted() {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated() {
        return $timestamp = strtotime(date_format($this->createdAt, 'Y-m-d H:i:s'));
    }

    /**
     * Get
     *
     */
    public function getupdated() {
        return $timestamp = strtotime(date_format($this->updatedAt, 'Y-m-d H:i:s'));
    }


    /**
     * Set entityPriBene.
     *
     * @param \FinFlow\ClientBundle\Entity\EntityPriBene|null $entityPriBene
     *
     * @return BeneficialOwner
     */
    public function setEntityPriBene(\FinFlow\ClientBundle\Entity\EntityPriBene $entityPriBene = null)
    {
        $this->entityPriBene = $entityPriBene;

        return $this;
    }

    /**
     * Get entityPriBene.
     *
     * @return \FinFlow\ClientBundle\Entity\EntityPriBene|null
     */
    public function getEntityPriBene()
    {
        return $this->entityPriBene;
    }

    /**
     * Set personalInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\PersonalInfo|null $personalInfo
     *
     * @return BeneficialOwner
     */
    public function setPersonalInfo(\FinFlow\ClientBundle\Entity\PersonalInfo $personalInfo = null)
    {
        $this->personalInfo = $personalInfo;

        return $this;
    }

    /**
     * Get personalInfo.
     *
     * @return \FinFlow\ClientBundle\Entity\PersonalInfo|null
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * Set idNo.
     *
     * @param string|null $idNo
     *
     * @return BeneficialOwner
     */
    public function setIdNo($idNo = null)
    {
        $this->idNo = $idNo;

        return $this;
    }

    /**
     * Get idNo.
     *
     * @return string|null
     */
    public function getIdNo()
    {
        return $this->idNo;
    }

    /**
     * Set idType.
     *
     * @param \FinFlow\SettingBundle\Entity\IdType|null $idType
     *
     * @return BeneficialOwner
     */
    public function setIdType(\FinFlow\SettingBundle\Entity\IdType $idType = null)
    {
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get idType.
     *
     * @return \FinFlow\SettingBundle\Entity\IdType|null
     */
    public function getIdType()
    {
        return $this->idType;
    }
}
