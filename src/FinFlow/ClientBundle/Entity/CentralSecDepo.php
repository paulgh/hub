<?php

namespace FinFlow\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * CentralSecDepo
 * @ORM\Table (name="central_sec_depo")
 * @ORM\Entity(repositoryClass="\FinFlow\ClientBundle\Repository\BeneficialOwnerRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @ORM\Entity
 * @UniqueEntity(fields="accountNo")
 */
class CentralSecDepo
{

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="account_name", type="string",nullable=true)
     * @Expose
     */
    private $accountName;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="account_no", type="string",nullable=true)
     * @Expose
     */
    private $accountNo;


    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="ini_acc_open_broker", type="string",nullable=true)
     * @Expose
     */
    private $iniAccOpenBroker;

    /**
     * @var PersonalInfo
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo",inversedBy="personalCsdAccountInfo",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",nullable=true)
     * }))
     * @MaxDepth(1)
     */
    private $personalInfo;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
        return $this->accountNo.' -'.$this->accountName;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param mixed $accountName
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    }


    /**
     * @return mixed
     */
    public function getIniAccOpenBroker()
    {
        return $this->iniAccOpenBroker;
    }

    /**
     * @param mixed $iniAccOpenBroker
     */
    public function setIniAccOpenBroker($iniAccOpenBroker)
    {
        $this->iniAccOpenBroker = $iniAccOpenBroker;
    }

    /**
     * @return PersonalInfo
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * @param PersonalInfo $personalInfo
     */
    public function setPersonalInfo($personalInfo)
    {
        $this->personalInfo = $personalInfo;
    }

    /**
     * @return mixed
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param mixed $accountNo
     */
    public function setAccountNo($accountNo)
    {
        $this->accountNo = $accountNo;
    }



}
