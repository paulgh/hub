<?php

namespace FinFlow\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EntityPriBene
 * @ORM\Table (name="entity_pri_bene")
 * @ORM\Entity(repositoryClass="\FinFlow\ClientBundle\Repository\BeneficialOwnerRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class EntityPriBene
{

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;


    /**
     * @Assert\NotBlank(message="Please Select the Entity")
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $personalInfo;


    /**
     *@Assert\NotBlank(message="Please Select the appropriate option ")
     * @ORM\ManyToOne(targetEntity="FinFlow\SettingBundle\Entity\AccountType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_type_id", referencedColumnName="id",nullable=true)
     * })
     */
    private $entityType;


    /**
     * @var
     *  @ORM\OneToMany(targetEntity="FinFlow\ClientBundle\Entity\Principal", mappedBy="entityPriBene", orphanRemoval=true, cascade={"persist"})
     */
    private $principal;

    /**
     * @var
     *  @ORM\OneToMany(targetEntity="FinFlow\ClientBundle\Entity\BeneficialOwner", mappedBy="entityPriBene", orphanRemoval=true, cascade={"persist"})
     */
    private $beneficialOwner;

    /**
     * @ORM\Column(name="bank_other", type="boolean", nullable=true)
     */
    private $bankOther;
    /**
     * @ORM\Column(name="bank_central", type="boolean", nullable=true)
     */
    private $bankCentral;

    /**
     * @ORM\Column(name="charitable", type="boolean", nullable=true)
     */
    private $charitable;
    /**
     * @ORM\Column(name="foundation", type="boolean", nullable=true)
     */
    private $foundation;


    /**
     * @ORM\Column(name="government_agency", type="boolean", nullable=true)
     */
    private $governmentAgency;

    /**
     * @ORM\Column(name="hedge_fund", type="boolean", nullable=true)
     */
    private $hedgeFund;

    /**
     * @ORM\Column(name="llc", type="boolean", nullable=true)
     */
    private $llc;


    /**
     * @ORM\Column(name="mutual_fund", type="boolean", nullable=true)
     */
    private $mutualFund;


    /**
 * @ORM\Column(name="partnership", type="boolean", nullable=true)
 */
    private $partnership;

    /**
     * @ORM\Column(name="pension", type="boolean", nullable=true)
     */
    private $pension;


    /**
     * @ORM\Column(name="other_penson", type="boolean", nullable=true)
     */
    private $otherPenson;

    /**
     * @ORM\Column(name="private_equity_fund", type="boolean", nullable=true)
     */
    private $privateEquityFund;


    /**
     * @ORM\Column(name="trust_ind", type="boolean", nullable=true)
     */
    private $trustInd;

    /**
     * @ORM\Column(name="trust_cop", type="boolean", nullable=true)
     */
    private $trustCop;


    /**
     * @ORM\Column(name="private_cop", type="boolean", nullable=true)
     */
    private $privateCop;


    /**
     * @ORM\Column(name="piv", type="boolean", nullable=true)
     */
    private $piv;


    /**
     * @ORM\Column(name="public_comp", type="boolean", nullable=true)
     */
    private $publicComp;

    /**
     * @ORM\Column(name="spv", type="boolean", nullable=true)
     */
    private $spv;


    /**
     * @ORM\Column(name="uni_hos_hmo", type="boolean", nullable=true)
     */
    private $uniHosHmo;


    /**
     * @ORM\Column(name="ind_joint_own", type="boolean", nullable=true)
     */
    private $indJointOwn;


    /**
     * @ORM\Column(name="broker_dealer", type="boolean", nullable=true)
     */
    private $brokerDealer;


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->principal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->beneficialOwner = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bankOther.
     *
     * @param bool|null $bankOther
     *
     * @return EntityPriBene
     */
    public function setBankOther($bankOther = null)
    {
        $this->bankOther = $bankOther;

        return $this;
    }

    /**
     * Get bankOther.
     *
     * @return bool|null
     */
    public function getBankOther()
    {
        return $this->bankOther;
    }

    /**
     * Set bankCentral.
     *
     * @param bool|null $bankCentral
     *
     * @return EntityPriBene
     */
    public function setBankCentral($bankCentral = null)
    {
        $this->bankCentral = $bankCentral;

        return $this;
    }

    /**
     * Get bankCentral.
     *
     * @return bool|null
     */
    public function getBankCentral()
    {
        return $this->bankCentral;
    }

    /**
     * Set charitable.
     *
     * @param bool|null $charitable
     *
     * @return EntityPriBene
     */
    public function setCharitable($charitable = null)
    {
        $this->charitable = $charitable;

        return $this;
    }

    /**
     * Get charitable.
     *
     * @return bool|null
     */
    public function getCharitable()
    {
        return $this->charitable;
    }

    /**
     * Set foundation.
     *
     * @param bool|null $foundation
     *
     * @return EntityPriBene
     */
    public function setFoundation($foundation = null)
    {
        $this->foundation = $foundation;

        return $this;
    }

    /**
     * Get foundation.
     *
     * @return bool|null
     */
    public function getFoundation()
    {
        return $this->foundation;
    }

    /**
     * Set governmentAgency.
     *
     * @param bool|null $governmentAgency
     *
     * @return EntityPriBene
     */
    public function setGovernmentAgency($governmentAgency = null)
    {
        $this->governmentAgency = $governmentAgency;

        return $this;
    }

    /**
     * Get governmentAgency.
     *
     * @return bool|null
     */
    public function getGovernmentAgency()
    {
        return $this->governmentAgency;
    }

    /**
     * Set hedgeFund.
     *
     * @param bool|null $hedgeFund
     *
     * @return EntityPriBene
     */
    public function setHedgeFund($hedgeFund = null)
    {
        $this->hedgeFund = $hedgeFund;

        return $this;
    }

    /**
     * Get hedgeFund.
     *
     * @return bool|null
     */
    public function getHedgeFund()
    {
        return $this->hedgeFund;
    }

    /**
     * Set llc.
     *
     * @param bool|null $llc
     *
     * @return EntityPriBene
     */
    public function setLlc($llc = null)
    {
        $this->llc = $llc;

        return $this;
    }

    /**
     * Get llc.
     *
     * @return bool|null
     */
    public function getLlc()
    {
        return $this->llc;
    }

    /**
     * Set mutualFund.
     *
     * @param bool|null $mutualFund
     *
     * @return EntityPriBene
     */
    public function setMutualFund($mutualFund = null)
    {
        $this->mutualFund = $mutualFund;

        return $this;
    }

    /**
     * Get mutualFund.
     *
     * @return bool|null
     */
    public function getMutualFund()
    {
        return $this->mutualFund;
    }

    /**
     * Set partnership.
     *
     * @param bool|null $partnership
     *
     * @return EntityPriBene
     */
    public function setPartnership($partnership = null)
    {
        $this->partnership = $partnership;

        return $this;
    }

    /**
     * Get partnership.
     *
     * @return bool|null
     */
    public function getPartnership()
    {
        return $this->partnership;
    }

    /**
     * Set pension.
     *
     * @param bool|null $pension
     *
     * @return EntityPriBene
     */
    public function setPension($pension = null)
    {
        $this->pension = $pension;

        return $this;
    }

    /**
     * Get pension.
     *
     * @return bool|null
     */
    public function getPension()
    {
        return $this->pension;
    }

    /**
     * Set otherPenson.
     *
     * @param bool|null $otherPenson
     *
     * @return EntityPriBene
     */
    public function setOtherPenson($otherPenson = null)
    {
        $this->otherPenson = $otherPenson;

        return $this;
    }

    /**
     * Get otherPenson.
     *
     * @return bool|null
     */
    public function getOtherPenson()
    {
        return $this->otherPenson;
    }

    /**
     * Set privateEquityFund.
     *
     * @param bool|null $privateEquityFund
     *
     * @return EntityPriBene
     */
    public function setPrivateEquityFund($privateEquityFund = null)
    {
        $this->privateEquityFund = $privateEquityFund;

        return $this;
    }

    /**
     * Get privateEquityFund.
     *
     * @return bool|null
     */
    public function getPrivateEquityFund()
    {
        return $this->privateEquityFund;
    }

    /**
     * Set trustInd.
     *
     * @param bool|null $trustInd
     *
     * @return EntityPriBene
     */
    public function setTrustInd($trustInd = null)
    {
        $this->trustInd = $trustInd;

        return $this;
    }

    /**
     * Get trustInd.
     *
     * @return bool|null
     */
    public function getTrustInd()
    {
        return $this->trustInd;
    }

    /**
     * Set trustCop.
     *
     * @param bool|null $trustCop
     *
     * @return EntityPriBene
     */
    public function setTrustCop($trustCop = null)
    {
        $this->trustCop = $trustCop;

        return $this;
    }

    /**
     * Get trustCop.
     *
     * @return bool|null
     */
    public function getTrustCop()
    {
        return $this->trustCop;
    }

    /**
     * Set privateCop.
     *
     * @param bool|null $privateCop
     *
     * @return EntityPriBene
     */
    public function setPrivateCop($privateCop = null)
    {
        $this->privateCop = $privateCop;

        return $this;
    }

    /**
     * Get privateCop.
     *
     * @return bool|null
     */
    public function getPrivateCop()
    {
        return $this->privateCop;
    }

    /**
     * Set piv.
     *
     * @param bool|null $piv
     *
     * @return EntityPriBene
     */
    public function setPiv($piv = null)
    {
        $this->piv = $piv;

        return $this;
    }

    /**
     * Get piv.
     *
     * @return bool|null
     */
    public function getPiv()
    {
        return $this->piv;
    }

    /**
     * Set publicComp.
     *
     * @param bool|null $publicComp
     *
     * @return EntityPriBene
     */
    public function setPublicComp($publicComp = null)
    {
        $this->publicComp = $publicComp;

        return $this;
    }

    /**
     * Get publicComp.
     *
     * @return bool|null
     */
    public function getPublicComp()
    {
        return $this->publicComp;
    }

    /**
     * Set spv.
     *
     * @param bool|null $spv
     *
     * @return EntityPriBene
     */
    public function setSpv($spv = null)
    {
        $this->spv = $spv;

        return $this;
    }

    /**
     * Get spv.
     *
     * @return bool|null
     */
    public function getSpv()
    {
        return $this->spv;
    }

    /**
     * Set uniHosHmo.
     *
     * @param bool|null $uniHosHmo
     *
     * @return EntityPriBene
     */
    public function setUniHosHmo($uniHosHmo = null)
    {
        $this->uniHosHmo = $uniHosHmo;

        return $this;
    }

    /**
     * Get uniHosHmo.
     *
     * @return bool|null
     */
    public function getUniHosHmo()
    {
        return $this->uniHosHmo;
    }

    /**
     * Set indJointOwn.
     *
     * @param bool|null $indJointOwn
     *
     * @return EntityPriBene
     */
    public function setIndJointOwn($indJointOwn = null)
    {
        $this->indJointOwn = $indJointOwn;

        return $this;
    }

    /**
     * Get indJointOwn.
     *
     * @return bool|null
     */
    public function getIndJointOwn()
    {
        return $this->indJointOwn;
    }

    /**
     * Set brokerDealer.
     *
     * @param bool|null $brokerDealer
     *
     * @return EntityPriBene
     */
    public function setBrokerDealer($brokerDealer = null)
    {
        $this->brokerDealer = $brokerDealer;

        return $this;
    }

    /**
     * Get brokerDealer.
     *
     * @return bool|null
     */
    public function getBrokerDealer()
    {
        return $this->brokerDealer;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return EntityPriBene
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return EntityPriBene
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return EntityPriBene
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }


    public function __toString()
    {
        return $this->personalInfo? (String) $this->personalInfo->getFullName():'';

    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set personalInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\PersonalInfo|null $personalInfo
     *
     * @return EntityPriBene
     */
    public function setPersonalInfo(\FinFlow\ClientBundle\Entity\PersonalInfo $personalInfo = null)
    {
        $this->personalInfo = $personalInfo;

        return $this;
    }



    /**
     * Get personalInfo.
     *
     * @return \FinFlow\ClientBundle\Entity\PersonalInfo|null
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * Add entityType.
     *
     * @param \FinFlow\SettingBundle\Entity\AccountType $entityType
     *
     * @return EntityPriBene
     */
    public function addEntityType(\FinFlow\SettingBundle\Entity\AccountType $entityType)
    {
        $this->entityType[] = $entityType;

        return $this;
    }

    /**
     * Remove entityType.
     *
     * @param \FinFlow\SettingBundle\Entity\AccountType $entityType
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEntityType(\FinFlow\SettingBundle\Entity\AccountType $entityType)
    {
        return $this->entityType->removeElement($entityType);
    }

    /**
     * Get entityType.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * Set entityType.
     *
     * @param \FinFlow\SettingBundle\Entity\AccountType|null $entityType
     *
     * @return EntityPriBene
     */
    public function setEntityType(\FinFlow\SettingBundle\Entity\AccountType $entityType = null)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * Add principal.
     *
     * @param \FinFlow\ClientBundle\Entity\Principal $principal
     *
     * @return EntityPriBene
     */
    public function addPrincipal(\FinFlow\ClientBundle\Entity\Principal $principal)
    {
        $this->principal[] = $principal;

        return $this;
    }

    /**
     * Remove principal.
     *
     * @param \FinFlow\ClientBundle\Entity\Principal $principal
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePrincipal(\FinFlow\ClientBundle\Entity\Principal $principal)
    {
        return $this->principal->removeElement($principal);
    }

    /**
     * Get principal.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Add beneficialOwner.
     *
     * @param \FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner
     *
     * @return EntityPriBene
     */
    public function addBeneficialOwner(\FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner)
    {
        $this->beneficialOwner[] = $beneficialOwner;

        return $this;
    }

    /**
     * Remove beneficialOwner.
     *
     * @param \FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBeneficialOwner(\FinFlow\ClientBundle\Entity\BeneficialOwner $beneficialOwner)
    {
        return $this->beneficialOwner->removeElement($beneficialOwner);
    }

    /**
     * Get beneficialOwner.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficialOwner()
    {
        return $this->beneficialOwner;
    }
}
