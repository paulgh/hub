<?php

namespace FinFlow\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductService
 * @ORM\Table (name="product_service")
 * @ORM\Entity(repositoryClass="\FinFlow\ClientBundle\Repository\BeneficialOwnerRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 * @UniqueEntity("primaryMobNumber")
 * @UniqueEntity("licenseNo")
 * @UniqueEntity("tin")
 */
class ProductService
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;

    /**
     * @var \personalInfo
     * @ORM\ManyToOne(targetEntity="FinFlow\ClientBundle\Entity\PersonalInfo",inversedBy="productService"))
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personal_info_id", referencedColumnName="id",nullable=true)
     * }))
     */
    private $personalInfo;

    /**
     * @ORM\Column(name="trade", type="boolean", nullable=true)
     * @Expose
     */
    private $trade;

    /**
     * @ORM\Column(name="trade_broad_auth", type="boolean", nullable=true)
     * @Expose
     */
    private $tradeBroadAuth;

    /**
     * @Assert\NotBlank(message="Fill in the Agent Name")
     * @ORM\ManyToOne(targetEntity="FinFlow\SettingBundle\Entity\Agent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $agent;

    /**
     * @ORM\Column(name="margin_account", type="boolean", nullable=true)
     * @Expose
     */
    private $marginAccount;


    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\SettingBundle\Entity\PrimeBroker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prime_broker_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     * })
     */
    private $primeBroker;


    /**
     * @ORM\Column(name="disclosure_issuer", type="boolean", nullable=true)
     * @Expose
     */
    private $disclosureIssuer;

    /**
     * @ORM\Column(name="disclosure_foreign_tax", type="boolean", nullable=true)
     * @Expose
     */
    private $disclosureForeignTax;

    /**
     * @ORM\Column(name="fx_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $fxTransaction;

    /**
     * @ORM\Column(name="mo_market_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $moMarketTransaction;

    /**
     * @ORM\Column(name="fixed_inc_transaction", type="boolean", nullable=true)
     * @Expose
     */
    private $fixedIncTransaction;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
        return $this->personalInfo? (String)$this->personalInfo->getFullName():'';
    }

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Country
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Date
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param Date $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return mixed
     */
    public function getLicenseType()
    {
        return $this->licenseType;
    }

    /**
     * @param mixed $licenseType
     */
    public function setLicenseType($licenseType)
    {
        $this->licenseType = $licenseType;
    }

    /**
     * @return mixed
     */
    public function getLicenseNo()
    {
        return $this->licenseNo;
    }

    /**
     * @param mixed $licenseNo
     */
    public function setLicenseNo($licenseNo)
    {
        $this->licenseNo = $licenseNo;
    }

    /**
     * @return mixed
     */
    public function getPrimaryMobNumber()
    {
        return $this->primaryMobNumber;
    }

    /**
     * @param mixed $primaryMobNumber
     */
    public function setPrimaryMobNumber($primaryMobNumber)
    {
        $this->primaryMobNumber = $primaryMobNumber;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param mixed $town
     */
    public function setTown($town)
    {
        $this->town = $town;
    }

    /**
     * @return mixed
     */
    public function getOtherContact()
    {
        return $this->otherContact;
    }

    /**
     * @param mixed $otherContact
     */
    public function setOtherContact($otherContact)
    {
        $this->otherContact = $otherContact;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBusinessLine()
    {
        return $this->businessLine;
    }

    /**
     * @param mixed $businessLine
     */
    public function setBusinessLine($businessLine)
    {
        $this->businessLine = $businessLine;
    }

    /**
     * @return mixed
     */
    public function getWealthSource()
    {
        return $this->wealthSource;
    }

    /**
     * @param mixed $wealthSource
     */
    public function setWealthSource($wealthSource)
    {
        $this->wealthSource = $wealthSource;
    }

    /**
     * @return mixed
     */
    public function getFundSource()
    {
        return $this->fundSource;
    }

    /**
     * @param mixed $fundSource
     */
    public function setFundSource($fundSource)
    {
        $this->fundSource = $fundSource;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Country
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getdeleted()
    {
        if (NULL != $this->deletedAt) {
            return $timestamp = strtotime(date_format($this->deletedAt, 'Y-m-d H:i:s'));
        } else {
            return 0;
        }
    }

    /**
     * Get getCountryIds
     *
     */
    public function getcreated()
    {
        return $timestamp = strtotime(date_format($this->createdAt, 'Y-m-d H:i:s'));
    }

    /**
     * Get
     *
     */
    public function getupdated()
    {
        return $timestamp = strtotime(date_format($this->updatedAt, 'Y-m-d H:i:s'));
    }


    /**
     * Set entityPriBene.
     *
     * @param \FinFlow\ClientBundle\Entity\EntityPriBene|null $entityPriBene
     *
     * @return Principal
     */
    public function setEntityPriBene(\FinFlow\ClientBundle\Entity\EntityPriBene $entityPriBene = null)
    {
        $this->entityPriBene = $entityPriBene;

        return $this;
    }

    /**
     * Get entityPriBene.
     *
     * @return \FinFlow\ClientBundle\Entity\EntityPriBene|null
     */
    public function getEntityPriBene()
    {
        return $this->entityPriBene;
    }

    /**
     * Set trade.
     *
     * @param bool|null $trade
     *
     * @return ProductService
     */
    public function setTrade($trade = null)
    {
        $this->trade = $trade;

        return $this;
    }

    /**
     * Get trade.
     *
     * @return bool|null
     */
    public function getTrade()
    {
        return $this->trade;
    }

    /**
     * Set tradeBroadAuth.
     *
     * @param bool|null $tradeBroadAuth
     *
     * @return ProductService
     */
    public function setTradeBroadAuth($tradeBroadAuth = null)
    {
        $this->tradeBroadAuth = $tradeBroadAuth;

        return $this;
    }

    /**
     * Get tradeBroadAuth.
     *
     * @return bool|null
     */
    public function getTradeBroadAuth()
    {
        return $this->tradeBroadAuth;
    }

    /**
     * Set marginAccount.
     *
     * @param bool|null $marginAccount
     *
     * @return ProductService
     */
    public function setMarginAccount($marginAccount = null)
    {
        $this->marginAccount = $marginAccount;

        return $this;
    }

    /**
     * Get marginAccount.
     *
     * @return bool|null
     */
    public function getMarginAccount()
    {
        return $this->marginAccount;
    }

    /**
     * Set disclosureIssuer.
     *
     * @param bool|null $disclosureIssuer
     *
     * @return ProductService
     */
    public function setDisclosureIssuer($disclosureIssuer = null)
    {
        $this->disclosureIssuer = $disclosureIssuer;

        return $this;
    }

    /**
     * Get disclosureIssuer.
     *
     * @return bool|null
     */
    public function getDisclosureIssuer()
    {
        return $this->disclosureIssuer;
    }

    /**
     * Set disclosureForeignTax.
     *
     * @param bool|null $disclosureForeignTax
     *
     * @return ProductService
     */
    public function setDisclosureForeignTax($disclosureForeignTax = null)
    {
        $this->disclosureForeignTax = $disclosureForeignTax;

        return $this;
    }

    /**
     * Get disclosureForeignTax.
     *
     * @return bool|null
     */
    public function getDisclosureForeignTax()
    {
        return $this->disclosureForeignTax;
    }

    /**
     * Set fxTransaction.
     *
     * @param bool|null $fxTransaction
     *
     * @return ProductService
     */
    public function setFxTransaction($fxTransaction = null)
    {
        $this->fxTransaction = $fxTransaction;

        return $this;
    }

    /**
     * Get fxTransaction.
     *
     * @return bool|null
     */
    public function getFxTransaction()
    {
        return $this->fxTransaction;
    }

    /**
     * Set moMarketTransaction.
     *
     * @param bool|null $moMarketTransaction
     *
     * @return ProductService
     */
    public function setMoMarketTransaction($moMarketTransaction = null)
    {
        $this->moMarketTransaction = $moMarketTransaction;

        return $this;
    }

    /**
     * Get moMarketTransaction.
     *
     * @return bool|null
     */
    public function getMoMarketTransaction()
    {
        return $this->moMarketTransaction;
    }

    /**
     * Set fixedIncTransaction.
     *
     * @param bool|null $fixedIncTransaction
     *
     * @return ProductService
     */
    public function setFixedIncTransaction($fixedIncTransaction = null)
    {
        $this->fixedIncTransaction = $fixedIncTransaction;

        return $this;
    }

    /**
     * Get fixedIncTransaction.
     *
     * @return bool|null
     */
    public function getFixedIncTransaction()
    {
        return $this->fixedIncTransaction;
    }

    /**
     * Set personalInfo.
     *
     * @param \FinFlow\ClientBundle\Entity\PersonalInfo|null $personalInfo
     *
     * @return ProductService
     */
    public function setPersonalInfo(\FinFlow\ClientBundle\Entity\PersonalInfo $personalInfo = null)
    {
        $this->personalInfo = $personalInfo;

        return $this;
    }

    /**
     * Get personalInfo.
     *
     * @return \FinFlow\ClientBundle\Entity\PersonalInfo|null
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * Set agent.
     *
     * @param \FinFlow\SettingBundle\Entity\Agent|null $agent
     *
     * @return ProductService
     */
    public function setAgent(\FinFlow\SettingBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent.
     *
     * @return \FinFlow\SettingBundle\Entity\Agent|null
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set primeBroker.
     *
     * @param \FinFlow\SettingBundle\Entity\PrimeBroker|null $primeBroker
     *
     * @return ProductService
     */
    public function setPrimeBroker(\FinFlow\SettingBundle\Entity\PrimeBroker $primeBroker = null)
    {
        $this->primeBroker = $primeBroker;

        return $this;
    }

    /**
     * Get primeBroker.
     *
     * @return \FinFlow\SettingBundle\Entity\PrimeBroker|null
     */
    public function getPrimeBroker()
    {
        return $this->primeBroker;
    }
}
