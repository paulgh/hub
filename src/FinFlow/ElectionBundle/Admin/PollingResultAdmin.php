<?php


    namespace FinFlow\ElectionBundle\Admin;

    use Sonata\AdminBundle\Admin\AbstractAdmin;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;
    use Doctrine\ORM\EntityRepository;

    final class PollingResultAdmin extends AbstractAdmin
    {


        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper
                ->add('totalValid')
                ->add('totalRejected')
                ->add('totalCast');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->add('constituency')
                ->add('pollingStation', 'sonata_type_model', array(
                    'label' => 'Polling Station Name'))
                ->add('year', null, array(
                    'label' => 'Election Period', 'disabled' => true))
                ->add('totalValid')
                ->add('totalRejected')
                ->add('totalCast')
                ->add('winner', null, array('label' => 'Pres. Winner'))
                ->add('pWinner', null, array('label' => 'Parl. Winner'))
                ->add('Profile', null, array(
                    'label' => 'Pink Sheet',
                    'template' => 'FinFlowElectionBundle:Default:pinkSheet.html.twig',
                    'attr' => array('style' => 'width: 54.171%; ')))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ]);
        }

        protected function configureFormFields(FormMapper $formMapper)
        {
            $fileFieldOptions = array();
            if (null != $this->getSubject()) {
                $image = $this->getSubject()->getPix();
                // use $fileFieldOptions so we can add other options to the field
                $fileFieldOptions = array('required' => false);
                $logoFieldOptions = array('required' => false);
                if ($image) {
                    // get the container so the full path to the image can be set
                    $container = $this->getConfigurationPool()->getContainer();
                    $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/pink_sheet/' . $image;
                    // add a 'help' option containing the preview's img tag
                    $fileFieldOptions['help'] = '<a class="fancybox" href="' . $fullPath . '"> <img  width="150px" src="' . $fullPath . '"
                            class="admin-preview" /></a>';
                }
            }
            $formMapper
                ->with('General Ballot information', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('constituency', null, array(
                    'error_bubbling' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return
                            $er->createQueryBuilder('y')
                                ->orderBy('y.name');
                    }
                ))
                ->add('pollingStation', 'sonata_type_model', array(
                    'label' => 'Polling Station Name'))
                ->add('year', null, array(
                    'label' => 'Election Period'))
                ->add('year', null, array(
                    'label' => 'Election Period',
                    'error_bubbling' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return
                            $er->createQueryBuilder('y')
                                ->where('y.resultLevel = :l')
                                ->setParameter('l', 'polling')
                                ->orderBy('y.name');
                    }
                ))
                ->end()
                ->with('Pink Sheet  ', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-danger',
                ))
                ->add('imageFile', 'file', array(
                    'label' => 'Upload Pink Sheet Document (Image,PDF. docx..)',
                    'by_reference' => false,
                    'data_class' => null,
                    'required' => false), $fileFieldOptions)
                ->end()
                ->with('Presidential Votes ', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-warning',
                ))
                ->add('winner', null, array())
//                ->add('pWinner', null, array('label'=>'Parl. Winner'))
                ->add('totalValid', null, array(
                    'label' => 'Total Valid Ballot', 'disabled' => true))
                ->add('totalCast', null, array(
                    'label' => 'Total Votes Casted'
                , 'disabled' => true
                ))
                ->add('totalRejected', null, array('label' => 'Rejected Ballot'))
                ->add('pollResultPartyBallot', 'sonata_type_collection',
                    array(
                        'error_bubbling' => false,
                        'by_reference' => false,
                        'required' => true,
                        'btn_add' => ' Click to add Results ',
                        'label' => false,
                        'type_options' => array()
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                    ))
                ->end()
                ->with('Parliamentary Votes ', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-danger',
                ))
                ->add('pWinner', null, array())
                ->add('ptotalValid', null, array(
                    'label' => 'Total Valid Ballot', 'disabled' => true))
                ->add('ptotalCast', null, array(
                    'label' => 'Total Votes Casted'
                , 'disabled' => true
                ))
                ->add('ptotalRejected', null, array('label' => 'Rejected Ballot'))
                ->add('pollResultParliamentaryBallot', 'sonata_type_collection',
                    array(
                        'error_bubbling' => false,
                        'by_reference' => false,
                        'required' => true,
                        'btn_add' => ' Click to add Results ',
                        'label' => false,
                        'type_options' => array()
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                    ));;
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
//        $showMapper
//            ->add('id')
//            ->add('totalValid')
//            ->add('totalRejected')
//            ->add('totalCast')

            ;
        }

        public function totalSetter($param)
        {

            $tV = array();
            $tP = array();
            $presWinner = array();
            $parWinner = array();

            foreach ($param->getPollResultPartyBallot() as $data) {
                $data->setResult($param);
                $presWinner[$data->getcandidate()->getId()] = $data->getVotes();
                $tV[] = $data->getVotes();
            }

            $votes = array_sum($tV);
            $param->setTotalCast($votes + $param->getTotalRejected());
            $param->setTotalValid($votes);

            if (count($tP) > 1) {

//            $winner = $this->getWinnerKeyData($presWinner);
//            $param->setWinner($winner);
            }

//            parlimanetary....
            foreach ($param->getPollResultParliamentaryBallot() as $pardata) {
                $pardata->setResult($param);
                $parWinner[$pardata->getcandidate()->getId()] = $pardata->getVotes();
                $tP[] = $pardata->getVotes();
            }
            $pVotes = array_sum($tP);
            $param->setPTotalCast($pVotes + $param->getPTotalRejected());
            $param->setPTotalValid($pVotes);

//            //if only array has data greater than 1. prevent single result entry....
            if (count($tP) > 1) {
//                $pWinner = $this->getWinnerKeyData($parWinner);
//                $param->setPWinner($pWinner);

            };

//            dump(count($tP));
//
//            exit;


        }


        public function setWinner($param)
        {
            $presWinner = array();
            $parWinner = array();

            //presidentials....
            foreach ($param->getPollResultPartyBallot() as $data) {
                $presWinner[$data->getcandidate()->getId()] = $data->getVotes();
            }

            if (count($presWinner) > 1) {
                $winner = $this->getWinnerKeyData($presWinner);
                $param->setWinner($winner);
//                dump($param->setWinner($winner));
//                dump($param->getPWinner($winner));
//
//                exit;
            }

//            exit;
            //parlimanetary....
            foreach ($param->getPollResultParliamentaryBallot() as $pardata) {
                $parWinner[$pardata->getcandidate()->getId()] = $pardata->getVotes();
            }

            if (count($parWinner) > 1) {
                $pWinner = $this->getWinnerKeyData($parWinner);
                $param->setPWinner($pWinner);
            }


            return $winner;
        }

        /**
         * @param array $winners
         * @return array
         */
        public function getWinnerKeyData(array $winners)
        {

            //sort the array...
            asort($winners);
            // return the last two highest...
            $checkHighestWinner = array_slice($winners, -2, 2, true);
            $highestValues = array_values($checkHighestWinner);

            // check if the array values are the same
            if ($highestValues[0] !== $highestValues[1]) {
                // get the last element key which is the highest value...
                //move to the last array[]
                // move the internal pointer to the end of the array.. which is the highest after the asort..
                end($checkHighestWinner);
                return $this->getWinnerData(key($checkHighestWinner));
            }
            return null;
        }

        public function prePersist($object)
        {
            $this->totalSetter($object);
        }

        public function postPersist($object)
        {
            $this->setWinner($object);
        }

        public function preUpdate($object)
        {
            $this->totalSetter($object);
        }

        public function postUpdate($object)
        {
            $winner=$this->setWinner($object);
            parent::postUpdate($object);
            // TODO: Change the autogenerated stub
        }

        public function getUser()
        {
            $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
            return $securityContext->getToken()->getUser();
        }

        public function getWinnerData($id)
        {
            return $data = $this->getConfigurationPool()->getContainer()
                ->get('doctrine')->getRepository("FinFlowElectionBundle:Candidate")
                ->findOneBy(array('id' => $id));
        }
    }