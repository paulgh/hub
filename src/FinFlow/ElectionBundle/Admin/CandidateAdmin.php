<?php

declare(strict_types=1);

namespace FinFlow\ElectionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class CandidateAdmin extends AbstractAdmin
{



//    protected function
    protected function configureDefaultSortValues(array &$sortValues): void
    {
        // display the first page (default = 1)
        $sortValues['_page'] = 1;

        // reverse order (default = 'ASC')
        $sortValues['_sort_order'] = 'DESC';

        // name of the ordered field (default = the model's id field, if any)
        $sortValues['_sort_by'] = 'id';
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('party')
            ->add('presidential')

        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('Profile', null, array(
                    'template' => 'FinFlowElectionBundle:Default:profileImage.html.twig',
                    'attr' => array('style' => 'width: 54.171%; ')))
            ->add('name')
            ->add('party')
            ->add('presidential')


            ->add('_action', null, [
                'actions' => [
//                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getPix();
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            $logoFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/candidate_image/' . $image;
                // add a 'help' option containing the preview's img tag
                $fileFieldOptions['help'] = '<a class="fancybox" href="' . $fullPath . '"> <img  width="150px" src="' . $fullPath . '"
                            class="admin-preview" /></a>';
            }
        }
        $formMapper
            ->add('imageFile', 'file', array(
                'label' => 'Profile photo',
                'by_reference' => false,
                'data_class' => null,
                'required' => false), $fileFieldOptions)
            ->add('name')
            ->add('party','sonata_type_model',array(
                'required'=>true))
            ->add('presidential')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
        ;
    }
}
