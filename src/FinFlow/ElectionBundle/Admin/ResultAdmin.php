<?php


    namespace FinFlow\ElectionBundle\Admin;

    use Doctrine\ORM\EntityRepository;
    use Sonata\AdminBundle\Admin\AbstractAdmin;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    final class ResultAdmin extends AbstractAdmin
    {


        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper
                ->add('totalValid')
                ->add('totalRejected')
                ->add('totalCast');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->add('constituency')
                ->add('year', null, array(
                    'label' => 'Election Period', 'disabled' => true))
                ->add('totalValid')
                ->add('totalRejected')
                ->add('totalCast')
                ->add('winner', null, array('label'=>'Pres. Winner'))
                ->add('pWinner', null, array('label'=>'Parl. Winner'))
                ->add('_action', null, [
                    'actions' => [
                        'edit' => [],
                        'delete' => [],
                    ],
                ]);
        }

        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
                ->with('General Ballot information', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-success',
                ))
                ->add('constituency', 'sonata_type_model')

                ->add('year', null, array(
                    'label' => 'Election Period',
                    'error_bubbling' => true,
                    'query_builder' => function(EntityRepository $er) {
                        return
                            $er->createQueryBuilder('y')
                                ->where('y.resultLevel = :l')
                                ->setParameter('l', 'Constituency');
                    }
                ))
                ->end()
                ->with('Presidential Votes ', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-warning',
                ))
                ->add('winner', null, array())

                ->add('totalCast', null, array(
                    'label' => 'Total Votes Casted'
                , 'disabled' => true
                ))
                ->add('totalRejected', null, array('label' => 'Rejected Ballot'))
                ->add('resultPartyBallot', 'sonata_type_collection',
                    array(
                        'error_bubbling' => false,
                        'by_reference' => false,
                        'required' => true,
                        'btn_add' => ' Click to add Results ',
                        'label' => false,
                        'type_options' => array()
                    ),
                    array('edit' => 'inline','inline' => 'table'))
                ->end()
                ->with('Parliamentary Votes ', array(
                    'class' => 'col-md-12',
                    'box_class' => 'box box-solid box-danger',
                ))

                ->add('pWinner', null, array())

                ->add('totalValid', null, array(
                    'label' => 'Total Valid Ballot', 'disabled' => true))
                ->add('ptotalValid', null, array(
                    'label' => 'Total Valid Ballot', 'disabled' => true))
                ->add('ptotalCast', null, array(
                    'label' => 'Total Votes Casted'
                , 'disabled' => true
                ))
                ->add('ptotalRejected', null, array('label' => 'Rejected Ballot'))
                ->add('resultParliamentaryBallot', 'sonata_type_collection',
                    array(
                        'error_bubbling' => false,
                        'by_reference' => false,
                        'required' => true,
                        'btn_add' => ' Click to add Results ',
                        'label' => false,
                        'type_options' => array()
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                    ));

            ;
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
//        $showMapper
//            ->add('id')
//            ->add('totalValid')
//            ->add('totalRejected')
//            ->add('totalCast')

            ;
        }

        public function totalSetter($param)
        {

            $tV = array();
            $tP = array();
            $presWinner = array();
            $parWinner = array();

            foreach ($param->getResultPartyBallot() as $data) {
                $data->setResult($param);
                $presWinner[$data->getcandidate()->getId()] = $data->getVotes();
                $tV[] = $data->getVotes();
            }

            $votes = array_sum($tV);
            $param->setTotalCast($votes + $param->getTotalRejected());
            $param->setTotalValid($votes);

            if (count($tP)>1){

//            $winner = $this->getWinnerKeyData($presWinner);
//            $param->setWinner($winner);
            }

//            parlimanetary....
            foreach ($param->getResultParliamentaryBallot() as $pardata) {
                $pardata->setResult($param);
                $parWinner[$pardata->getcandidate()->getId()] = $pardata->getVotes();
                $tP[] = $pardata->getVotes();
            }
            $pVotes = array_sum($tP);
            $param->setPTotalCast($pVotes + $param->getPTotalRejected());
            $param->setPTotalValid($pVotes);
            //if only array has data greater than 1. prevent single result entry....

            if (count($tP)>1){

//            $pWinner = $this->getWinnerKeyData($parWinner);
//            $param->setPWinner($pWinner);

            };


        }


        public function setWinner($param)
        {
            $presWinner = array();
            $parWinner = array();

            //presidentials....
            foreach ($param->getResultPartyBallot() as $data) {
                $presWinner[$data->getcandidate()->getId()] = $data->getVotes();
            }
            $winner = $this->getWinnerKeyData($presWinner);
            $param->setWinner($winner);


            //parlimanetary....
            foreach ($param->getResultParliamentaryBallot() as $pardata) {
                $parWinner[$pardata->getcandidate()->getId()] = $pardata->getVotes();
            }
            $pWinner = $this->getWinnerKeyData($parWinner);
            $param->setPWinner($pWinner);

        }
        /**
         * @param array $winners
         * @return array
         */
        public function getWinnerKeyData(array $winners)
        {
            //sort the array...
            asort($winners);
            // return the last two highest...
            $checkHighestWinner = array_slice($winners, -2, 2, true);
            $highestValues = array_values($checkHighestWinner);
            // check if the array values are the same
            if ($highestValues[0] !== $highestValues[1]) {
                // get the last element key which is the highest value...
                //move to the last array[]
                // move the internal pointer to the end of the array.. which is the highest after the asort..
                end($checkHighestWinner);
               return  $this->getWinnerData( key($checkHighestWinner));
            }
            return null;
        }


        public function prePersist($object)
        {
            $this->totalSetter($object);
        }


        public function preUpdate($object)
        {
            $this->totalSetter($object);
        }

        public function postPersist($object)
        {
            $this->setWinner($object);
        }


        public function getUser()
        {
            $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
            return $securityContext->getToken()->getUser();
        }

        public function getWinnerData($id)
        {
            return $data = $this->getConfigurationPool()->getContainer()
                ->get('doctrine')->getRepository("FinFlowElectionBundle:Candidate")
                ->findOneBy(array('id' => $id));
        }
    }