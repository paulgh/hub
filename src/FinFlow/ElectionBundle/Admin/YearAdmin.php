<?php

declare(strict_types=1);

namespace FinFlow\ElectionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class YearAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description')

        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('setAsHome')
            ->add('totalRegVoter')

            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('name')
            ->add('resultLevel', 'choice', array(
                'label'=>'Result is collected at ',
                'choices' =>
                    array(
                        'Polling Station Level' => 'Polling',
                        'Constituency Level' => 'Constituency'
                    ),
                     'help'=>'At which level are the results collected into system..'
                         )
                 )
            ->add('description')
            ->add('totalRegVoter',null,array('label'=>'Total number of Registered Voters',
                'help'=>'This take the number of voters in the Register. '))
            ->add('setAsHome',null,array('label'=>'This data will be for the landing Page',
                'help'=>'Set a custom default election analysis (Active..)'))


        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
        ;
    }



//    public function prePersist($object)
//    {
//        exit;
//        $this->totalSetter($object);
//    }
//
//    public function preUpdate($object)
//    {
//        exit;
//        $this->totalSetter($object);
//    }
}


