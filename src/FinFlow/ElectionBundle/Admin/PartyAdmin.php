<?php

declare(strict_types=1);

namespace FinFlow\ElectionBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class PartyAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name')

        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('Profile', null, array(
                'template' => 'FinFlowElectionBundle:Default:partyLogo.html.twig',
                'attr' => array('style' => 'width: 54.171%; ')))
            ->add('name')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $fileFieldOptions = array();
        if (null != $this->getSubject()) {
            $image = $this->getSubject()->getPix();
            // use $fileFieldOptions so we can add other options to the field
            $fileFieldOptions = array('required' => false);
            $logoFieldOptions = array('required' => false);
            if ($image) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/uploads/party_image/' . $image;

                // add a 'help' option containing the preview's img tag

                $fileFieldOptions['help'] = '<a class="fancybox" href="' . $fullPath . '"> <img  width="150px" src="' . $fullPath . '"
                            class="admin-preview" /></a>';            }

        }
        $formMapper

            ->add('imageFile', 'file', array(
                'label' => 'Party Logo ',
                'by_reference' => false,
                'data_class' => null,
                'required' => false), $fileFieldOptions)
            ->add('name')
            ->add('acronymn')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
        ;
    }
}
