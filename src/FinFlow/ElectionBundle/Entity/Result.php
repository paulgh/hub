<?php

namespace FinFlow\ElectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SerializerBundle\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use APY\DataGridBundle\Grid\Mapping as GRID;
use JMS\Serializer\Annotation\Accessor;

/**
 * Town
 * 
 * @ORM\Table (name="result")
 * @ORM\Entity(repositoryClass="FinFlow\ElectionBundle\Repository\ConstResultRepository")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")

 * 
 */
class Result {

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Year")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="year_id", referencedColumnName="id")
     * })
     */
    private $year;





    /**
     *
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Constituency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="constituency_id", referencedColumnName="id")
     * })
     */
    private $constituency;


    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Candidate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="p_winner_id", referencedColumnName="id")
     * })
     */
    private $pWinner;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Candidate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="winner_id", referencedColumnName="id")
     * })
     */
    private $winner;

    /**
     * @var string
     * @ORM\Column(name="total_valid", type="integer", length=255,nullable=true)
     */
    private $totalValid;


    /**
     * @var string
     * @ORM\Column(name="total_rejected", type="integer", length=255,nullable=true)
     */
    private $totalRejected;


    /**
     * @var string
     * @ORM\Column(name="total_cast", type="integer", length=255,nullable=true)
     */
    private $totalCast;


    /**
     * @var string
     * @ORM\Column(name="p_total_valid", type="integer", length=255,nullable=true)
     */
    private $ptotalValid;


    /**
     * @var string
     * @ORM\Column(name="p_total_rejected", type="integer", length=255,nullable=true)
     */
    private $ptotalRejected;


    /**
     * @var string
     * @ORM\Column(name="p_total_cast", type="integer", length=255,nullable=true)
     */
    private $ptotalCast;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultPartyBallot = new \Doctrine\Common\Collections\ArrayCollection();
        $this->resultParliamentaryBallot = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="FinFlow\ElectionBundle\Entity\PartyBallot", mappedBy="result", orphanRemoval=true, cascade={"persist"})
     */
    private $resultPartyBallot;


    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="FinFlow\ElectionBundle\Entity\ParliamentaryBallot", mappedBy="result", orphanRemoval=true, cascade={"persist"})
     */
    private $resultParliamentaryBallot;


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getResultType()
    {
        return $this->resultType;
    }

    /**
     * @param mixed $resultType
     */
    public function setResultType($resultType)
    {
        $this->resultType = $resultType;
    }

    /**
     * @return mixed
     */
    public function getConstituency()
    {
        return $this->constituency;
    }

    /**
     * @param mixed $constituency
     */
    public function setConstituency($constituency)
    {
        $this->constituency = $constituency;
    }

    /**
     * @return string
     */
    public function getTotalValid()
    {
        return $this->totalValid;
    }

    /**
     * @param string $totalValid
     */
    public function setTotalValid($totalValid)
    {
        $this->totalValid = $totalValid;
    }

    /**
     * @return string
     */
    public function getTotalRejected()
    {
        return $this->totalRejected;
    }

    /**
     * @param string $totalRejected
     */
    public function setTotalRejected($totalRejected)
    {
        $this->totalRejected = $totalRejected;
    }

    /**
     * @return string
     */
    public function getTotalCast()
    {
        return $this->totalCast;
    }

    /**
     * @param string $totalCast
     */
    public function setTotalCast($totalCast)
    {
        $this->totalCast = $totalCast;
    }

    /**
     * @return mixed
     */
    public function getResultPartyBallot()
    {
        return $this->resultPartyBallot;
    }

    /**
     * @param mixed $resultPartyBallot
     */
    public function setResultPartyBallot($resultPartyBallot)
    {
        $this->resultPartyBallot = $resultPartyBallot;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }





    /**
     * Add resultPartyBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PartyBallot $resultPartyBallot
     *
     * @return Result
     */
    public function addResultPartyBallot(\FinFlow\ElectionBundle\Entity\PartyBallot $resultPartyBallot)
    {
        $this->resultPartyBallot[] = $resultPartyBallot;

        return $this;
    }

    /**
     * Remove resultPartyBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PartyBallot $resultPartyBallot
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultPartyBallot(\FinFlow\ElectionBundle\Entity\PartyBallot $resultPartyBallot)
    {
        return $this->resultPartyBallot->removeElement($resultPartyBallot);
    }


    /**
     * Set ptotalValid.
     *
     * @param int $ptotalValid
     *
     * @return Result
     */
    public function setPtotalValid($ptotalValid)
    {
        $this->ptotalValid = $ptotalValid;

        return $this;
    }

    /**
     * Get ptotalValid.
     *
     * @return int
     */
    public function getPtotalValid()
    {
        return $this->ptotalValid;
    }

    /**
     * Set ptotalRejected.
     *
     * @param int $ptotalRejected
     *
     * @return Result
     */
    public function setPtotalRejected($ptotalRejected)
    {
        $this->ptotalRejected = $ptotalRejected;

        return $this;
    }

    /**
     * Get ptotalRejected.
     *
     * @return int
     */
    public function getPtotalRejected()
    {
        return $this->ptotalRejected;
    }

    /**
     * Set ptotalCast.
     *
     * @param int $ptotalCast
     *
     * @return Result
     */
    public function setPtotalCast($ptotalCast)
    {
        $this->ptotalCast = $ptotalCast;

        return $this;
    }

    /**
     * Get ptotalCast.
     *
     * @return int
     */
    public function getPtotalCast()
    {
        return $this->ptotalCast;
    }

    /**
     * Add resultParliamentaryBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\ParliamentaryBallot $resultParliamentaryBallot
     *
     * @return Result
     */
    public function addResultParliamentaryBallot(\FinFlow\ElectionBundle\Entity\ParliamentaryBallot $resultParliamentaryBallot)
    {
        $this->resultParliamentaryBallot[] = $resultParliamentaryBallot;

        return $this;
    }

    /**
     * Remove resultParliamentaryBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\ParliamentaryBallot $resultParliamentaryBallot
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultParliamentaryBallot(\FinFlow\ElectionBundle\Entity\ParliamentaryBallot $resultParliamentaryBallot)
    {
        return $this->resultParliamentaryBallot->removeElement($resultParliamentaryBallot);
    }

    /**
     * Get resultParliamentaryBallot.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultParliamentaryBallot()
    {
        return $this->resultParliamentaryBallot;
    }

    /**
     * @return mixed
     */
    public function getPWinner()
    {
        return $this->pWinner;
    }

    /**
     * @param mixed $pWinner
     */
    public function setPWinner($pWinner)
    {
        $this->pWinner = $pWinner;
    }

    /**
     * @return mixed
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param mixed $winner
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
    }
}
