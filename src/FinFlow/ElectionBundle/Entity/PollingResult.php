<?php

    namespace FinFlow\ElectionBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Doctrine\ORM\Mapping\JoinColumn;
    use Doctrine\Common\Collections\ArrayCollection;
    use Gedmo\Mapping\Annotation as Gedmo;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\Validator\Constraints as Assert;
    use Symfony\Component\Validator\Constraints\NotBlank;
    use JMS\SerializerBundle\Annotation\Exclude;
    use JMS\Serializer\Annotation\ExclusionPolicy;
    use JMS\Serializer\Annotation\Expose;
    use JMS\Serializer\Annotation\Type;
    use APY\DataGridBundle\Grid\Mapping as GRID;
    use JMS\Serializer\Annotation\Accessor;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


    /**
     * Town
     * @Vich\Uploadable
     * @ORM\Table (name="polling_result")
     * @ORM\Entity(repositoryClass="FinFlow\ElectionBundle\Repository\PollResultRepository")
     * @ExclusionPolicy("all")
     * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
     * @ExclusionPolicy("all")
     *
     */
    class PollingResult {

        /**
         *
         * @ORM\Column(name="id", type="integer", nullable=false)
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         * @Expose
         */
        private $id;

        /**
         * @Assert\NotBlank()
         * @ORM\ManyToOne(targetEntity="Year")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="year_id", referencedColumnName="id")
         * })
         */
        private $year;

        /**
         *
         * @Assert\NotBlank
         * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\Constituency",inversedBy="pollingResult")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="constituency_id", referencedColumnName="id")
         * })
         */
        private $constituency;


        /**
         *
         * @Assert\NotBlank
         * @ORM\ManyToOne(targetEntity="FinFlow\LocationBundle\Entity\PollingStation")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="polling_id", referencedColumnName="id")
         * })
         */
        private $pollingStation;


        /**
         *
         * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Candidate")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="p_winner_id", referencedColumnName="id")
         * })
         */
        private $pWinner;

        /**
         *
         * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Candidate")
         * @ORM\JoinColumns({
         *   @ORM\JoinColumn(name="winner_id", referencedColumnName="id")
         * })
         */
        private $winner;

        /**
         * @var string
         * @ORM\Column(name="total_valid", type="integer", length=255,nullable=true)
         */
        private $totalValid;


        /**
         * @var string
         * @ORM\Column(name="total_rejected", type="integer", length=255,nullable=true)
         */
        private $totalRejected;


        /**
         * @var string
         * @ORM\Column(name="total_cast", type="integer", length=255,nullable=true)
         */
        private $totalCast;


        /**
         * @var string
         * @ORM\Column(name="p_total_valid", type="integer", length=255,nullable=true)
         */
        private $ptotalValid;


        /**
         * @var string
         * @ORM\Column(name="p_total_rejected", type="integer", length=255,nullable=true)
         */
        private $ptotalRejected;


        /**
         * @var string
         * @ORM\Column(name="p_total_cast", type="integer", length=255,nullable=true)
         */
        private $ptotalCast;


        /**
         * Constructor
         */
        public function __construct()
        {
            $this->pollResultPartyBallot = new \Doctrine\Common\Collections\ArrayCollection();
            $this->pollResultParliamentaryBallot = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * @Assert\Valid()
         * @ORM\OneToMany(targetEntity="FinFlow\ElectionBundle\Entity\PollPartyBallot", mappedBy="result", orphanRemoval=true, cascade={"persist"})
         */
        private $pollResultPartyBallot;


        /**
         * @Assert\Valid()
         * @ORM\OneToMany(targetEntity="FinFlow\ElectionBundle\Entity\PollParliamentaryBallot", mappedBy="result", orphanRemoval=true, cascade={"persist"})
         */
        private $pollResultParliamentaryBallot;



        /**
         * @ORM\Column(name="pix", type="string", length=255, nullable=true)
         * @Expose
         */
        private $pix;

        /**
         * NOTE: This is not a mapped field of entity metadata, just a simple property.
         *
         * @Vich\UploadableField(mapping="pink_sheet", fileNameProperty="pix")
         *
         * @var File
         */
        private $imageFile;

        /**
         * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
         * of 'UploadedFile' is injected into this setter to trigger the  update. If this
         * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
         * must be able to accept an instance of 'File' as the bundle will inject one here
         * during Doctrine hydration.
         *
         * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
         * @return Candidate
         * @throws \Exception
         */
        public function setImageFile(File $image = null) {
            $this->imageFile = $image;
            if ($image) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
                $this->updatedAt = new \DateTime('now');
            }
            return $this;
        }

        /**
         * @return File
         */
        public function getImageFile() {
            return $this->imageFile;
        }



        /**
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="created_at", type="datetime",nullable=true)
         * @Type("DateTime<'Y-m-d H:i:s'>")
         */
        private $createdAt;

        /**
         * @Gedmo\Timestampable(on="update")
         * @ORM\Column(name="updated_at", type="datetime",nullable=true)
         * @Type("DateTime<'Y-m-d H:i:s'>")
         */
        private $updatedAt;

        /**
         * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
         */
        private $deletedAt;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId() {
            return $this->id;
        }

        /**
         * @return mixed
         */
        public function getYear()
        {
            return $this->year;
        }

        /**
         * @param mixed $year
         */
        public function setYear($year)
        {
            $this->year = $year;
        }

        /**
         * @return mixed
         */
        public function getResultType()
        {
            return $this->resultType;
        }



        /**
         * @return mixed
         */
        public function getConstituency()
        {
            return $this->constituency;
        }

        /**
         * @param mixed $constituency
         */
        public function setConstituency($constituency)
        {
            $this->constituency = $constituency;
        }

        /**
         * @return string
         */
        public function getTotalValid()
        {
            return $this->totalValid;
        }

        /**
         * @param string $totalValid
         */
        public function setTotalValid($totalValid)
        {
            $this->totalValid = $totalValid;
        }

        /**
         * @return string
         */
        public function getTotalRejected()
        {
            return $this->totalRejected;
        }

        /**
         * @param string $totalRejected
         */
        public function setTotalRejected($totalRejected)
        {
            $this->totalRejected = $totalRejected;
        }

        /**
         * @return string
         */
        public function getTotalCast()
        {
            return $this->totalCast;
        }

        /**
         * @param string $totalCast
         */
        public function setTotalCast($totalCast)
        {
            $this->totalCast = $totalCast;
        }



        /**
         * @return mixed
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * @param mixed $createdAt
         */
        public function setCreatedAt($createdAt)
        {
            $this->createdAt = $createdAt;
        }

        /**
         * @return mixed
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * @param mixed $updatedAt
         */
        public function setUpdatedAt($updatedAt)
        {
            $this->updatedAt = $updatedAt;
        }

        /**
         * @return mixed
         */
        public function getDeletedAt()
        {
            return $this->deletedAt;
        }

        /**
         * @param mixed $deletedAt
         */
        public function setDeletedAt($deletedAt)
        {
            $this->deletedAt = $deletedAt;
        }




        /**
         * Set ptotalValid.
         *
         * @param int $ptotalValid
         *
         * @return Result
         */
        public function setPtotalValid($ptotalValid)
        {
            $this->ptotalValid = $ptotalValid;

            return $this;
        }

        /**
         * Get ptotalValid.
         *
         * @return int
         */
        public function getPtotalValid()
        {
            return $this->ptotalValid;
        }

        /**
         * Set ptotalRejected.
         *
         * @param int $ptotalRejected
         *
         * @return Result
         */
        public function setPtotalRejected($ptotalRejected)
        {
            $this->ptotalRejected = $ptotalRejected;

            return $this;
        }

        /**
         * Get ptotalRejected.
         *
         * @return int
         */
        public function getPtotalRejected()
        {
            return $this->ptotalRejected;
        }

        /**
         * Set ptotalCast.
         *
         * @param int $ptotalCast
         *
         * @return Result
         */
        public function setPtotalCast($ptotalCast)
        {
            $this->ptotalCast = $ptotalCast;

            return $this;
        }

        /**
         * Get ptotalCast.
         *
         * @return int
         */
        public function getPtotalCast()
        {
            return $this->ptotalCast;
        }


        /**
         * @return mixed
         */
        public function getPWinner()
        {
            return $this->pWinner;
        }

        /**
         * @param mixed $pWinner
         */
        public function setPWinner($pWinner)
        {
            $this->pWinner = $pWinner;
        }

        /**
         * @return mixed
         */
        public function getWinner()
        {
            return $this->winner;
        }

        /**
         * @param mixed $winner
         */
        public function setWinner($winner)
        {
            $this->winner = $winner;
        }
    
    /**
     * Set pollingStation.
     *
     * @param \FinFlow\LocationBundle\Entity\PollingStation|null $pollingStation
     *
     * @return PollingResult
     */
    public function setPollingStation(\FinFlow\LocationBundle\Entity\PollingStation $pollingStation = null)
    {
        $this->pollingStation = $pollingStation;

        return $this;
    }

    /**
     * Get pollingStation.
     *
     * @return \FinFlow\LocationBundle\Entity\PollingStation|null
     */
    public function getPollingStation()
    {
        return $this->pollingStation;
    }

    /**
     * Add pollResultPartyBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PollPartyBallot $pollResultPartyBallot
     *
     * @return PollingResult
     */
    public function addPollResultPartyBallot(\FinFlow\ElectionBundle\Entity\PollPartyBallot $pollResultPartyBallot)
    {
        $this->pollResultPartyBallot[] = $pollResultPartyBallot;

        return $this;
    }

    /**
     * Remove pollResultPartyBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PollPartyBallot $pollResultPartyBallot
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePollResultPartyBallot(\FinFlow\ElectionBundle\Entity\PollPartyBallot $pollResultPartyBallot)
    {
        return $this->pollResultPartyBallot->removeElement($pollResultPartyBallot);
    }

    /**
     * Get pollResultPartyBallot.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPollResultPartyBallot()
    {
        return $this->pollResultPartyBallot;
    }

    /**
     * Add pollResultParliamentaryBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PollParliamentaryBallot $pollResultParliamentaryBallot
     *
     * @return PollingResult
     */
    public function addPollResultParliamentaryBallot(\FinFlow\ElectionBundle\Entity\PollParliamentaryBallot $pollResultParliamentaryBallot)
    {
        $this->pollResultParliamentaryBallot[] = $pollResultParliamentaryBallot;

        return $this;
    }

    /**
     * Remove pollResultParliamentaryBallot.
     *
     * @param \FinFlow\ElectionBundle\Entity\PollParliamentaryBallot $pollResultParliamentaryBallot
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePollResultParliamentaryBallot(\FinFlow\ElectionBundle\Entity\PollParliamentaryBallot $pollResultParliamentaryBallot)
    {
        return $this->pollResultParliamentaryBallot->removeElement($pollResultParliamentaryBallot);
    }

    /**
     * Get pollResultParliamentaryBallot.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPollResultParliamentaryBallot()
    {
        return $this->pollResultParliamentaryBallot;
    }

        /**
         * @return mixed
         */
        public function getPix()
        {
            return $this->pix;
        }

        /**
         * @param mixed $pix
         */
        public function setPix($pix)
        {
            $this->pix = $pix;
        }
}
