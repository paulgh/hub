<?php

namespace FinFlow\ElectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FinFlow\LocationBundle\Model\CountryInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\SerializerBundle\Annotation\Exclude;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * PartyBallot
 * @ORM\Table (name="party_ballot")
 * @ORM\Entity(repositoryClass="FinFlow\ElectionBundle\Repository\ConstPartyBallot")
 * @ExclusionPolicy("all")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ExclusionPolicy("all")
 */
class PartyBallot
{

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     */
    private $id;



    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Candidate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="candidate_id", referencedColumnName="id")
     * })
     */
    private $candidate;


    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="votes", type="integer",nullable=false)
     * @Expose
     */
    private $votes;


    /**
     * @var PersonalInfo
     * @ORM\ManyToOne(targetEntity="FinFlow\ElectionBundle\Entity\Result",inversedBy="resultPartyBallot",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="result_id", referencedColumnName="id",nullable=true)
     * }))
     * @MaxDepth(1)
     */
    private $result;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createdAt;


    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updatedAt;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
      return '';
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param mixed $accountName
     */
    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;
    }


    /**
     * @return mixed
     */
    public function getIniAccOpenBroker()
    {
        return $this->iniAccOpenBroker;
    }

    /**
     * @param mixed $iniAccOpenBroker
     */
    public function setIniAccOpenBroker($iniAccOpenBroker)
    {
        $this->iniAccOpenBroker = $iniAccOpenBroker;
    }

    /**
     * @return PersonalInfo
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * @param PersonalInfo $personalInfo
     */
    public function setPersonalInfo($personalInfo)
    {
        $this->personalInfo = $personalInfo;
    }

    /**
     * @return mixed
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param mixed $accountNo
     */
    public function setAccountNo($accountNo)
    {
        $this->accountNo = $accountNo;
    }




    /**
     * Set votes.
     *
     * @param int $votes
     *
     * @return PartyBallot
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes.
     *
     * @return int
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set candidate.
     *
     * @param \FinFlow\ElectionBundle\Entity\Candidate|null $candidate
     *
     * @return PartyBallot
     */
    public function setCandidate(\FinFlow\ElectionBundle\Entity\Candidate $candidate = null)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate.
     *
     * @return \FinFlow\ElectionBundle\Entity\Candidate|null
     */
    public function getCandidate()
    {
        return $this->candidate;
    }

    /**
     * Set result.
     *
     * @param \FinFlow\ElectionBundle\Entity\Result|null $result
     *
     * @return PartyBallot
     */
    public function setResult(\FinFlow\ElectionBundle\Entity\Result $result = null)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result.
     *
     * @return \FinFlow\ElectionBundle\Entity\Result|null
     */
    public function getResult()
    {
        return $this->result;
    }
}
