<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';


// $loader->add('BCC', __DIR__.'/../vendor/bcc/cron-manager-bundle');
// $loader->add('Lunetics', __DIR__.'/../vendor');
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));


return $loader;
