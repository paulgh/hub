<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200803065829 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result ADD p_winner_id INT DEFAULT NULL, ADD winner_id INT DEFAULT NULL, CHANGE total_valid total_valid INT DEFAULT NULL, CHANGE total_rejected total_rejected INT DEFAULT NULL, CHANGE total_cast total_cast INT DEFAULT NULL, CHANGE p_total_valid p_total_valid INT DEFAULT NULL, CHANGE p_total_rejected p_total_rejected INT DEFAULT NULL, CHANGE p_total_cast p_total_cast INT DEFAULT NULL');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113664FFECC FOREIGN KEY (p_winner_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1135DFCD4B8 FOREIGN KEY (winner_id) REFERENCES candidate (id)');
        $this->addSql('CREATE INDEX IDX_136AC113664FFECC ON result (p_winner_id)');
        $this->addSql('CREATE INDEX IDX_136AC1135DFCD4B8 ON result (winner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC113664FFECC');
        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC1135DFCD4B8');
        $this->addSql('DROP INDEX IDX_136AC113664FFECC ON result');
        $this->addSql('DROP INDEX IDX_136AC1135DFCD4B8 ON result');
        $this->addSql('ALTER TABLE result DROP p_winner_id, DROP winner_id, CHANGE total_valid total_valid INT NOT NULL, CHANGE total_rejected total_rejected INT NOT NULL, CHANGE total_cast total_cast INT NOT NULL, CHANGE p_total_valid p_total_valid INT NOT NULL, CHANGE p_total_rejected p_total_rejected INT NOT NULL, CHANGE p_total_cast p_total_cast INT NOT NULL');
    }
}
