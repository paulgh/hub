<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200815090731 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE polling_result ADD polling_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE polling_result ADD CONSTRAINT FK_7189C55F693B626F FOREIGN KEY (constituency_id) REFERENCES constituency (id)');
        $this->addSql('ALTER TABLE polling_result ADD CONSTRAINT FK_7189C55F6B4374D8 FOREIGN KEY (polling_id) REFERENCES polling_station (id)');
        $this->addSql('CREATE INDEX IDX_7189C55F6B4374D8 ON polling_result (polling_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE polling_result DROP FOREIGN KEY FK_7189C55F693B626F');
        $this->addSql('ALTER TABLE polling_result DROP FOREIGN KEY FK_7189C55F6B4374D8');
        $this->addSql('DROP INDEX IDX_7189C55F6B4374D8 ON polling_result');
        $this->addSql('ALTER TABLE polling_result DROP polling_id');
    }
}
