<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200815085950 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE polling_result (id INT AUTO_INCREMENT NOT NULL, year_id INT DEFAULT NULL, p_winner_id INT DEFAULT NULL, winner_id INT DEFAULT NULL, total_valid INT DEFAULT NULL, total_rejected INT DEFAULT NULL, total_cast INT DEFAULT NULL, p_total_valid INT DEFAULT NULL, p_total_rejected INT DEFAULT NULL, p_total_cast INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, constituency_id INT DEFAULT NULL, INDEX IDX_7189C55F40C1FEA7 (year_id), INDEX IDX_7189C55F693B626F (constituency_id), INDEX IDX_7189C55F664FFECC (p_winner_id), INDEX IDX_7189C55F5DFCD4B8 (winner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE poll_party_ballot (id INT AUTO_INCREMENT NOT NULL, candidate_id INT DEFAULT NULL, result_id INT DEFAULT NULL, votes INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_7DD691BF91BD8781 (candidate_id), INDEX IDX_7DD691BF7A7B643 (result_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE poll_parliamentary_ballot (id INT AUTO_INCREMENT NOT NULL, candidate_id INT DEFAULT NULL, result_id INT DEFAULT NULL, votes INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_D1F09BCC91BD8781 (candidate_id), INDEX IDX_D1F09BCC7A7B643 (result_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE polling_result ADD CONSTRAINT FK_7189C55F40C1FEA7 FOREIGN KEY (year_id) REFERENCES year (id)');
        $this->addSql('ALTER TABLE polling_result ADD CONSTRAINT FK_7189C55F664FFECC FOREIGN KEY (p_winner_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE polling_result ADD CONSTRAINT FK_7189C55F5DFCD4B8 FOREIGN KEY (winner_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE poll_party_ballot ADD CONSTRAINT FK_7DD691BF91BD8781 FOREIGN KEY (candidate_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE poll_party_ballot ADD CONSTRAINT FK_7DD691BF7A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
        $this->addSql('ALTER TABLE poll_parliamentary_ballot ADD CONSTRAINT FK_D1F09BCC91BD8781 FOREIGN KEY (candidate_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE poll_parliamentary_ballot ADD CONSTRAINT FK_D1F09BCC7A7B643 FOREIGN KEY (result_id) REFERENCES polling_result (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE poll_parliamentary_ballot DROP FOREIGN KEY FK_D1F09BCC7A7B643');
        $this->addSql('DROP TABLE polling_result');
        $this->addSql('DROP TABLE poll_party_ballot');
        $this->addSql('DROP TABLE poll_parliamentary_ballot');
    }
}
