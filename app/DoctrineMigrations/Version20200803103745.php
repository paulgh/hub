<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200803103745 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC113A7750E77');
        $this->addSql('DROP INDEX IDX_136AC113A7750E77 ON result');
        $this->addSql('ALTER TABLE result ADD p_winner_id INT DEFAULT NULL, ADD winner_id INT DEFAULT NULL, DROP result_type_id, DROP par_winner_id, DROP pres_winner_id');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113664FFECC FOREIGN KEY (p_winner_id) REFERENCES candidate (id)');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1135DFCD4B8 FOREIGN KEY (winner_id) REFERENCES candidate (id)');
        $this->addSql('CREATE INDEX IDX_136AC113664FFECC ON result (p_winner_id)');
        $this->addSql('CREATE INDEX IDX_136AC1135DFCD4B8 ON result (winner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC113664FFECC');
        $this->addSql('ALTER TABLE result DROP FOREIGN KEY FK_136AC1135DFCD4B8');
        $this->addSql('DROP INDEX IDX_136AC113664FFECC ON result');
        $this->addSql('DROP INDEX IDX_136AC1135DFCD4B8 ON result');
        $this->addSql('ALTER TABLE result ADD result_type_id INT DEFAULT NULL, ADD par_winner_id INT DEFAULT NULL, ADD pres_winner_id INT DEFAULT NULL, DROP p_winner_id, DROP winner_id');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC113A7750E77 FOREIGN KEY (result_type_id) REFERENCES result_type (id)');
        $this->addSql('CREATE INDEX IDX_136AC113A7750E77 ON result (result_type_id)');
    }
}
