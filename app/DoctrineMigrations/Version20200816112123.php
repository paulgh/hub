<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200816112123 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE year ADD result_level VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE poll_party_ballot DROP FOREIGN KEY FK_7DD691BF7A7B643');
        $this->addSql('ALTER TABLE poll_party_ballot ADD CONSTRAINT FK_7DD691BF7A7B643 FOREIGN KEY (result_id) REFERENCES polling_result (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE poll_party_ballot DROP FOREIGN KEY FK_7DD691BF7A7B643');
        $this->addSql('ALTER TABLE poll_party_ballot ADD CONSTRAINT FK_7DD691BF7A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
        $this->addSql('ALTER TABLE year DROP result_level');
    }
}
